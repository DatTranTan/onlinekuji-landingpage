/* eslint-disable react-hooks/exhaustive-deps */
import { useMutation } from '@apollo/react-hooks'
import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { MUTATION_SPIN } from '../../../graphql/mutation/spin'
import Prize from './assets/prize.png'
import { ReloadOutlined } from '@ant-design/icons'
import {
  Wrapper,
  TriangleTop,
  ButtonCustom,
  VideoCustom
} from './LuckyOpenMobile.style'
import { TriangleBottom, NameEvent, StatusEvent } from './LuckyOpenMobile.style'
import { VideoWrapper, ButtonWrapper } from './LuckyOpenMobile.style'
import { RankPrize, NamePrize, ImageWrapper } from './LuckyOpenMobile.style'
import { EventEndsTime, Ranking, PrizeWrapper } from './LuckyOpenMobile.style'
import { WrapperArmorial, EventTitleWrapper } from './LuckyOpenMobile.style'
import { Notification } from '../../../components/Notification/NotificationCustom'
import * as qs from 'query-string'
import dayjs from 'dayjs'
import Confetti from 'react-confetti'

export default function LuckyOpenMobileIpad() {
  const location = useLocation()
  const eventId = qs.parse(location.search).eventId
  const [showPrize, setShowPrize] = React.useState(true)
  const [confetti, setConfetti] = React.useState(false)
  const [dataSpin, setDataSpin] = useState<any>()

  const [spinCircle] = useMutation(MUTATION_SPIN, {
    fetchPolicy: 'no-cache'
  })

  useEffect(() => {
    spinCircle({
      variables: {
        eventId
      }
    })
      .then(({ data }) => setDataSpin(data.spin))
      .catch(({ graphQLErrors }) =>
        Notification({
          type: 'error',
          message: ' エラー',
          description: graphQLErrors[0]?.message
        })
      )
  }, [])

  return (
    <Wrapper>
      {confetti && (
        <Confetti
          id='confetti'
          gravity={0.4}
          run={confetti}
          tweenDuration={2}
          numberOfPieces={300}
          style={{
            width: '100%',
            height: '100vh',
            zIndex: 1
          }}
        />
      )}
      <TriangleTop />
      {dataSpin && (
        <EventTitleWrapper>
          <NameEvent>{dataSpin.event.name}</NameEvent>
          <StatusEvent>
            {showPrize
              ? 'お待ちください！賞品は決定中です。'
              : dataSpin?.rank !== 0
              ? 'すごい！様の賞品'
              : '残念でした'}
          </StatusEvent>
          <EventEndsTime>
            イベント終了時間：&nbsp;
            {dayjs(dataSpin.event.endTime).format('YYYY.MM.DD')}
          </EventEndsTime>
        </EventTitleWrapper>
      )}
      <TriangleBottom />
      <VideoWrapper>
        {!showPrize ? (
          <PrizeWrapper>
            {dataSpin?.rank !== 0 ? (
              <>
                <WrapperArmorial>
                  <img width='100%' height='100%' alt='aaa' src={Prize} />
                  <Ranking>{dataSpin ? `${dataSpin.rank}` : ''}</Ranking>
                </WrapperArmorial>
                <ImageWrapper>
                  <img
                    src={dataSpin ? dataSpin.imageUrl : ''}
                    alt='prize'
                    style={{ objectFit: 'contain' }}
                    width='100%'
                    height='100%'
                  />
                </ImageWrapper>
                <RankPrize>{dataSpin ? `${dataSpin.rank}等賞` : ''}</RankPrize>
                <NamePrize>{dataSpin ? dataSpin.name : ''}</NamePrize>
              </>
            ) : (
              <NamePrize>次回に、頑張ってください！</NamePrize>
            )}
            <ButtonWrapper>
              <ButtonCustom
                type='primary'
                shape='round'
                danger
                icon={<ReloadOutlined />}
                onClick={() => window.location.reload()}
              >
                賞品回転を続けます。
              </ButtonCustom>
            </ButtonWrapper>
          </PrizeWrapper>
        ) : (
          <VideoCustom
            src={
              dataSpin
                ? dataSpin?.video?.url
                : 'https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4'
            }
            autoPlay={true}
            playsInline={true}
            muted={true}
            preload='yes'
            controls={false}
            onEnded={() => {
              setShowPrize(false)
              if (dataSpin?.rank !== 0) {
                setConfetti(true)
                setTimeout(() => {
                  setConfetti(false)
                }, 20000)
              }
            }}
          />
        )}
      </VideoWrapper>
    </Wrapper>
  )
}
