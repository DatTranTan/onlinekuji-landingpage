import { styled } from 'baseui'
import { Button } from 'antd'
import Div100vh from 'react-div-100vh'

const MainContent = styled(Div100vh, () => ({
  width: '100%'
  // minHeight: 'calc(100vh - 90px)'
  // height: '100%'
}))

const Container = styled('div', () => ({
  maxWidth: '1300px',
  height: '100%',
  margin: 'auto',
  padding: ' 0 1.5rem',
  paddingTop: '90px',
  display: 'flex',
  position: 'relative',
  justifyContent: 'center',
  alignItems: 'center',
  fontFamily: 'Roboto, Helvetica, Arial, sans-serif',

  '@media only screen and (max-width: 1024px)': {
    padding: '0 2rem'
  },

  '@media only screen and (max-width: 768px)': {
    padding: '0 1.5rem'
  },

  '@media only screen and (max-width: 500px)': {
    padding: 0
  }
}))

const WrapperImg = styled('div', () => ({
  width: '100%'
}))

const WrapInfor = styled('div', () => ({
  textAlign: 'center',
  color: ' #272652',
  marginTop: '2rem',
  h1: {
    fontWeight: 700,
    color: ' #272652',
    fontSize: '2rem'
  }
}))

const ButtonPlay = styled(Button, () => ({
  width: '14rem',
  height: '4rem',
  borderRadius: '30px',
  border: '3px solid #558b95',
  fontSize: '26px',
  fontWeight: 700,
  color: '#558b95',
  cursor: 'pointer',
  animationDuration: '1s',
  animationName: 'fadeInUp',

  '&:hover': {
    backgroundColor: '#558b95',
    color: '#ffff',
    boxShadow: '0 0 10px #558b95'
  },

  '&:focus': {
    outline: 'none',
    border: '3px solid',
    borderColor: '#558b95'
  }
}))
const WrapperGif = styled('div', () => ({
  display: 'flex',
  justifyContent: 'center'
  // @media (maxWidth: 800px) {
  //   flex-flow: column,
  // }
}))

export { WrapperImg, WrapInfor, ButtonPlay, WrapperGif, Container, MainContent }
