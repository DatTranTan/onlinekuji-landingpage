import React from 'react'
import { ContainerLottery } from './Lottery.style'
import LuckyIpad from './LuckyIpad/LuckyIpad'

function Lottery() {
  return (
    <ContainerLottery>
      <LuckyIpad />
    </ContainerLottery>
  )
}

export default Lottery
