/* eslint-disable */
import React, { useState } from "react";
import {
  WrapperLucky,
  TextHeader,
  BlockCenter,
  BlockButton,
  ButtonRight,
  BlockVideo,
  WrapperVideo,
  WrapperPrize,
  BlockImg,
  VideoXXX,
  Div100vh,
  // ButtonForm,
  Ablank,
  DivText,
  ImgArrow,
  SvgStyle,
} from "./LuckyIpad.style";
import { useMutation, useQuery } from "@apollo/client";
import { useLocation } from "react-router-dom";
import { MUTATION_SPIN } from "../../../../graphql/mutation/spin";
import { Notification } from "../../../../components/Notification/NotificationCustom";
import { QUERY_EVENT } from "../../../../graphql/query/event";
import BackgroundNoPrize from "./VideoPrize/image/background_hazure.png";
import GiftBackground from "./VideoPrize/image/item.png";
import Prize from "./VideoPrize/image/prize.png";
import * as qs from "query-string";
import { Spin, Button } from "antd";
import { use100vh } from "react-div-100vh";
import { v4 as uuidv4 } from "uuid";
import Cookies from "universal-cookie";
import {
  MUTATION_INCREASE_WIN,
  MUTATION_SPUN,
} from "../../../../graphql/mutation/spun";
import Forward from "./VideoPrize/image/forward.png";
import Gift from "../../../../assets/images/box1.png";
import BtnRedirectToForm from "./BtnRedirectToForm";
import {
  isIE
} from "react-device-detect";

function LuckyIpad() {
  const NewID = uuidv4();
  const cookies = new Cookies();
  let customerID;
  async function clickCookies() {
    (await cookies.get("userID")) === undefined && cookies.set("userID", NewID);
    if (!customerID) customerID = await cookies.get("userID");
  }

  const myFunction = () => {
    let element;
    window.innerWidth > 1050
      ? (element = document.getElementById("ViewVideo"))
      : (element = document.getElementById("ViewVideoMobile"));
    element.scrollIntoView();
  };
  const height = use100vh();
  const location = useLocation();
  const eventId = qs.parse(location.search).eventId;
  const [showPrize, setShowPrize] = React.useState(true);
  const [play, setPlay] = useState(false);
  const [dataSpinByEvent, setDataSpinByEvent] = useState<any>();
  // const [idspin, setIdspin] = useState('')
  const [visibleButton, setVisibleButton] = useState(false);
  const [idSpin, setIdSpin] = useState();
  const [winPrize, setWinPrize] = useState(false);
  // const [onClickButton, setOnClickButton] = useState(false)

  const videoRef = React.useRef<HTMLVideoElement>(null);

  const [videoDimension, setVideoDimension] = useState({
    width: 0,
    height: 0,
  });
  const [loadingVideo, setLoadingVideo] = useState(true);
  const [customerLost, setCustomerLost] = useState("");
  const [style, setStyle] = React.useState({});
  const [showVideo, setShowVideo] = useState("");
  const [spinCircle, { loading }] = useMutation(MUTATION_SPIN, {
    fetchPolicy: "no-cache",
  });

  const [checkSpun] = useMutation(MUTATION_SPUN, {
    fetchPolicy: "no-cache",
  });
  const [increaseTotalWin] = useMutation(MUTATION_INCREASE_WIN, {
    fetchPolicy: "no-cache",
  });

  const { data, loading: loader } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventId,
    },
  });

  // console.log(data?.event.video.url, 'eventID')

  const reloadButton = () => {
    checkSpun({
      variables: {
        eventId,
        userId: customerID,
      },
    }).then(({ data }) => {
      // console.log(data, 'checkSpun')
      data?.checkSpun && setPlay(false);
      setLoadingVideo(false);
    });
    spinCircle({
      variables: {
        eventId,
        userId: customerID,
      },
    })
      .then(({ data }) => {
        setCustomerLost(data?.spin?.event?.customerLost);
        setDataSpinByEvent(data.spin);
        setIdSpin(data?.spin?.SpinPrizeID);
        if (
          data.spin.rank !== 0 ||
          (data.spin.rank === 0 && data.spin.event.customerLost === "Win")
        ) {
          setWinPrize(true);
          increaseTotalWin({
            variables: {
              eventId,
            },
          });
        }
        if (data.spin.rank === 0) {
          setWinPrize(true);
        }

        // setIdspin(data.spin.id)
      })
      // .then(() => window.location.reload())
      .catch(({ graphQLErrors }) =>
        // Notification({
        //   type: 'error',
        //   message: ' エラー',
        //   description: graphQLErrors[0]?.message
        // })
        {
          const mess = graphQLErrors.map((item) => item.message);
          console.log("graphQLErrors", graphQLErrors);
          if (mess.includes("このイベントはまだ開始されていません")) {
            Notification({
              type: "error",
              message: " エラー",
              description: "イベントはまだ開始されていません。",
            });
            setShowPrize(true);
            setPlay(false);
          } else if (mess.includes("このイベントは既に終了しました")) {
            Notification({
              type: "error",
              message: " エラー",
              description: "このイベントは既に終了しました",
            });
            setShowPrize(true);
            setPlay(false);
          } else {
            Notification({
              type: "error",
              message: " エラー",
              description: graphQLErrors[0]?.message,
            });
          }
        }
      );
    // .catch(({ graphQLErrors }) => {
    //   const mess = graphQLErrors.map((item) => item.message)
    //   if (mess.includes('このイベントはまだ開始されていません')) {
    //     Notification({
    //       type: 'error',
    //       message: ' エラー',
    //       description: 'イベントはまだ開催されていません。'
    //     })
    //     setShowPrize(true)
    //     setPlay(false)
    //   } else if (mess.includes('このイベントは既に終了しました')) {
    //     Notification({
    //       type: 'error',
    //       message: ' エラー',
    //       description: 'このイベントは既に終了しました'
    //     })
    //     setShowPrize(true)
    //     setPlay(false)
    //   } else {
    //     Notification({
    //       type: 'error',
    //       message: ' エラー',
    //       description: graphQLErrors[0]?.message
    //     })
    //   }
    // })
    // console.log(data, 'CheckSpin')
    setVisibleButton(false);
  };

  React.useEffect(() => {
    if (videoDimension?.width && videoDimension?.height) {
      if (videoDimension.width > videoDimension.height) {
        setStyle({ width: "70vw", height: "auto" });
      } else setStyle({ width: "auto", height: "60vh" });
    }
  }, [videoDimension]);

  React.useEffect(() => {
    if (dataSpinByEvent) {
      let video;
      if (
        data?.event.video !== null &&
        data?.event.video !== undefined &&
        dataSpinByEvent?.rank !== 0
      ) {
        video = data?.event.video?.url;
      } else if (
        data?.event.video !== null &&
        data?.event.video !== undefined &&
        dataSpinByEvent?.rank === 0
      ) {
        video =
          data?.event.noPrizeVideo === null ||
          data?.event.noPrizeVideo?.url === ""
            ? data?.event.video?.url
            : data?.event.noPrizeVideo.url;
      } else {
        video = "https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4";
      }
      setShowVideo(video);
    }
  }, [dataSpinByEvent]);
  React.useEffect(() => {
    if (showVideo.length > 0 && dataSpinByEvent && videoRef.current) {
      videoRef.current.play();
      videoRef.current.muted = false;
    }
  }, [showVideo, dataSpinByEvent, videoRef]);

  // React.useEffect(() => {
  //     let virtualElement = document.querySelector('myVideo')
  //     virtualElement.setAttribute('src', showVideo)
  //     if(!isIOS){
  //       virtualElement.addEventListener('loadeddata', () => {
  //         if(virtualElement.offsetWidth && virtualElement.offsetHeight){
  //           setVideoDimension({
  //             width: virtualElement.offsetWidth,
  //             height: virtualElement.offsetHeight
  //           })
  //           setLoadingVideo(false)
  //           virtualElement.remove()
  //         }
  //       })
  //     } else {
  //       setVideoDimension({
  //         width: virtualElement.offsetWidth,
  //         height: virtualElement.offsetHeight
  //       })
  //       setLoadingVideo(false)
  //       virtualElement.remove()
  //     }
  //     document.getElementById('root').appendChild(virtualElement)
  // }, [showVideo])

  const BannerPrize = () => (
    <div
      style={{
        background: "url(" + Prize + ")",
        backgroundRepeat: "no-repeat",
        width: "100%",
        height: "50%",
        objectFit: "cover",
        backgroundPosition: "center",
        backgroundSize: "contain",
      }}
    ></div>
  );

  return (
    <>
      <WrapperLucky id="ViewVideoMobile">
        <TextHeader>
          {/* <h1>化粧品サンプル抽選会</h1> */}
          {loader ? (
            <h1>
              <Spin />
            </h1>
          ) : (
            <h1>{data?.event.name}</h1>
          )}
        </TextHeader>
        <Div100vh
          id="ViewVideo"
          showPrize={showPrize}
          nowin={!showPrize && dataSpinByEvent?.rank === 0}
        >
          <BlockCenter
            height={height}
            style={{ width: "100%", height: "100%" }}
          >
            <BlockVideo
              height={height}
              style={{ width: "100%", height: "100%" }}
              nowin={!showPrize && dataSpinByEvent?.rank === 0}
            >
              <WrapperVideo style={{ width: "100%" }}>
                {!showPrize === true ? (
                  <WrapperPrize style={{ width: "100%", height: "100%" }}>
                    {dataSpinByEvent?.rank !== 0 ? (
                      <>
                        <BlockImg
                        // style={{
                        //   width: '100%',
                        //   height: '100%',
                        //   backgroundImage: 'url(' + BackgroundHighLight + ')',
                        //   backgroundSize: 'cover',
                        //   display: 'flex',
                        //   justifyContent: 'center',
                        //   alignItems: 'center'
                        // }}
                        >
                          <BannerPrize />

                          {/* <ImgPrize src={Prize} alt='prize' /> */}

                          <div
                            style={{
                              backgroundImage: dataSpinByEvent?.imageUrl
                                ? `url('${dataSpinByEvent?.imageUrl}')`
                                : `url('${Gift}')`,
                              backgroundRepeat: "no-repeat",
                              width: "100%",
                              height: "100%",
                              objectFit: "cover",
                              backgroundPosition: "center",
                              backgroundSize: "contain",
                            }}
                          ></div>
                          {/* <ImgGift
                            src={dataSpinByEvent?.imageUrl}
                            alt='prize'
                          /> */}

                          {/* <img
                            src={dataSpinByEvent?.imageUrl}
                            alt='backgroundPrize'
                            style={{
                              width: "100%",
                              height: "100%",
                              objectFit: "unset",
                            }}
                          /> */}
                          {/* <Blocktext>
                            「{dataSpinByEvent?.name}」が当たりました!
                          </Blocktext> */}
                          {visibleButton === true && (
                            <>
                            <BtnRedirectToForm
                              idSpin={idSpin}
                              eventId={eventId}
                              setShowPrize={setShowPrize}
                              setPlay={setPlay}
                              Forward={Forward}
                              rank={dataSpinByEvent?.rank}
                            />
                          </>
                          )}
                        </BlockImg>
                        {visibleButton === true && (
                            <>
                          {Number(dataSpinByEvent?.event?.timeOut) > 0 && (
                            <p className="timeout-text">
                              当選時の応募情報の入力は時間制限がございます。
                              <br />
                              {dataSpinByEvent?.event?.timeOut}
                              分を越えての送信は当選無効となりますのでお早めにご入力・送信ください。
                            </p>
                          )}
                          </>
                          )}
                      </>
                    ) : (
                      <>
                        <BlockImg>
                          {/* <div
                            style={{
                              background: dataSpinByEvent?.imageUrl
                                ? 'url(' + dataSpinByEvent?.imageUrl + ')'
                                : 'url(' + BackgroundNoPrize + ')',
                              backgroundRepeat: 'no-repeat',
                              width: '100%',
                              height: '100%',
                              objectFit: 'unset',
                              backgroundPosition: 'center',
                              backgroundSize: 'cover',
                              borderRadius: '19px'
                            }}
                          ></div> */}
                          {customerLost === "Win" && <BannerPrize />}

                          <div
                            style={{
                              backgroundImage: dataSpinByEvent?.imageUrl
                                ? `url('${dataSpinByEvent?.imageUrl}')`
                                : customerLost === "Win"
                                ? `url('${Gift}')`
                                : `url('${BackgroundNoPrize}')`,
                              backgroundRepeat: "no-repeat",
                              width: "100%",
                              height: "100%",
                              objectFit:
                                customerLost === "Win" ? "cover" : "unset",
                              backgroundPosition: "center",
                              backgroundSize:
                                customerLost === "Win" ? "contain" : "cover",
                              borderRadius: "19px",
                            }}
                          ></div>
                          {customerLost === "Win" && (
                            <BtnRedirectToForm
                              idSpin={idSpin}
                              eventId={eventId}
                              setShowPrize={setShowPrize}
                              setPlay={setPlay}
                              Forward={Forward}
                              rank={dataSpinByEvent?.rank}
                            />
                          )}
                        </BlockImg>
                        {customerLost === "Win" && visibleButton === true && (
                            <>
                          {Number(dataSpinByEvent?.event?.timeOut) > 0 && (
                            <p className="timeout-text">
                              当選時の応募情報の入力は時間制限がございます。
                              <br />
                              {dataSpinByEvent?.event?.timeOut}
                              分を越えての送信は当選無効となりますのでお早めにご入力・送信ください。
                            </p>
                          )}
                          </>
                          )}
                      </>

                      // <ImgBackgroundNoprize src={dataSpinByEvent?.imageUrl ? dataSpinByEvent?.imageUrl : BackgroundNoPrize} alt='' />
                    )}
                  </WrapperPrize>
                ) : (
                  <>
                    {play === false ? (
                      <img
                        src={GiftBackground}
                        alt=""
                        style={{
                          width: isIE?'':'100%',
                          height: "100%",
                          objectFit: "contain",
                          margin: "0 auto",
                        }}
                      />
                    ) : (
                      <>
                        {loading || loadingVideo ? (
                          <div
                            style={{
                              width: "100%",
                              height: "100%",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <Spin />
                          </div>
                        ) : (
                          <VideoXXX
                            id="myVideo"
                            style={style}
                            onLoadedData={() => {
                              setVideoDimension({
                                width: videoRef.current.offsetWidth,
                                height: videoRef.current.offsetHeight,
                              });
                              setLoadingVideo(false);
                            }}
                            ref={videoRef}
                            src={showVideo}
                            autoPlay={true}
                            playsInline={true}
                            muted={true}
                            preload="yes"
                            controls={false}
                            onEnded={() => {
                              setShowPrize(false);
                              // setTimeout(() => {
                              //   setPlay(false)
                              // }, 3000)
                              setTimeout(() => {
                                // dataSpinByEvent?.rank !== 0 &&
                                // setvisibleForm(true)
                                setPlay(false);
                                setVisibleButton(true);
                              }, 0);
                            }}
                          />
                        )}
                      </>
                    )}
                  </>
                )}
              </WrapperVideo>
            </BlockVideo>
            {/* <TextBottom>
              <h1>
                イベント終了時間{' '}
              {data
                ? dayjs(parseFloat(data?.event.endTime)).format('YYYY.MM.DD')
                : ''}
              {console.log(data, 'ss')}
              </h1>
            </TextBottom> */}
          </BlockCenter>
        </Div100vh>
        <BlockButton>
          {/* <ButtonLeft>抽選スタート</ButtonLeft> */}
          {!winPrize && (
            <>
              <ButtonRight
                disabled={play}
                onClick={async () => {
                  await clickCookies();
                  reloadButton();
                  setShowPrize(true);
                  setPlay(true);
                  // play == true && window.location.reload()
                  myFunction();
                }}
              >
                抽選スタート
              </ButtonRight>
            </>
          )}
        </BlockButton>
      </WrapperLucky>
    </>
  );
}

export default LuckyIpad;
