/* eslint-disable */
import React, { useCallback, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { Scrollbars } from 'react-custom-scrollbars'
// import Input from '../../../components/Input/Input'
import Input from '../../../../../components/Input/Input'
import Button, { KIND } from '../../../../../components/Button/Button'
// import { Input } from 'antd'
import Select from '../../../../../components/Select/Select'
import { Popconfirm, Modal, Checkbox, Radio } from 'antd'
import {
    Form,
    ButtonGroup,
    Container,
    Wrapper,
    ContainerForm,
    BlockForm,
    BlockTitle
} from './FormPrize.style'
import queryString from 'query-string'
import {
    FormFields,
    FormLabel,
    Error
} from '../../../../../components/FormFields/FormFields'
import { Notification } from '../../../../../components/Notification/NotificationCustom'
import { OverideSave, OverideCancel } from './Overrides'
import { MUTATION_CREATE_CUSTOMER } from '../../../../../graphql/mutation/create-customer'
import { RegisterCustomerForm } from './Interface'
import { MUTATION_SPIN } from '../../../../../graphql/mutation/spin'
import { toNumeric } from 'japanese-string-utils'
import { useHistory, useLocation } from 'react-router-dom'
import { SUCCESSFORM } from '../../../../../settings/constants'
import { from } from 'apollo-link'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { QUERY_QUESTIONNAIRE } from '../../../../../graphql/query/questionnaire'
import { QUERY_EVENT } from '../../../../../graphql/query/event'
import Questionnaire from './Questionnaire'
import { useEffect } from 'react'
import { MUTATION_CONFIRM_PRIZE } from '../../../../../graphql/mutation/confirm-prize'
const { confirm } = Modal

var genders = [
    { label: '男性', id: 0 },
    { label: '女性', id: 1 },
    { label: '回答しない', id: 2 }
]
interface EventFormInput {
    name: string
    contact_number: string
    email: string
    reemail: string
    address: string
    thirdRequired: string
    thirdNonRequired: string
}

type Props = any
const FormPrize: React.FC<Props> = (props) => {
    const location = useLocation()
    const history = useHistory()
    const eventID = queryString.parse(location.search).spin
    const event = queryString.parse(location.search).event
    const userRef = React.useRef<any>(null)
    const selectGenderRef = React.useRef<HTMLInputElement | HTMLDivElement>(null)
    const [visible, setVisible] = useState(false)
    const [modal, contextHolder] = Modal.useModal()
    const [multiAnswer, setMultiAnswer] = useState({})
    const [justOneAnswer, setJustOneAnswer] = useState({ index: '', value: '' })
    const [textRequired, setTextRequired] = useState({})
    const [textNonRequired, setTextNonRequired] = useState({})
    const { data: dataEvent } = useQuery(QUERY_EVENT, {
        variables: {
            id: event
        }
    })
    const [confirmPrize] = useMutation(MUTATION_CONFIRM_PRIZE)
    const questionnaire = dataEvent?.event.questionnaire?.questionList
    const listQuestion = questionnaire?.map((item) => ({
        id: item?.id,
        listQuestion: item?.question
    }))
    const [listAnswer, setListAnswer] = useState([])
    // const [answer, setAnswer] = useState()
    const [questionList, setQuestionList] = useState([])
    useEffect(() => {
        if (dataEvent?.event?.questionnaire) {
            setQuestionList(dataEvent?.event.questionnaire.questionList)
        }
    }, [dataEvent])

    const { register, handleSubmit, setValue, errors, setError, reset, control } =
        useForm<RegisterCustomerForm>({
            defaultValues: {
                name: '',
                gender: '',
                contact_number: '',
                email: '',
                reemail: '',
                address: '',
                postalCode: ''
            }
        })

    const [text, setText] = React.useState('')
    const [type, setType] = React.useState(null)
    const [onDisable, setOnDisable] = React.useState(false)
    const [AddCustomer] = useMutation(MUTATION_CREATE_CUSTOMER, {
        fetchPolicy: 'no-cache'
    })
    const handleTypeChange = (value) => {
        setValue('gender', value[0].id)
        setType(value)

        // console.log(value[0].id)
    }
    const onChange = (e, index) => {
        setMultiAnswer({ index, value: e })
    }

    const onChangeJustOne = (e, index) => {
        setJustOneAnswer({ index, value: e.target.value })
    }

    const textNonRequiredHandler = (index, e) => {
        setTextNonRequired({ index, value: e.target.value })
    }
    const textRequiredHandler = (index, e) => {
        setTextRequired({ index, value: e.target.value })
    }
    const onSubmit = (data: any) => {
        // console.log(eventID)
        // Notification({
        //   type: 'success',
        //   message: 'ユーザ作成に成功しました。'
        // })
        // props.setVisible(false)
        // Form.presetFields()
        // convert data to questionList
        // questionList.map((item, index) => {
        //   questionnaire.push(item.question);
        //   if (item.type === "First") {
        //     let answer = [...data[`content${index}`]];
        //     questionnaire.push(answer.join());
        //   }  else  {
        //     questionnaire.push(data[`content${index}`]);
        //   }
        // });
        const questionnaire = []
        questionList.map((item, index) => {
            questionnaire.push({
                ...item,
                answer: data[`content${index}`]
            })
        })

        if (data.email !== data.reemail) {
            setError('email', 'notmatch', 'abcxyz')
            setError('reemail', 'notmatch', 'abcxyz')
        }
        data.email === data.reemail &&
            confirm({
                title: '情報を送信します。よろしいですか？',
                icon: <ExclamationCircleOutlined />,
                // content: 'Some descriptions',
                onOk: () => {
                    const answer = questionList.map((item, index) => { })
                    AddCustomer({
                        variables: {
                            createCustomerInput: {
                                name: data.name,
                                age: parseFloat(toNumeric(data?.age)),
                                // age: 11,
                                // gender: data.gender,
                                gender: type[0]?.label,
                                // gender: 'ff',
                                tel: data.contact_number,
                                email: data.email,
                                address: data.address,
                                postalCode: data.postalCode,
                                prizeId: eventID,
                                questionnaire: JSON.stringify(questionnaire)
                            }
                        }
                    })
                        .then(() => {
                            Notification({
                                type: 'success',
                                message: 'ご応募頂き有難うございました。'
                            })
                            // props.setVisible(false)
                            reset()
                            history.replace(SUCCESSFORM)
                            // console.log(history.go(-1), 'Check History')
                        })
                        .catch((err) => {
                            Notification({ type: 'error', message: err.toString() })
                        })
                }
            })
        // console.log(data, 'show data Age')
    }
    React.useEffect(() => {
        register({ name: 'name', required: true, maxLength: 50 })
        register({ name: 'tel', required: true, maxLength: 20 })
        register({ name: 'email' }, { required: true, maxLength: 100 })
        register({ name: 'reemail' }, { required: true, maxLength: 100 })
        register({ name: 'address', required: true })
        register({ name: 'age', required: true })
        register({ name: 'gender' }, { required: true })
        // register({ name: 'CheckNumber' }, { required: true })
        register({ name: 'postalCode', required: true })
    }, [register])

    // console.log(props.idspin, 'idspin')

    React.useEffect(() => {
        if (userRef.current) userRef.current.focus()
    }, [userRef])
    let newArrQuestion = []
    listQuestion?.forEach((item1) => {
        const code = listAnswer?.find((item2) => item2?.index === item1.id)
        if (code) {
            const array = Array.isArray(code?.value)
                ? code?.value.join(',')
                : code.value
            newArrQuestion.push(item1.listQuestion, array)
        }
    })
    console.log(newArrQuestion, '0000')

    return (
        <>
            <ContainerForm>
                <BlockForm>
                    <BlockTitle>当選者様情報入力フォーム</BlockTitle>
                    <p style={{ color: 'rgb(102, 109, 146)' }}>
                        必要な情報を記入して下さい。
                        <span style={{ color: 'red' }}>(*)は必須の項目です</span>
                    </p>
                    <Form onSubmit={handleSubmit(onSubmit)} id='myForm'>
                        {/* <Scrollbars
              autoHide
              renderView={(props) => (
                <div {...props} style={{ ...props.style, overflow: 'auto' }} />
              )}
              renderTrackHorizontal={(props) => (
                <div
                  {...props}
                  style={{ display: 'none' }}
                  className='track-horizontal'
                />
              )}
            > */}
                        <Wrapper>
                            <FormFields>
                                <FormLabel>
                                    氏名&nbsp;<span style={{ color: 'red' }}>*</span>
                                </FormLabel>
                                <Input
                                    name='name'
                                    clearOnEscape
                                    inputRef={(e: any) => {
                                        register(e, {
                                            required: true,
                                            maxLength: 50,
                                            minLength: 4
                                        })
                                        userRef.current = e
                                    }}
                                />
                                {errors.name && errors.name.type === 'required' && (
                                    <Error>これは必須の項目です。</Error>
                                )}
                                {errors.name && errors.name.type === 'minLength' && (
                                    <Error>氏名は4文字以上設定してください。</Error>
                                )}
                                {errors.name && errors.name.type === 'maxLength' && (
                                    <Error>氏名は50文字以下設定してください。</Error>
                                )}
                            </FormFields>
                            <FormFields>
                                <FormLabel>年齢</FormLabel>
                                <Input
                                    // type='number'
                                    placeholder='年齢を入力してください(半角/全角)'
                                    inputRef={register({
                                        maxLength: 2,
                                        minLength: 0,
                                        // maxLength: 50,
                                        // required: true,
                                        // pattern: /^[^a-z]+$/,
                                        pattern:
                                            /^[^a-z\^@\^!\^#\^$\^%\^^\^&\^*\^+\^=\^.\^<\^>\^:\^,\^"\^~\^`\^_\^-\^ぁ-ゔ\^一-龠\^ァ-ヴー\^＠\^！\^＃\^＄\^％\^＾\^＆\^＊\^（\^）\^＿\^-]+$/
                                        // pattern: /^[^ぁ-ゔ]+$/
                                    })}
                                    name='age'
                                />
                                {/* {errors.age && errors.age.type === 'required' && (
                  <Error>これは必須の項目です。</Error>
                )} */}
                                {errors.age && errors.age.type === 'pattern' && (
                                    <Error>年齢には100以下入力してください。</Error>
                                )}
                                {errors.age && errors.age.type === 'minLength' && (
                                    <Error>年齢は0歳以上入力してください。</Error>
                                )}
                                {errors.age && errors.age.type === 'maxLength' && (
                                    <Error>年齢には100以下入力してください。</Error>
                                )}

                            </FormFields>

                            <FormFields>
                                <FormLabel>
                                    性別&nbsp;<span style={{ color: 'red' }}>*</span>
                                </FormLabel>
                                {/* <Input
                  inputRef={register({ maxLength: 50, required: true })}
                  name='gender'
                /> */}
                                <Select
                                    options={genders}
                                    name='gender'
                                    labelKey='label'
                                    valueKey='id'
                                    value={type}
                                    clearable={false}
                                    placeholder='性別を選択する'
                                    onChange={({ value }) => {
                                        // setValue('gender', value[0]?.id)
                                        handleTypeChange(value)
                                    }}
                                // onChange={handleTypeChange}
                                />
                                {errors.gender && errors.gender.type === 'required' && (
                                    <Error>これは必須の項目です。</Error>
                                )}

                            </FormFields>
                            <FormFields>
                                <FormLabel>
                                    住所&nbsp;<span style={{ color: 'red' }}>*</span>
                                </FormLabel>
                                <Input
                                    inputRef={register({ maxLength: 50, required: true })}
                                    name='address'
                                />
                                {errors.address && errors.address.type === 'required' && (
                                    <Error>これは必須の項目です。</Error>
                                )}
                                {errors.address && errors.address.type === 'maxLength' && (
                                    <Error>住所は50文字以下設定してください。</Error>
                                )}
                            </FormFields>
                            <FormFields>
                                <FormLabel>
                                    メールアドレス&nbsp;<span style={{ color: 'red' }}>*</span>
                                </FormLabel>
                                <Input
                                    inputRef={register({
                                        required: true,
                                        maxLength: 100,
                                        pattern:
                                            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                    })}
                                    name='email'
                                />
                                {errors.email && errors.email.type === 'required' && (
                                    <Error>これは必須の項目です。</Error>
                                )}
                                {errors.email && errors.email.type === 'pattern' && (
                                    <Error>メールアドレスの形式は正しくありません。</Error>
                                )}
                                {errors.email && errors.email.type === 'maxLength' && (
                                    <Error>メールアドレス100文字以下設定してください。</Error>
                                )}
                                {errors.email && errors.email.type === 'notmatch' && (
                                    <Error>確認用のメールアドレスは一致していません</Error>
                                )}
                            </FormFields>

                            <FormFields>
                                <FormLabel>
                                    メールアドレスの確認&nbsp;
                                    <span style={{ color: 'red' }}>*</span>
                                </FormLabel>
                                <Input
                                    inputRef={register({
                                        required: true,
                                        maxLength: 100,
                                        pattern:
                                            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                    })}
                                    name='reemail'
                                />
                                {errors.reemail && errors.reemail.type === 'required' && (
                                    <Error>これは必須の項目です。</Error>
                                )}
                                {errors.reemail && errors.reemail.type === 'pattern' && (
                                    <Error>メールアドレスの形式は正しくありません。</Error>
                                )}
                                {errors.reemail && errors.reemail.type === 'maxLength' && (
                                    <Error>メールアドレス100文字以下設定してください。</Error>
                                )}
                                {errors.reemail && errors.reemail.type === 'notmatch' && (
                                    <Error>確認用のメールアドレスは一致していません</Error>
                                )}
                            </FormFields>

                            <FormFields>
                                <FormLabel>
                                    電話番号&nbsp;<span style={{ color: 'red' }}>*</span>
                                </FormLabel>
                                <Input
                                    name='contact_number'
                                    inputRef={register({ maxLength: 20, required: true })}
                                />
                                {errors.contact_number &&
                                    errors.contact_number.type === 'required' && (
                                        <Error>これは必須の項目です。</Error>
                                    )}
                                {errors.contact_number &&
                                    errors.contact_number.type === 'maxLength' && (
                                        <Error>電話番号は20文字以下設定してください。</Error>
                                    )}
                            </FormFields>

                            <FormFields>
                                <FormLabel>
                                    郵便番号&nbsp;<span style={{ color: 'red' }}>*</span>
                                </FormLabel>
                                <Input
                                    inputRef={register({ maxLength: 50, required: true })}
                                    name='postalCode'
                                />
                                {errors.postalCode && errors.postalCode.type === 'required' && (
                                    <Error>これは必須の項目です。</Error>
                                )}
                                {errors.postalCode &&
                                    errors.postalCode.type === 'maxLength' && (
                                        <Error> 郵便番号は50文字以下設定してください。</Error>
                                    )}
                            </FormFields>

                            {/* questionnaire */}
                            <Questionnaire
                                errors={errors}
                                register={register}
                                setValue={setValue}
                                Controller={Controller}
                                control={control}
                            />
                            {/* end questionnaire */}
                        </Wrapper>
                        {/* </Scrollbars> */}
                        <ButtonGroup>
                            {/* <Button
                type='primary'
                kind={KIND.minimal}
                onClick={reset}
                overrides={OverideCancel}
              >
                キャンセル
              </Button> */}

                            <Button disable={true} type='submit' overrides={OverideSave}>
                                応募する
                            </Button>
                        </ButtonGroup>
                    </Form>
                </BlockForm>
            </ContainerForm>
        </>
    )
}
export default FormPrize
