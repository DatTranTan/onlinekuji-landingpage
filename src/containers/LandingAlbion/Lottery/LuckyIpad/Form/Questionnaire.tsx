
/* eslint-disable */
import { Modal, Row } from 'antd'
import React from 'react'
import {
  FormFields,
  FormLabel
} from '../../../../../components/FormFields/FormFields'
import TypeFirst from './Components/TypeFirst'
import TypeSecond from './Components/TypeSecond'
import TypeThird from './Components/TypeThird'

type Props = any
const Questionnaire: React.FC<Props> = (props) => {
  const { setValue, register, errors, control, Controller, formData, isConfirm, questionList } = props


  return (
    <>
      {questionList?.map((item, index) => {
        return (
          <>
            <div style={{ display: isConfirm ? 'block' : 'none', padding:"5px 0" }}>
              {item.type === 'First' && (
                <TypeFirst
                  formData={formData}
                  data={item}
                  index={index}
                  errors={errors}
                  register={register}
                  setValue={setValue}
                  Controller={Controller}
                  control={control}
                />
              )}
              {item.type === 'Second' && (
                <TypeSecond
                  formData={formData}
                  data={item}
                  index={index}
                  errors={errors}
                  register={register}
                  setValue={setValue}
                  Controller={Controller}
                  control={control}
                />
              )}
              {item.type === 'Third' && (
                <TypeThird
                  formData={formData}
                  data={item}
                  index={index}
                  errors={errors}
                  register={register}
                  setValue={setValue}
                />
              )}
            </div>
            <div style={{ display: !isConfirm ? 'block' : 'none' }}>
              <FormFields>
                <FormLabel>
                  <Row>質問:&nbsp;{item?.question}</Row>
                  <Row>答え:&nbsp;{typeof (formData?.[`content${index}`]) == 'string' ? formData?.[`content${index}`] :
                    formData?.[`content${index}`]?.join()}</Row>
                </FormLabel>
              </FormFields>
            </div>
          </>
        )
      })}
    </>
  )
}
export default Questionnaire