/* eslint-disable */
import { useQuery } from "@apollo/react-hooks";
import { Checkbox, Modal, Radio } from "antd";
import queryString from "query-string";
import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import {
  Error,
  FormFields,
  FormLabel,
} from "../../../../../../components/FormFields/FormFields";
// import Input from '../../../components/Input/Input'
import Input from "../../../../../../components/Input/Input";
import { QUERY_EVENT } from "../../../../../../graphql/query/event";
const { confirm } = Modal;

type Props = any;
const Questionnaire: React.FC<Props> = (props) => {
  const { data, index, setValue, register, errors,formData } = props;
  const [defaultValue, setdefaultValue]: any = useState({});

  return (
    <>
      <FormFields>
        <FormLabel>
          {data.question}
          {data.content.toUpperCase() === "TRUE" && (
            <>
              &nbsp;<span style={{ color: "red" }}>*</span>
            </>
          )}
        </FormLabel>
        <Input
          inputRef={register({
            required: data.content.toUpperCase() === "TRUE",
          })}
          name={`content${index}`}
          // onChange={(e) => textRequiredHandler(item1.id, e)}
        />
        {errors[`content${index}`] &&
          errors[`content${index}`].type === "required" && (
            <Error>これは必須の項目です。</Error>
          )}
      </FormFields>
    </>
  );
};
export default Questionnaire;
