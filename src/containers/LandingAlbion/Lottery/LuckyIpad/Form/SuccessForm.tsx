import React from 'react'
import { ContainerSuccess } from './FormPrize.style'
import TICK from '../VideoPrize/image/tick.png'

function SuccessForm() {
  return (
    <ContainerSuccess>
      ご応募頂き有難うございました。
      <img
        src={TICK}
        alt='Tick'
        style={{ width: '20%', maxWidth: '250px', height: 'auto' }}
      />
    </ContainerSuccess>
  )
}

export default SuccessForm
