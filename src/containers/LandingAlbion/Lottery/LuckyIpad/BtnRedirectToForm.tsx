import { Button } from 'antd'
import React from 'react'
import { Ablank, DivText, ImgArrow } from './LuckyIpad.style'

export default function BtnRedirectToForm({
  idSpin,
  eventId,
  setShowPrize,
  setPlay,
  Forward,
  rank
}) {
  return (
    <div>
      <Button
        onClick={() => {
          setShowPrize(true)
          setPlay(false)
          localStorage.setItem('prize','true')
          localStorage.setItem('rank',rank)
          window.open(`${window.location.origin}/form-prize?spin=${idSpin}&event=${eventId}`,'_blank')
        }}
        style={{
          border: 'none',
          // backgroundColor: '#fff',
          backgroundColor: 'transparent',
          height: '5rem'
        }}
      >
        <Ablank
          // href={`${window.location.origin}/form-prize?spin=${idSpin}&event=${eventId}`}
          target='_blank'
        >
          <DivText>
            <p style={{ margin: 'auto' }}>プレゼント発送情報を入力する</p>
            <ImgArrow src={Forward} alt='' />
          </DivText>
        </Ablank>
      </Button>
    </div>
  )
}
