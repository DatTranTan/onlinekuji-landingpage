import styled, { keyframes } from 'styled-components'
import { Button } from 'antd'

const WrapperLucky = styled.div`
  width: 100%;
  padding-left: 3.5rem;
  padding-right: 3.5rem;
  font-family: Arial;
  /* background-color: yellow; */
  @media (max-width: 500px) {
    display: flex;
    flex-flow: column;
    justify-content: space-around;
    height: 100%;
    width: 100%;
    padding-left: 1rem;
    padding-right: 1rem;
  }
  .timeout-text{
    margin-top:2rem; 
    color: red; 
    font-size: 1.25rem;
    text-align: center;
    @media (max-width: 1350px) {
        font-size: 1rem;
      }
      /* @media (max-width: 1110px) {
        font-size: 1.2rem;
      }  */
      @media (max-width: 950px) {
        font-size: 1.5rem;
      } 
      @media (max-width: 500px) {
        font-size: 1rem;
      } 
  }
`
const TextHeader = styled.div`
  h1 {
    color: white;
    text-align: center;
    /* text-shadow: 3px 7px 0px #5094c3, -1px 0px 0 #5094c3, 0 2px 0 #5094c3,
      0 -2px 0 #5094c3, 4px 2px #5094c3, -2px -2px 0px #5094c3,
      1px 1px 0 #5094c3, -1px 1px 0 #5094c3; */
    text-shadow: 5px 7px 0px #5094c3, -2px 2px 0 #5094c3, 0 2px 0 #5094c3,
      0 -2px 0 #5094c3, 5px 2px #5094c3, -2px -2px 0px #5094c3,
      2px 2px 0 #5094c3, -2px 2px 0 #5094c3;
    color: #fff;
    font-size: 4rem;
    font-weight: 600;
    word-break: break-word;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    @media (max-width: 1120px) and (max-height: 820px) {
      font-size: 3rem;
    }
    @media (max-width: 1120px) and (max-height: 1120px) {
      font-size: 3rem;
    }
    @media (max-width: 500px) {
      font-size: 2rem;
      margin-bottom: 1rem;
    }
  }
`
const BlockCenter = styled.div`
  /* width: ${(props) =>
    props.height ? `${props.height * 1.07}px` : '60rem'}; */
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column;
  @media (max-width: 1100px) {
    width: 50rem;
  }
  @media (max-width: 1100px) and (max-height: 900px) {
    width: 40rem;
  }
  @media (max-width: 850px) {
    width: 40rem;
  }

  /* @media (min-width: 1152px) and (max-height: 860px) {
    width: 40rem;
  } */
  @media (max-width: 500px) {
    width: 100%;
  }
  /* width: 60rem; */
  /* max-height: 650px; */
  /* @media (max-width: 1120px) {
    width: 50rem;
  }
  @media (max-width: 850px) {
    width: 40rem;
  }

  @media (max-height: 700px) {
    width: 45rem;
  }
  @media (max-height: 600px) {
    width: 40rem;
  }
  @media (max-height: 500px) {
    width: 30rem;
  } */
  /* @media (max-width: 850px) and (max-height: 1124) {
    width: 43rem;
  } */
  /* @media (min-width: 1152px) and (max-height: 860px) {
    width: 40rem;
  }
  @media (max-width: 500px) {
    width: 100%;
  } */
`
const TextBottom = styled.div`
  /* position: absolute;
  right: 0; */
  display: flex;
  justify-content: flex-end;
  margin-bottom: 2rem;
  color: white;
  font-weight: 600;
  font-size: 2.3rem;
  margin-top: 1rem;
  @media (max-width: 2000px) {
    font-size: 1.3rem;
  }
  @media (max-width: 1150px) and (max-height: 1400px) {
    font-size: 1rem;
  }
  @media (max-width: 850px) {
    font-size: 1rem;
  }
  @media (max-height: 860px) {
    font-size: 1rem;
  }
  @media (max-width: 500px) {
    font-size: 1rem;
  }
`
const BlockButton = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  position: relative;
  height: 13rem;
  @media (max-width: 1100px) {
    height: 10rem;
  }
  @media (max-width: 500px) {
    flex-flow: column;
    height: 6rem;
  }
`
const ButtonLeft = styled(Button)`
  height: 4.5rem;
  width: 25rem;
  left: -5.8rem;
  border-radius: 100px;
  position: absolute;
  color: #ffcc5a;
  font-size: 2.5rem;
  font-weight: 900;
  @media (max-width: 1120px) {
    left: -3.8rem;
    height: 4rem;
    width: 19rem;
    font-size: 2rem;
  }
  @media (max-width: 850px) {
    /* height: 3rem; */
    width: 14rem;
    left: -1rem;
    font-size: 1.5rem;
  }
  @media (max-width: 500px) {
    position: relative;
    left: 0;
    margin-bottom: 1rem;
    height: 3.5rem;
  }
`
const ButtonRight = styled(Button)`
  height: 6rem;
  width: 37rem;
  margin-top: -1rem;
  border-radius: 100px;
  color: #ffcc5a;
  font-size: 3.2rem;
  font-weight: 900;
  border: none !important;
  &:hover {
    color: #ffcc5a !important;
  }
  &:active {
    box-shadow: 1px 4px 1px #797272 !important;
    color: #ffcc5a !important;
    border: 1px solid #fff !important;
  }
  &:focus {
    box-shadow: 1px 4px 1px #797272 !important;
    color: #ffcc5a !important;
  }
  &:visited {
    box-shadow: 1px 4px 1px #797272 !important;
    color: #ffcc5a !important;
  }

  &.ant-btn[disabled] {
    box-shadow: 1px 4px 1px #797272 !important;
    color: #ffcc5a !important;
  }
  &.ant-btn[disabled]:hover {
    box-shadow: 1px 4px 1px #797272 !important;
    color: #ffcc5a !important;
  }
  &.ant-btn[disabled]:focus {
    box-shadow: 1px 4px 1px #797272 !important;
    color: #ffcc5a !important;
  }
  &.ant-btn[disabled]:active {
    box-shadow: 1px 4px 1px #797272 !important;
    color: #ffcc5a !important;
  }
  @media (max-width: 2000px) {
    height: 4rem;
    width: 22rem;
    font-size: 2rem;
  }

  @media (max-width: 1120px) {
    margin-top: 0;
    width: 21rem;
    font-size: 2rem;
  }
  @media (max-width: 1120px) and (max-height: 850px) {
    width: 19rem;
    height: 3.5rem;
    font-size: 1.6rem;
    margin-top: 0;
  }
  @media (max-width: 850px) {
    /* height: 3rem; */
    width: 16rem;
    font-size: 1.5rem;
    height: 3.5rem;
  }
  @media (min-width: 1152px) and (max-height: 860px) {
    width: 15rem;
    height: 3rem;
    font-size: 1.5rem;
  }
  @media (max-width: 500px) {
    height: 3.5rem;
  }
`
const BlockVideo = styled.div`
  /* height: ${(props) =>
    props.height ? `${props.height / 1.7}px` : '34.5rem'}; */
  /* height: 34.5rem; */
  background-color: #ffffff;
  border-radius: 35px;
  /* @media (max-width: 1150px) {
    height: 35rem;
  } */
  /* @media (min-width: 1152px) and (max-height: 860px) {
    height: 20rem;
  }
  @media (max-width: 1150px) and (max-height: 860px) {
    height: 28rem;
  }
  @media (max-width: 1300px) and (max-height: 820px) {
    height: 30rem;
  }

  @media (max-width: 850px) {
    height: 25rem;
  }
  @media (max-height: 700px) {
    height: 25rem;
  }
  @media (max-height: 600px) {
    height: 20rem;
  }
  @media (max-height: 500px) {
    height: 15rem;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 20rem;
  } */
  @media (max-width: 1100px) {
    height: 30rem;
  }
  /* @media (max-width: 1100px) and (max-height: 900px) {
    height: 17rem;
  } */
  @media (max-width: 850px) {
    height: ${(props) => (props.nowin === true ? '' : '25rem')};
    display: flex;
    justify-content: center;
    align-items: center;
  }
  @media (max-width: 500px) {
    height: ${(props) => (props.nowin === true ? '' : '23rem')};
  }
  @media (max-width: 350px) {
    height: ${(props) => (props.nowin === true ? '' : '19rem')};
  }
`
const WrapperVideo = styled.div`
  /* display: contents; */
  object-fit: cover;
  height: 100%;
  display: flex;
  align-items: center;
`
const WrapperPrize = styled.div`
  /* display: contents; */
  height: 100%;
  @media (max-width: 500px) {
    height: auto;
  }
`
const BlockImg = styled.div`
  width: 100%;
  /* width: 70vw; */
  height: 100%;
  object-fit: cover;
  display: flex;
  justify-content: center;
  flex-flow: column;
  align-items: center;
  position: relative;
  @media (max-width: 500px) {
    /* padding-bottom: 3rem; */
    /* height: 60%; */
  }
`
const Blocktext = styled.div`
  position: absolute;
  bottom: 50px;
  font-size: 2.5rem;
  font-weight: 600;
  color: #707070;
  @media (max-width: 2000px) {
    font-size: 1.5rem;
  }
  @media (max-width: 1150px) and (max-height: 1400px) and (min-height: 900px) {
    font-size: 1.3rem;
  }
  @media (max-width: 850px) {
    bottom: 40px;
    font-size: 1.3rem;
  }
`
const VideoXXX = styled.video`
  margin: auto;
  object-fit: cover;
  /* width: auto; */
  /* width: 100%; */
  /* width: ${(props) =>
    props.videoWidth > props.videoHeight ? '70vw' : 'auto'};
  height: ${(props) =>
    props.videoHeight > props.videoWidth ? '60vh' : 'auto'}; */
  /* border-radius: 35px; */
  /* @media (max-width: 500px) {
    height: 40%;
  }
  @media (max-width: 1024px) {
    height: 40%;
  } */
`
const Div100vh = styled.div`
  height: 100vh;
  background-color: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 30px;
  border: 10px solid #c3ecff;
  @media (max-width: 1100px) and (max-height: 1400px) {
    /* height: 66vh; */
    /* height: auto; */
  }
  @media (max-width: 850px) {
    height: ${(props) => (props.nowin === true ? '60vw' : '100vw')};
  }
  @media (max-width: 500px) {
    /* height: ${(props) => (props.showPrize === false ? '' : '550px')}; */
    height: ${(props) => (props.nowin === true ? '250px' : '550px')};
  }
`
const ImgPrize = styled.img`
  width: 100%;
  height: auto;
  /* margin-top: -6rem; */
  @media (max-width: 500px) {
    margin-top: 1rem;
  }
`
const ImgGift = styled.img`
  width: 50%;
  height: auto;
  max-height: 500px;
  object-fit: contain;

  @media (max-width: 1100px) {
    max-height: 230px;
  }

  @media (max-width: 768px) {
    max-height: 500px;
  }

  @media (max-width: 500px) {
    margin-top: 1rem;
    max-height: 270px;
    width: 14rem;
  }
`

const ImgBackgroundNoprize = styled.img`
  /* width: 100%; */
  /* width: 70vw;
  height: 100%; */
  object-fit: contain;
  border-radius: 35px;
  border: 1px solid #b7d5e7;
  @media (max-width: 500px) {
    height: 50%;
  }
`

const ButtonForm = styled(Button)`
  width: 100%;
  background-color: #74daf7;
  border-radius: 12px;
  height: 3rem;
  margin-top: 2rem;
  color: #fff;
  font-size: 1rem;
  font-weight: 600;
`

const Ablank = styled.a`
  width: 55%;
  @media (max-width: 1400px) {
    width: 80%;
  }
  @media (max-width: 800px) {
    width: 80%;
  }
  @media (max-width: 500px) {
    width: 95%;
  }
`

const DivText = styled.div`
  text {
    margin: 0;
    color: white;
    /* text-shadow: -1px -1px 0 red, 1px -1px 0 red, -1px 1px 0 red, 1px 1px 0 red; */
  }
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.5rem;
  font-weight: 600;
  color: #fb4747;
  background: transparent;
  /* @media (max-width: 1650px) {
    margin-top: -0.8rem;
    font-size: 1rem;
  } */
  @media (max-width: 500px) {
    margin-top: 2.2rem;
    font-size: 1rem;
  }
  @media (max-width: 375px) {
    margin-top: 0.2rem;
    font-size: 1rem;
  }
  @media (max-width: 420px) {
    margin-top: 1.2rem;
    font-size: 1rem;
  }
  @media (max-width: 320px) {
    margin-top: 0.2rem;
    font-size: 1rem;
  }
`
const ImgArrow = styled.img`
  width: 15%;
  height: auto;
  animation: ${() => backtop()} 2s infinite;
  @media (max-width: 1400px) {
    width: 10%;
  }
`
const backtop = () => keyframes`
            0% {
               transform: translateX(0)
            };
            /* 15% {
               transform: translateY(-25%) rotate(-5deg);
            }; */
            /* 30% {
               transform: translateX(20%) ;
            }; */
            /* 45% {
               transform: translateX(-15%) rotate(-3deg);
            }; */
            60% { 
              transform: translateX(20%);
            };
            /* 75% {
               transform: translateX(-5%) rotate(-1deg); */
            };100% {
               transform: translateX(0);
            };
`
const SvgStyle = styled.svg`
  height: 13rem;
  width: 730px;

  @media (max-width: 1650px) {
    width: 623px;
    height: 8rem;
  }
  @media (max-width: 1300px) {
    width: 623px;
    height: 8rem;
  }
  @media (max-width: 1024px) {
    width: 376px;
    height: 8rem;
  }
  @media (max-width: 768px) and (max-height: 600px) {
    width: 472px;
    height: 13rem;
  }
  @media (max-width: 720px) {
    width: 253px;
    height: 6rem;
  }
  @media (max-width: 720px) and (max-height: 550px) {
    width: 394px;
    height: 11rem;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 6rem;
  }

  @media (max-width: 420px) {
    width: 100%;
    height: 7rem;
  }

  @media (max-width: 320px) {
    width: 100%;
    height: 5rem;
  }

  p {
    font-size: 143px;
    @media (max-width: 1650px) {
      font-size: 200px;
    }
    @media (max-width: 1300px) {
      font-size: 150px;
    }
    @media (max-width: 1024px) {
      font-size: 110px;
    }
    @media (max-width: 768px) {
      font-size: 98px;
    }
    @media (max-width: 720px) {
      font-size: 96px;
    }
    @media (max-width: 540px) {
      font-size: 96px;
    }
    @media (max-height: 420px) {
      font-size: 96px;
    }

    /* fill: rgba(230, 112, 96, 0.815);
    stroke-width: 3px;
    stroke: #fb4747;
    stroke-linejoin: round;
    stroke-dasharray: 80;
    animation: animate 1s linear infinite; */
    fill: rgba(230, 112, 96, 0.815);
    stroke-width: 3px;
    stroke: #fb4747;
  }
  /* @keyframes animate {
    100% {
      stroke-dashoffset: 160;
    }
  } */
`

export {
  WrapperLucky,
  TextHeader,
  BlockCenter,
  TextBottom,
  BlockButton,
  ButtonLeft,
  ButtonRight,
  BlockVideo,
  WrapperVideo,
  WrapperPrize,
  BlockImg,
  Blocktext,
  VideoXXX,
  Div100vh,
  ImgPrize,
  ImgGift,
  ImgBackgroundNoprize,
  ButtonForm,
  Ablank,
  DivText,
  ImgArrow,
  SvgStyle
}
