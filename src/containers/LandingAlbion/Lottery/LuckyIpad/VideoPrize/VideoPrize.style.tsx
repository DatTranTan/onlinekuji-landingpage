import styled from 'styled-components'

export const BlockVideo = styled.div`
  height: 35rem;
  background-color: #fff;
  border-radius: 35px;
  /* @media (max-width: 1150px) {
    height: 35rem;
  } */
  @media (max-width: 1150px) and (max-height: 820px) {
    height: 26rem;
  }
  @media (max-width: 850px) {
    height: 25rem;
  }
`
export const ContainerVideo = styled.div``
export const WrapperVideo = styled.div``
export const WrapperPrize = styled.div``
export const BlockImg = styled.div``
export const Blocktext = styled.div``
export const VideoXXX = styled.video``
