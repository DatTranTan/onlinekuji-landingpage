import {
  BlockVideo,
  ContainerVideo,
  WrapperVideo,
  WrapperPrize,
  BlockImg,
  Blocktext
} from './VideoPrize.style'
import { useMutation } from '@apollo/client'
import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { MUTATION_SPIN } from '../../../../../graphql/mutation/spin'
import BackgroundPrize from './image/background_atari.png'
import BackgroundNoPrize from './image/background_hazure.png'
import { ReloadOutlined } from '@ant-design/icons'
import { Notification } from '../../../../../components/Notification/NotificationCustom'
import { QUERY_EVENT } from '../../../../../graphql/query/event'
import * as qs from 'query-string'
import {
  BlockButton,
  ButtonLeft,
  ButtonRight,
  TextBottom
} from '../LuckyIpad.style'

function VideoPrize() {
  const location = useLocation()
  const eventId = qs.parse(location.search).eventId
  const [showPrize, setShowPrize] = React.useState(true)
  const [dataSpin, setDataSpin] = useState<any>()
  const [play, setPlay] = useState(false)
  const [dataSpinByEvent, setDataSpinByEvent] = useState<any>()
  const [idspin, setIdspin] = useState('')

  const [spinCircle] = useMutation(MUTATION_SPIN, {
    fetchPolicy: 'no-cache'
  })

  const reloadButton = () => {
    spinCircle({
      variables: {
        eventId
        // userId: customerID
      }
    })
      .then(({ data }) => {
        setDataSpinByEvent(data.spin)
        setIdspin(data.spin.id)
      })
      .catch(({ graphQLErrors }) => {
        const mess = graphQLErrors.map((item) => item.message)
        if (mess.includes('このイベントはまだ開始されていません')) {
          Notification({
            type: 'error',
            message: ' エラー',
            description: 'イベントはまだ開始されていません。'
          })
          setShowPrize(true)
          setPlay(false)
        } else if (mess.includes('このイベントは既に終了しました')) {
          Notification({
            type: 'error',
            message: ' エラー',
            description: 'このイベントは既に終了しました'
          })
          setShowPrize(true)
          setPlay(false)
        } else {
          Notification({
            type: 'error',
            message: ' エラー',
            description: graphQLErrors[0]?.message
          })
        }
      })
  }
  return (
    <BlockVideo>
      {/* <img
        src={BackgroundNoPrize}
        alt=''
        style={{ width: '100%', height: '100%' }}
      /> */}
      <ContainerVideo>
        <WrapperVideo>
          {/* {!showPrize ? (
            <WrapperPrize>
              {dataSpinByEvent?.rank !== 0 ? (
                <>
                  <BlockImg></BlockImg>
                  <Blocktext></Blocktext>
                </>
              ) : (
                <img
                  src={BackgroundNoPrize}
                  alt=''
                  style={{ width: '100%', height: '100%' }}
                />
              )}
            </WrapperPrize>
          ) : (
            <>
              {play === false ? (
                <img alt='cai hinh hop qua' />
              ) : (
                <VideoXXX
                  src={
                    'https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4'
                  }
                  autoPlay={true}
                  playsInline={true}
                  muted={true}
                  preload='yes'
                  controls={false}
                  onEnded={() => {
                    setShowPrize(false)
                    setTimeout(() => {
                      setPlay(false)
                    }, 3000)
                  }}
                />
              )}
            </>
          )} */}
        </WrapperVideo>
      </ContainerVideo>
    </BlockVideo>
  )
}

export default VideoPrize
