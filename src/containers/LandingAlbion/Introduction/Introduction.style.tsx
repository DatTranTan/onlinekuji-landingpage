import styled from 'styled-components'

const WrapperIntroduction = styled.div`
  background-color: #fff;
  /* height: 30rem; */
  margin-top: -10rem;
  margin-left: 3.5rem;
  margin-right: 3.5rem;
  border-radius: 62px;
  width: calc(100%-7rem);
  display: flex;
  flex-flow: column;
  justify-content: center;
  @media (max-width: 500px) {
    margin-top: -4rem;
    margin-left: 1rem;
    margin-right: 1rem;
    border-radius: 30px;
  }
`
const ImgTopLeft = styled.img`
  width: 215px;
  height: 220px;
  padding-right: 20px;
  padding-bottom: 20px;
  /* width: 150px;
  height: 150px; */
  /* @media (max-width: 800px) {
    padding-right: 20px;
    padding-bottom: 20px;
  } */
  @media (max-width: 500px) {
    width: 100px;
    height: 101px;
    padding-right: 8px;
    padding-bottom: 4px;
  }
`
const BlockTitle = styled.div`
  display: flex;
  flex-flow: column;
  /* justify-content: flex-end; */
  align-items: center;
  height: 9rem;
  text-align: center;
  @media (max-width: 500px) {
    height: 6.5rem;
  }
`
const ContainerTitle = styled.div`
  color: #1da5cc;
  font-size: 2.5rem;
  font-weight: 900;
  z-index: 10;
  margin-top: 4rem;
  @media (max-width: 1300px) {
    font-size: 2.2rem;
  }
  @media (max-width: 500px) {
    font-size: 2rem;
  }
`
const LineHight = styled.img`
  width: 32rem;
  @media (max-width: 500px) {
    width: 18rem;
  }
`
const Container = styled.div`
  /* background-color: red; */
`
const BlockIntroduction = styled.div`
  width: 100%;
  padding-top: 4rem;
  padding-bottom: 4vw;
  display: flex;
  justify-content: space-around;
  padding-left: 12vw;
  padding-right: 12vw;
  @media (max-width: 800px) {
    flex-flow: column;
    /* justify-content: center; */
    align-items: center;
  }
  @media (max-width: 500px) {
    padding-left: 5vw;
    padding-right: 5vw;
  }
`
const BlockIntroductionMid = styled.div`
  width: 100%;
  padding-top: 4rem;
  padding-bottom: 4vw;
  display: flex;
  justify-content: space-around;
  flex-flow: row-reverse;
  padding-left: 12vw;
  padding-right: 12vw;
  @media (max-width: 800px) {
    flex-flow: column;
    /* justify-content: center; */
    align-items: center;
  }
  @media (max-width: 500px) {
    padding-left: 5vw;
    padding-right: 5vw;
  }
`

const ContainerLeft = styled.div`
  width: 17vw;
  height: 17vw;
  background-color: #74daf7;
  border-radius: 23px;
  padding: 1.5rem;
  @media (max-width: 800px) {
    width: 12rem;
    height: 12rem;
  }
`
const ImgGift = styled.img``
const LineGift = styled.img`
  height: 17vw;
  @media (max-width: 800px) {
    display: none;
  }
`
const LineRow = styled.img`
  @media (min-width: 800px) {
    display: none;
  }
  @media (max-width: 800px) {
    width: 15rem;
    margin-top: 1rem;
    margin-bottom: 1rem;
  }
`
const ContainerRight = styled.div`
  @media (max-width: 800px) {
    width: 100%;
  }
`
const ContainerStep = styled.div`
  font-size: 2.5rem;
  font-weight: 600;
  color: #74daf7;
  margin-left: 1.5rem;
  @media (max-width: 1120px) {
    font-size: 1.5rem;
  }
  @media (max-width: 800px) {
    font-size: 2rem;
  }
`
const Content = styled.div`
  width: 38vw;
  height: 11vw;
  background-color: #FFEDC5;
  padding: 1.5rem;
  @media (max-width: 1120px) and (max-height: 420px) {
    padding: 0.8rem;
  }
  span {
    opacity: 1;
    font-size: 2rem;
    color: #707070;
    @media (max-width: 1600px) {
      font-size: 1.5rem;
    }
    @media (max-width: 1400px) {
      font-size: 1.2rem;
    }
    @media (max-width: 1120px) {
      font-size: 1rem;
    }
    @media (max-width: 1120px) and (max-height: 420px) {
      font-size: 0.8rem;
    }
    @media (max-width: 800px) {
      font-size: 1.5rem;
    }
    @media (max-width: 800px) and (max-height: 400px) {
      font-size: 1.2rem;
    }
    @media (max-width: 500px) {
      font-size: 1rem;
    }
  }
  @media (max-width: 800px) {
    width: 100%;
    height: 12rem;
  }
  @media (max-width: 500px) {
    height: 10rem;
  }
`
const ContentLast = styled.div`
  width: 38vw;
  height: 13vw;
  background-color: #FFEDC5;
  padding: 1.5rem;
  @media (max-width: 1120px) and (max-height: 420px) {
    padding: 0.8rem;
  }
  span {
    opacity: 1;
    font-size: 2rem;
    color: #707070;
    @media (max-width: 1600px) {
      font-size: 1.5rem;
    }
    @media (max-width: 1400px) {
      font-size: 1.2rem;
    }
    @media (max-width: 1120px) {
      font-size: 1rem;
    }

    @media (max-width: 1120px) and (max-height: 420px) {
      font-size: 0.8rem;
    }
    @media (max-width: 800px) {
      font-size: 1.5rem;
    }
    @media (max-width: 800px) and (max-height: 400px) {
      font-size: 1.2rem;
    }
    @media (max-width: 500px) {
      font-size: 0.9rem;
    }
  }
  @media (max-width: 800px) {
    width: 100%;
    height: 12rem;
  }
  @media (max-width: 500px) {
    height: 10rem;
    margin-bottom: 2rem;
  }
`
const ImgBotRight = styled.img`
  @media (max-width: 800px) {
    width: 150px;
    height: 150px;
  }
  @media (max-width: 500px) {
    width: 100px;
    height: 100px;
  }
`
const DivBottomLine = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  padding-right: 3.4rem;
  margin-top: -16.8rem;
  @media (max-width: 800px) {
    padding-right: 3.5rem;
    margin-top: -9.3rem;
  }
  @media (max-width: 500px) {
    /* padding-right: 2rem;
    margin-top: -9.3rem; */
    padding-right: 1rem;
    margin-top: -6.2rem;
  }
`

export {
  WrapperIntroduction,
  ImgTopLeft,
  BlockTitle,
  ContainerTitle,
  LineHight,
  Container,
  BlockIntroduction,
  ContainerLeft,
  ImgGift,
  LineGift,
  ContainerRight,
  ContainerStep,
  Content,
  ImgBotRight,
  DivBottomLine,
  LineRow,
  BlockIntroductionMid,
  ContentLast
}
