import React from 'react'
import {
  WrapperIntroduction,
  ImgTopLeft,
  BlockTitle,
  ContainerTitle,
  LineHight,
  Container,
  BlockIntroduction,
  BlockIntroductionMid,
  ContainerLeft,
  ImgGift,
  LineGift,
  ContainerRight,
  ContainerStep,
  Content,
  ContentLast,
  ImgBotRight,
  DivBottomLine,
  LineRow
} from './Introduction.style'
import LineLeft from '../img/line11.png'
import Line3 from '../img/line3.png'
import LineBot from '../img/lineBottom.png'
import Line7 from '../img/line7.png'
import Line7Copy from '../img/line7Copy.png'
import Itemgift from '../img/itemgift.png'

function Introduction() {
  return (
    <>
      <ImgTopLeft src={LineLeft} alt='' />
      <WrapperIntroduction>
        <BlockTitle>
          <ContainerTitle>抽選システム案内</ContainerTitle>
          <LineHight src={Line3} alt='' />
        </BlockTitle>
        <Container>
          <BlockIntroduction>
            <ContainerLeft>
              <ImgGift
                src={Itemgift}
                alt='itemgift'
                style={{ width: '100%', height: '100%', objectFit: 'contain' }}
              />
            </ContainerLeft>
            <div style={{ width: '1px' }} />
            <LineGift src={Line7} alt='' />
            <LineRow src={Line7Copy} alt='' />
            <ContainerRight>
              <ContainerStep>STEP1</ContainerStep>
              <Content>
                <span>ウェブサイトでプレゼントリストを ご覧ください。</span>
              </Content>
            </ContainerRight>
          </BlockIntroduction>
          <BlockIntroductionMid>
            <ContainerLeft>
              <ImgGift
                src={Itemgift}
                alt='itemgift'
                style={{ width: '100%', height: '100%', objectFit: 'contain' }}
              />
            </ContainerLeft>
            <div style={{ width: '1px' }} />
            <LineGift src={Line7} alt='' />
            <LineRow src={Line7Copy} alt='' />
            <ContainerRight>
              <ContainerStep>STEP2</ContainerStep>
              <Content>
                <span>
                  以下の抽選ボタンを押してから、
                  <br /> 抽選結果をお待ちください。
                </span>
              </Content>
            </ContainerRight>
          </BlockIntroductionMid>
          <BlockIntroduction style={{ marginBottom: '10vw' }}>
            <ContainerLeft>
              <ImgGift
                src={Itemgift}
                alt='itemgift'
                style={{ width: '100%', height: '100%', objectFit: 'contain' }}
              />
            </ContainerLeft>
            <div style={{ width: '1px' }} />
            <LineGift src={Line7} alt='' />
            <LineRow src={Line7Copy} alt='' />
            <ContainerRight>
              <ContainerStep>STEP3</ContainerStep>
              <ContentLast>
                <span>
                  当選された方は以下のフォームに 必要事項を入力してください。
                  <br />
                  フォームに入力されたご住所へ プレゼントをお届けします。
                </span>
              </ContentLast>
            </ContainerRight>
          </BlockIntroduction>
        </Container>
      </WrapperIntroduction>
      <DivBottomLine>
        <ImgBotRight src={LineBot} alt='' />
      </DivBottomLine>
    </>
  )
}

export default Introduction
