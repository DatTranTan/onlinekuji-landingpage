/* eslint-disable */
import React, { useState } from 'react'
import {
  WrapperAlbion,
  ContainerAlbion,
  ContainerPanner,
  ContainerPrize,
  ContainerIntroduction,
  ContainerLottery,
  ContainerPannerBot,
  PannerLeft,
  PannerRight,
  Footer,
  Content1,
  LineTop,
  ContainerTitle,
  LineImg,
  ImgBackTop,
  ImgLeft,
  ImgRight,
  ImgHref
} from './LandingAlbion.style'
import PrizeAlbion from '../../components/PrizeAlbion/PrizeAlbion'
import { QUERY_EVENT } from '../../graphql/query/event'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { useLocation } from 'react-router-dom'
import queryString from 'query-string'
import Line1 from './img/line1.png'
import Introduction from './Introduction/Introduction'
import Lottery from './Lottery/Lottery'
import Line4 from './img/line4.png'
import { BackTop } from 'antd'
import { DivBackTop } from '../LotteryPage/Spinning/LuckyOpenIpad.style'
import BackToTop from './img/backtobutton.png'
import { useEffect } from 'react'
import { MUTATION_VISIT } from '../../graphql/mutation/total-visit'

function LandingAlbion() {
  // const slideImages = [
  //   './img/background_hazure.png',
  //   './img/background_atari.png'
  // ]
  const location = useLocation()
  const eventID = queryString.parse(location.search).eventId
  const { data } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventID
    }
  })

  let dataPrizes = eventID && data?.event.prizes
  let losePrizes = dataPrizes && dataPrizes?.filter((item) => item.rank === 0)
  let winPrizes = dataPrizes && dataPrizes?.filter((item) => item.rank !== 0)
  const dataOrderPrizes = dataPrizes && [...winPrizes, ...losePrizes]

  const [increaseTotalVisit] = useMutation(MUTATION_VISIT, {
    fetchPolicy: 'no-cache'
  })

  useEffect(() => {
    increaseTotalVisit({
      variables: {
        eventId: eventID
      }
    })
  }, [])
  return (
    <WrapperAlbion>
      <ContainerAlbion>
        {/* <LineTop /> */}
        <ContainerPanner>
          {/* <div style={{ height: '1000px', width: '1000px' }} /> */}
          <ImgHref
            href={data?.event?.bannerUrl === '' ? '#' : data?.event?.bannerUrl}
            target={data?.event?.bannerUrl === '' ? '' : '_blank'}
            disabled={data?.event.bannerUrl === '' && true}
          >
            <img
              src={data?.event.banner}
              alt=''
              style={{
                objectFit: 'unset',
                width: '100%',
                height: 'auto',
                // minHeight: '30rem',
                maxHeight: '1200px'
              }}
            />
          </ImgHref>
        </ContainerPanner>
        {/* <LineTop /> */}
        <Content1>
          <ContainerTitle>当選者へのプレゼント</ContainerTitle>
          <LineImg src={Line1} alt='line1' />
        </Content1>
        <ContainerPrize>
          {dataOrderPrizes
            ?.filter((item) =>
              data?.event.customerLost === 'Win' ? item : item?.rank !== 0
            )
            ?.map((item, index) => (
              <PrizeAlbion
                key={index}
                {...item}
                namePrize={item.name}
                numberPrize={item.quantity}
                imagePrize={item.imageUrl}
              />
            ))}
        </ContainerPrize>
        {/* <LineTop /> */}
        <ContainerIntroduction>
          <Introduction />
        </ContainerIntroduction>
        <ContainerLottery>
          {/* <h1
            style={{
              textAlign: 'center',
              textShadow:
                '3px 7px 0px #5094c3, -1px 0px 0 #5094c3, 0 2px 0 #5094c3, 0 -2px 0 #5094c3, 4px 2px #5094c3, -2px -2px 0px #5094c3, 1px 1px 0 #5094c3, -1px 1px 0 #5094c3',
              color: '#fff',
              fontSize: '4rem',
              fontWeight: 600
            }}
          >
            化粧品サンプル抽選会
          </h1> */}
          <Lottery />
        </ContainerLottery>
        <div
          style={{
            backgroundImage: 'url(' + Line4 + ')',
            // background: 'url(' + Line4 + ')',
            width: '100%',
            height: '2rem',
            backgroundColor: '#fff',
            marginTop: '3rem'
          }}
        />
        <ContainerPannerBot>
          <PannerLeft>
            {data?.event.footerBanner !== null && (
              <ImgHref
                href={
                  data?.event?.footerBannerUrl?.[0] === ''
                    ? '#'
                    : data?.event?.footerBannerUrl?.[0]
                }
                target={data?.event?.footerBannerUrl?.[0] === '' ? '' : '_blank'}
                disabled={data?.event.footerBannerUrl?.[0] === '' && true}
              >
                <ImgLeft
                  src={
                    data?.event.footerBanner !== null &&
                    data?.event.footerBanner?.[0]
                  }
                  alt='PannerLeft'
                />
              </ImgHref>
            )}
          </PannerLeft>
          <PannerRight>
            {data?.event.footerBanner !== null && (
              <ImgHref
                href={
                  data?.event?.footerBannerUrl?.[1] === ''
                    ? '#'
                    : data?.event?.footerBannerUrl?.[1]
                }
                disabled={data?.event.footerBannerUrl?.[1] === '' && true}
                target={data?.event?.footerBannerUrl?.[1] === '' ? '' : '_blank'}
              >
                <ImgRight
                  src={
                    data?.event.footerBanner !== null &&
                    data?.event.footerBanner?.[1]
                  }
                  alt='PannerRight'
                />
              </ImgHref>
            )}
          </PannerRight>
        </ContainerPannerBot>
        <DivBackTop>
          <BackTop duration={0}>
            <ImgBackTop src={BackToTop} alt='BackTop' />
          </BackTop>
        </DivBackTop>
        <Footer />
      </ContainerAlbion>
    </WrapperAlbion>
  )
}

export default LandingAlbion
