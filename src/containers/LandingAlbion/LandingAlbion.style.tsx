import styled from 'styled-components'
import {isDesktop} from 'react-device-detect'

const WrapperAlbion = styled.div`
  height: 300vh;
`
const ContainerAlbion = styled.div`
  background: #74daf7;
`
const ContainerPanner = styled.div`
  width: 100%;
  border: 1;
  background-color: #fff;
  border: 1px solid #707070;
`
const ContainerPrize = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  justify-content: center;
  background: linear-gradient(#ffcc5a, #fff 116%);
  border: 1px solid #707070;
  border-top: none;
  padding-bottom: 5%;
  @media (max-width: 500px) {
    padding-bottom: 8%;
    justify-content: space-around;
  }
`
const ContainerIntroduction = styled.div`
  padding-bottom: 8rem;
  @media (max-width: 500px) {
    padding-bottom: 4rem;
  }
`
const ContainerLottery = styled.div``
const ContainerPannerBot = styled.div`
  background-color: #fff;
  height: 80vh;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: 3.5rem;
  padding-right: 3.5rem;
  @media (max-width: 1500px) and (min-height: 800px) {
    height: 60vh;
  }
  @media (max-width: 1400px) and (min-height: 950px) {
    height: 57vh;
  }
  @media (max-width: 1100px) {
    height: 32vh;
  }
  @media (max-width: 1100px) and (max-height: 800px) {
    height: ${isDesktop?'75vh':'50vh'};
  }
  @media (max-width: 1100px) and (max-height: 420px) {
    height: ${isDesktop?'75vh':'80vh'};
  }
  @media (max-width: 500px) {
    /* padding-left: 2rem;
    padding-right: 2rem; */
    padding: 2rem;
    height: 72vh;
    flex-flow: column;
    justify-content: center;
  }

  @media (max-width: 500px) and (max-height: 760px) {
    /* padding-left: 2rem;
    padding-right: 2rem; */
    padding: 2rem;
    height: 82vh;
    flex-flow: column;
    justify-content: center;
  }
`
const ImgHref = styled.a``

const PannerLeft = styled.div`
  /* padding: 3rem; */
  width: 48%;
  height: 80%;
  background-color: #ffcc5a;
  border-radius: 64px;
  display: block;
  padding: 1rem;
  @media (max-width: 1080px) {
    border-radius: 30px;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 15rem;
    margin-bottom: 1rem;
  }
  @media (max-width: 500px) and (max-height: 760px) {
    width: 100%;
    height: 14rem;
    margin-bottom: 1rem;
  }
  @media (max-width: 350px) {
    height: 12rem;
  }
`
const ImgLeft = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 64px;
  object-fit: unset;
  @media (max-width: 1080px) {
    border-radius: 30px;
    object-fit: unset;
  }
`

const PannerRight = styled.div`
  /* padding: 3rem; */
  width: 48%;
  height: 80%;
  background-color: #ffcc5a;
  border-radius: 64px;
  display: block;
  padding: 1rem;
  @media (max-width: 1080px) {
    border-radius: 30px;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 15rem;
    margin-top: 1rem;
  }
  @media (max-width: 500px) and (max-height: 760px) {
    width: 100%;
    height: 14rem;
    margin-top: 1rem;
  }
  @media (max-width: 350px) {
    height: 12rem;
  }
`
const ImgRight = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 64px;
  object-fit: unset;
  @media (max-width: 1080px) {
    border-radius: 10px;
    object-fit: unset;
  }
`
const Footer = styled.div`
  height: 15rem;
  width: 100%;
  @media (max-width: 1080px) {
    height: 12rem;
  }
  @media (max-width: 500px) {
    height: 8rem;
  }
`
const Content1 = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-flow: column;
  border-top: 1px solid #707070;
  background: #ffcc5a;
  padding-top: 4rem;
`
const LineTop = styled.div`
  background: #74daf7;
  max-height: 70px;
  height: 10vw;
  width: 100%;
  @media (min-width: 600px) {
    height: 6vw;
  }
`
const ContainerTitle = styled.div`
  color: #fff;
  font-size: 2.5rem;
  font-weight: 900;
  z-index: 10;
  @media (max-width: 1300px) {
    font-size: 2rem;
  }
  @media (max-width: 500px) {
    font-size: 2rem;
  }
`
const LineImg = styled.img`
  width: 400px;
  margin-top: -22px;
  @media (max-width: 1500px) {
    width: 320px;
  }
`
const ImgBackTop = styled.img`
  width: 9rem;
  height: auto;
  @media (max-width: 800px) {
    width: 8rem;
  }
  @media (max-width: 500px) {
    width: 5rem;
  }
`
export {
  WrapperAlbion,
  ContainerAlbion,
  ContainerPanner,
  ContainerPrize,
  ContainerIntroduction,
  ContainerLottery,
  ContainerPannerBot,
  PannerLeft,
  PannerRight,
  Footer,
  Content1,
  LineTop,
  ContainerTitle,
  LineImg,
  ImgBackTop,
  ImgLeft,
  ImgRight,
  ImgHref
}
