import { styled } from 'baseui'

export const Wrapper = styled('div', () => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  margin: 'auto'
}))

export const ExportWrapper = styled('div', () => ({}))

export const ButtonWrapper = styled('div', () => ({
  position: 'fixed',
  top: 0,
  left: '20%',
  height: '60px',
  zIndex: 10,
  display: 'flex',
  width: '100%',
  alignItems: 'center',

  '@media only screen and (max-width: 1280px)': {
    left: '15%'
  },
  '@media only screen and (max-width: 1024px)': {
    left: '0.5rem'
  }
}))
export const PageWrapper = styled('div', () => ({
  position: 'relative',
  display: 'flex',
  flexDirection: 'column',
  // justifyContent: 'center',
  alignItems: 'center',
  height: '100vh'
}))

export const TitleWrapper = styled('div', () => ({
  marginTop: '1rem',
  width: '100%',
  height: '20%',
  justifyContent: 'center',
  display: 'flex',
  alignItems: 'center',
  fontSize: '2.8rem',
  fontWeight: 'bold',

  '@media only screen and (max-width: 1440px)': {
    fontSize: '2.5rem'
  },
  '@media only screen and (max-width: 1024px)': {
    fontSize: '4rem'
  },
  '@media only screen and (max-width: 768px)': {
    fontSize: '3rem'
  }
}))

export const EventWrapper = styled('div', () => ({
  width: '100%',
  height: '8%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  fontSize: '2.5rem',
  fontWeight: '600'

  // '@media only screen and (max-width: 1440px)': {
  //   marginTop: '4rem',
  //   fontSize: '2.5rem'
  // },
  // '@media only screen and (max-width: 1280px)': {
  //   marginTop: '3rem',
  //   fontSize: '2.5rem'
  // },
  // '@media only screen and (max-width: 1024px)': {
  //   marginTop: '8rem',
  //   fontSize: '4rem'
  // },
  // '@media only screen and (max-width: 768px)': {
  //   marginTop: '4rem',
  //   fontSize: '3rem'
  // }
}))
export const DateWrapper = styled('div', () => ({
  position: 'absolute',
  bottom: '4%',
  left: '0%',
  width: '100%',
  alignItems: 'center',
  display: 'flex',
  justifyContent: 'center',
  fontSize: '2rem',
  fontWeight: '600'

  // '@media only screen and (max-width: 1440px)': {
  //   marginTop: '4rem',
  //   fontSize: '2.5rem'
  // },
  // '@media only screen and (max-width: 1280px)': {
  //   marginTop: '3rem',
  //   fontSize: '2.5rem'
  // },
  // '@media only screen and (max-width: 1024px)': {
  //   marginTop: '8rem',
  //   fontSize: '4rem'
  // },
  // '@media only screen and (max-width: 768px)': {
  //   marginTop: '4rem',
  //   fontSize: '3rem'
  // }
}))
export const QRCodeWrapper = styled('div', () => ({
  alignItems: 'center',
  display: 'flex',
  justifyContent: 'center',
  margin: 'auto'

  // '@media only screen and (max-width: 1440px)': {
  //   marginTop: '4rem'
  // },
  // '@media only screen and (max-width: 1280px)': {
  //   marginTop: '3rem'
  // },
  // '@media only screen and (max-width: 1024px)': {
  //   marginTop: '8rem'
  // },
  // '@media only screen and (max-width: 768px)': {
  //   marginTop: '4rem'
  // }
}))
export const PrimaryHeaderText = styled('span', () => ({
  color: '#01BD87'
}))
export const PrimaryDateText = styled('span', () => ({
  // color: '#01BD87'
  color: 'black'
}))
