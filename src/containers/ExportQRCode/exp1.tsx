import { PrinterOutlined, ReloadOutlined } from '@ant-design/icons'
import { useQuery } from '@apollo/react-hooks'
import { Result, Spin } from 'antd'
import Button from '../../components/Button/Button'
import QRCode from 'qrcode.react'
import React, { useEffect, useState } from 'react'
// import { QUERY_EVENTS } from '../../graphql/query/events'
import image from '../../assets/images/backgroundQR.png'
import { useDeviceType } from '../../settings/useDeviceType'
import giftIcon from '../../assets/images/giftIcon.png'
import {
  ButtonWrapper,
  Wrapper,
  QRCodeWrapper,
  ExportWrapper,
  PageWrapper
} from './export.style'
// import {
//   EventWrapper,
//   TitleWrapper,
//   DateWrapper,
//   PrimaryHeaderText,
//   PrimaryDateText
// } from './export.style'
import dayjs from 'dayjs'
import { useLocation } from 'react-router-dom'
import queryString from 'query-string'
import { QUERY_EVENTS } from '../../graphql/query/events'
import { LANDING_PAGE } from '../../settings/constants'

function ExportQRCode() {
  const location = useLocation()
  const search = queryString.parse(location.search) ?? {}
  const { desktop, tablet } = useDeviceType(window.navigator.userAgent)
  console.log(search)

  const { data, loading, error, refetch } = useQuery(QUERY_EVENTS, {
    variables: {
      offset: parseInt(search.offset.toString()),
      limit: parseInt(search.limit.toString())
    },
    fetchPolicy: 'network-only'
  })
  let total = data?.events.count
  const eventsData =
    data?.events?.events
      .map((item, idx) => ({
        id: item.id,
        name: item.name,
        startTime: item.startTime,
        endTime: item.endTime,
        idx: idx + 1,
        link: `${window.location.origin}${LANDING_PAGE}?eventId=${item.id}`
      }))
      .filter((item) => {
        const text = item.name.toLowerCase()
        if (search.selected.toString() === 'all') {
          return (
            text.includes(search.search.toString().toLowerCase()) ||
            search.search.toString() === ''
          )
        } else {
          console.log(
            (text.includes(search.search.toString().toLowerCase()) ||
              search.search.toString() === '') &&
              JSON.parse(search.selected.toString()).includes(item.idx)
          )
          return (
            (text.includes(search.search.toString().toLowerCase()) ||
              search.search.toString() === '') &&
            JSON.parse(search.selected.toString()).includes(item.idx)
          )
        }
      }) || []
  const [hideButton, setHideButton] = useState(true)
  function handleButton() {
    setHideButton(false)
    setTimeout(() => {
      window.print()
      setHideButton(true)
    }, 500)
  }

  return (
    <Wrapper style={{ height: `calc(100% * ${total})` }}>
      <Spin
        size={'large'}
        spinning={loading}
        style={{ marginTop: loading ? '40vh' : 0 }}
      >
        {eventsData && (
          <ExportWrapper>
            {hideButton && (
              <ButtonWrapper>
                <Button onClick={handleButton}>
                  <PrinterOutlined />
                  &nbsp; 印刷
                </Button>
                {/* <Button
                  loading={loading}
                  icon={}
                  onClick={() => refetch()}
                >
                  Reload
                </Button> */}
              </ButtonWrapper>
            )}
            {eventsData?.map((item) => {
              return (
                <PageWrapper>
                  {/* <img
                    src={image}
                    alt={'オンライン抽選管理ページ'}
                    height='100%'
                    style={{ position: 'absolute', zIndex: -1 }}
                  />
                  <TitleWrapper>
                    オンライン
                    <PrimaryHeaderText>抽選管理ページ</PrimaryHeaderText>{' '}
                  </TitleWrapper>
                  <EventWrapper>{item.name}</EventWrapper>
                  <DateWrapper>
                    <PrimaryDateText>
                      {dayjs(item.startTime).format('YYYY.MM.DD')}
                    </PrimaryDateText>
                    &nbsp; - &nbsp;
                    <PrimaryDateText>
                      {dayjs(item.endTime).format('YYYY.MM.DD')}
                    </PrimaryDateText>
                  </DateWrapper> */}
                  <QRCodeWrapper>
                    <QRCode
                      id='qr-code'
                      value={item.link}
                      size={
                        tablet
                          ? 570
                          : desktop && window.innerWidth > 1280
                          ? 475
                          : 400
                      }
                      level={'H'}
                      includeMargin={false}
                      imageSettings={{
                        src: giftIcon,
                        x: null,
                        y: null,
                        height: 64,
                        width: 64,
                        excavate: true
                      }}
                    />
                  </QRCodeWrapper>
                </PageWrapper>
              )
            })}
          </ExportWrapper>
        )}{' '}
        {!loading && error && (
          <Result
            style={{ paddingTop: '25vh' }}
            status='404'
            title='イベントに賞品がありません。'
            // subTitle="イベントに賞品がありません。"
          />
        )}
      </Spin>
    </Wrapper>
  )
}

export default ExportQRCode
