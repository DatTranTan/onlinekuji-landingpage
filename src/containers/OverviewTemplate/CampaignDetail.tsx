/* eslint-disable */
import React, { useCallback, useState } from 'react'
// import { AuthContext, Role } from '../../context/auth'
import { useDrawerDispatch, useDrawerState } from '../../context/DrawerContext'
import {
    Form,
    DrawerTitleWrapper,
    DrawerTitle,
    FieldDetails,
    ButtonGroup,
    RequiredCol
  } from '../DrawerItems/DrawerItems.style'
import { QuillEditorWrapper } from './CampaignDetail.style'
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import  { CustomQuill } from "../../components/ToolbarQuill/CustomQuill";
import QuillToolbar, { modules, formats } from "../../components/ToolbarQuill/ToolbarQuill";


type Props = any

const UserForm: React.FC<Props> = (props) => {

  let { value } = useDrawerState('data') ?? null;
  console.log("detail: ", value);

  // value = '<p><strong class="avc">abcxyz</strong></p>'
  return (
    <QuillEditorWrapper>
      <CustomQuill>
       <QuillToolbar/>
        <ReactQuill
          theme="snow"
          modules={modules}
          formats={formats}
          readOnly={true}
          defaultValue={value ? value : ""}
          // value={value ? value : ""}
        />
      </CustomQuill>
    </QuillEditorWrapper>
  )
}
export default UserForm
