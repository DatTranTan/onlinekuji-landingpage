import styled from "styled-components";

export const QuillEditorWrapper = styled.div`
    border: 2px solid red;
    min-height: 80vh;
    .ql-toolbar.ql-snow {
        display: none !important;
    }
    .ql-container.ql-snow {
        border: none !important;
    }
`