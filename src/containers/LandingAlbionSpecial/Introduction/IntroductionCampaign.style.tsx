import styled from "styled-components";

export const QuillEditorWrapper = styled.div`
    .ql-toolbar.ql-snow {
        display: none !important;
    }
    .ql-container.ql-snow {
        border: none !important;
    }
`