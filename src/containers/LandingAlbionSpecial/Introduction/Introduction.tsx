import React from "react";
import {
  WrapperIntroduction,
  BlockTitle,
  ContainerTitle,
  Container,
  ContainerContent,
} from "./Introduction.style";
import {TITLEINFO} from '../constants/constant'

function Introduction(data) {
  const [timeOut, setTimeOut] = React.useState(0)
  React.useEffect(() => {
    if(data?.data?.event?.timeOut){
      setTimeOut(Number(data?.data?.event?.timeOut))
    }
  },[data])
  
  return (
    <Container>
      <BlockTitle>キャンペーン概要</BlockTitle>
      <WrapperIntroduction>
        <ContainerTitle>■内容</ContainerTitle>
        <ContainerContent>
          文化放送と文化放送パーソナリティが「もっと一緒に」感じられるスペシャルプレゼントが当たる！オンラインくじです。
          <br />
          イベント参加券あり、文化放送グッズあり、スペシャル体験あり、、、！１日１回運試しにぜひチャレンジください！！
        </ContainerContent>

        <ContainerTitle>■プレゼント</ContainerTitle>
        <ContainerContent>
          {TITLEINFO.week1.notification}
          <br/>
          {TITLEINFO.week1.products.map(item =>  <>{item} <br/></>)}
        </ContainerContent>

        <ContainerContent>
          {TITLEINFO.week2.notification}
          <br/>
          {TITLEINFO.week2.products.map(item =>  <>{item} <br/></>)}
        </ContainerContent>

        <ContainerContent>
          {TITLEINFO.week3.notification}
          <br/>
          {TITLEINFO.week3.products.map(item =>  <>{item} <br/></>)}
        </ContainerContent>

        <ContainerContent>
          {TITLEINFO.week4.notification}
          <br/>
          {TITLEINFO.week4.products.map(item =>  <>{item} <br/></>)}
        </ContainerContent>

        {/* <ContainerContent>
          「第４弾もお楽しみ♪」
        </ContainerContent> */}

        <ContainerTitle>■キャンペーン規約</ContainerTitle>
        <ContainerContent>
          株式会社文化放送（以下「当社」といいます。）が実施するその場で当たる！浜祭2021プレゼントキャンペーン（以下，「本キャンペーン」といいます。）へ応募を希望される方は，
          この応募規約（以下，「本規約」といいます。）をよくお読みの上，同意される場合のみ応募してください。なお，本キャンペーンに応募された方は，本規約に同意したものとみなします。
        </ContainerContent>

        <ContainerTitle>応募に関する注意事項</ContainerTitle>
        <ContainerContent>
          次の場合にはご応募は無効となります。
          <br />
          1.当社及びその関係会社の社員及びキャンペーンの関係者による応募の場合
          <br />
          2.キャンペーンサイトへの不正アクセス、メールアドレスの不正使用その他の不正行為を行った場合
          <br />
          3.プログラム等による自動応募と当社が判断した場合
          <br />
          4.その他、応募者が応募規約等又は本規約に違反した場合
          <br />
          ●キャンペーンへのご応募、お問い合わせにかかる通信料は、応募者のご負担となります。
          <br />
          ●機種・OS・ブラウザ等の理由により一部のPC、携帯電話、スマートフォン、タブレット等ではご応募いただけない場合もございます。
          <br />
          ●応募者の利用する通信端末や通信事業者の提供するサービスの不具合等により、キャンペーンへのご応募が行えない場合や、情報漏えいその他の損害が応募者や第三者に発生した場合においても、当社に故意又は過失がない限り当社は責任を負いかねますので、あらかじめご了承ください。
          <br />
          ●応募者からのアンケート回答内容等（以下「回答内容」といいます）の著作権は応募者に帰属します。ただし、当社及びその関係会社は、回答内容を応募者に個別に承諾を得ることなく、無償で期間の制限なく自由に利用（複製、改変、翻案、公衆送信びそのために必要な加工等を含み、これに限りません）することができ、また第三者に利用させることができるものとします。応募者は回答内容について著作者人格権を行使しないものとします。
          <br />
          {timeOut > 0 && `●当選時の応募情報の入力は時間制限がございます。${timeOut}分を越えての送信は当選無効となりますのでお早めにご入力・送信ください。`}
          <br />
        </ContainerContent>

        <ContainerTitle>当選に関する注意事項</ContainerTitle>
        <ContainerContent>
          プレゼントの発送先は、日本国内に限らせていただきます。
          <br />
          ●住所の誤記、転居先不明、長期不在等の理由によりプレゼントをお届けできない場合、当選は無効とさせていただきます。
          <br />
          ●当選の権利及びプレゼントを第三者へ譲渡・換金・転売することはできません。プレゼントはキャンペーンにご応募していただいたお礼となっておりますので、オークション等への出品もご遠慮ください。
          <br />
          ●景品表示法等の法令の規制により、同時期に実施する当社又は第三者が実施するキャンペーンと重複して当選できない場合がございます。
          <br />
          ●抽選後に応募者が応募規約等又は本規約に違反していたことが判明した場合、当選を無効とさせていただく場合がございます。
          <br />
        </ContainerContent>

        <ContainerTitle>個人情報の提供</ContainerTitle>
        <ContainerContent>
          本キャンペーンへの応募にあたり応募者の氏名や住所等を正確に提供して頂く必要がある場合，提供された内容の全部又は一部について，虚偽，誤記又は記載漏れがあった場合には，本キャンペーンの対象外とさせて頂くことがあります。
        </ContainerContent>

        <ContainerTitle>禁止事項</ContainerTitle>
        <ContainerContent>
          本キャンペーンへの応募に際しては，以下の行為を禁止します。当社は，応募者が以下に該当する行為を行ったと判断した場合には，事前に通知することなく，本キャンペーンから当該応募者を対象外としたり、又はその投稿データを一次的に非掲載とし若しくは削除することがあります。
          <br />
          ●本キャンペーンの運営を妨げる行為
          <br />
          ●本規約に違反する行為
          <br />
          ●他人に迷惑，不利益，損害，または不快感を与える行為
          <br />
          ●他人を誹謗中傷し，またはその名誉もしくは信用を害する行為
          <br />
          ●他人の著作権その他の知的財産権を侵害する行為
          <br />
          ●他人の財産，プライバシーもしくは肖像権を侵害する行為
          <br />
          ●営利を目的とした情報提供，広告宣伝もしくは勧誘行為
          <br />
          ●公序良俗に反する行為
          <br />
          ●その他当社が相当でないと判断する行為
          <br />
        </ContainerContent>

        <ContainerTitle>本サービスの提供の停止等</ContainerTitle>
        <ContainerContent>
          当社は，以下のいずれかの事由があると判断した場合，応募者に事前に通知することなく本キャンペーンを中断または中止することがあります。
          <br />
          ●本サービス実施にかかるコンピュータシステムの保守点検または更新を行う場合
          <br />
          ●地震，落雷，火災，停電または天災などの不可抗力により，本キャンペーンの実施が困難となった場合
          <br />
          ●コンピュータまたは通信回線等が事故により停止した場合
          <br />
          ●その他，当社が本キャンペーンの実施を困難と判断した場合
          <br />
          ●当社は，前項による本キャンペーンの中断または中止により，応募者が被った不利益または損害について，一切の責任を負わないものとします。
        </ContainerContent>

        <ContainerTitle>キャンペーン内容の変更等</ContainerTitle>
        <ContainerContent>
          当社は，応募者に予告することなく，本キャンペーンの内容の一部または全部を変更し，もしくは本サービスの提供を中止することがあります。当社は，これによって応募者に生じた損害について一切の責任を負いません。
        </ContainerContent>

        <ContainerTitle>個人情報の取り扱い</ContainerTitle>
        <ContainerContent>
          本キャンペーンにあたってご提供いただいた個人情報は，以下の目的でのみ使用いたします。
          <br />
          ●当選者への賞品の発送
          <br />
          ●上記に関する連絡
          <br />
          ●商品やサービスの参考とするための個人を特定しない統計情報の形での利用
          本キャンペーンにあたってご提供いただいた個人情報は，応募者ご本人の同意がある場合を除いて，第三者に提供されることはありません。
          <br />
        </ContainerContent>

        <ContainerTitle>通知または連絡</ContainerTitle>
        <ContainerContent>
          当社から応募者に対する通知または連絡は，当社の定める方法で行うものとし，応募者のメールアドレスの不備等により通知または連絡が到達しなかった場合には，本キャンペーンの対象外とさせて頂くことがあります。
        </ContainerContent>
      </WrapperIntroduction>
    </Container>
  );
}

export default Introduction;
