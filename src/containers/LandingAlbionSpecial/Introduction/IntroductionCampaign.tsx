import React from "react";
import {
  WrapperIntroduction,
  BlockTitle,
  ContainerTitle,
  Container,
  ContainerContent,
} from "./Introduction.style";
import {TITLEINFO} from '../constants/constant'
import { QuillEditorWrapper } from './IntroductionCampaign.style';
import  { CustomQuill } from "../../../components/ToolbarQuill/CustomQuill";
import QuillToolbar, { modules, formats } from "../../../components/ToolbarQuill/ToolbarQuill";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

const IntroductionCampaign = ({ data }) => {
    const [dataCamp, setDataCamp] = React.useState(null)
    React.useEffect(() => {
        if(data?.event?.campaign){
            setDataCamp(data?.event?.campaign?.content)
        }
    },[data])
    return (
        <Container>
            {/* <div dangerouslySetInnerHTML={{ __html: dataCamp }}/> */}
            <QuillEditorWrapper>
                <CustomQuill>
                <QuillToolbar/>
                    <ReactQuill
                        theme="snow"
                        readOnly={true}
                        modules={modules}
                        formats={formats}
                        value={dataCamp ? dataCamp : ""}
                    />
                </CustomQuill>
            </QuillEditorWrapper>
        </Container>
    );
}

export default IntroductionCampaign;
