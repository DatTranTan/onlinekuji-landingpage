/* eslint-disable */
import React, { useState, useEffect } from 'react'
import {
  WrapperPrize,
  ContainerPrizeB,
  ContainerPanner,
  ContainerPrize,
  ContainerIntroduction,
  ContainerLottery,
  ContainerPannerBot,
  PannerLeft,
  PannerRight,
  Footer,
  Content1,
  LineTop,
  ContainerTitle,
  LineImg,
  ImgBackTop,
  ImgLeft,
  ImgRight,
  ImgHref,
  ContainerTitleSub,
  ContainerTitleSubSpin, Div100vh, WrapperLucky, WrapperCard, ButtonToForm
} from './Prize.style'
import PrizeAlbion from '../../../components/PrizeAlbion/PrizeAlbion'
import PrizeBanner from './PrizeBanner/PrizeBanner'
import BtnRedirectToForm from '../Lottery/LuckyIpad/BtnRedirectToForm'
// import { QUERY_EVENT } from '../../graphql/query/event'
// import { useMutation, useQuery } from '@apollo/react-hooks'
// import { useLocation } from 'react-router-dom'
// import queryString from 'query-string'
// import Line1 from './img/line1.png'
// import Introduction from './Introduction/Introduction'
import Lottery from '../Lottery/Lottery'
// import Line4 from './img/line4.png'
import { BackTop, Card } from 'antd'
// import { DivBackTop } from '../LotteryPage/Spinning/LuckyOpenIpad.style'
// import BackToTop from './img/backtobutton.png'
// import { useEffect } from 'react'
// import { MUTATION_VISIT } from '../../graphql/mutation/total-visit'
import ImageNull from '../../../assets/images/box2.png'
// const [idSpin, setIdSpin] = useState()

const { Meta } = Card;
function Prize() {
  useEffect(() => {

  }, [])

  return (
    <WrapperPrize>
      <ContainerPrizeB>
        <ContainerLottery>
          <WrapperLucky>
            <ContainerTitle>その場で当たる！浜祭2021プレゼントキャンペーン！</ContainerTitle>
            <Div100vh>
              <WrapperCard>
                <ContainerTitleSub>ご当選おめでとうございます！</ContainerTitleSub>
                <Card
                  hoverable
                  style={{ width: '100%', height: '100%' }}
                  cover={
                    <img
                      // id='imageid'
                      src={ImageNull}
                      alt='imagePrize'
                      style={{
                        width: '100%',
                        height: '320px',
                        objectFit: 'unset'
                      }}
                    />
                  }
                >
                  <Meta
                    title={
                      <h2 style={{ color: 'black', fontWeight: 900, textAlign: 'center' }}>AAAAAAAAA</h2>}
                    style={{ color: 'black', fontSize: '1rem' }}
                    description="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt placeat accusamus,
                   deleniti animi debitis ipsum. Itaque, ratione distinctio obcaecati nihil tempora eum!
                    Magni quisquam amet ad tempore rerum consectetur molestiae?" />
                </Card>
                <ButtonToForm
                >
                  プレゼント発送情報を入力
                </ButtonToForm>
              </WrapperCard>
            </Div100vh>
          </WrapperLucky>
        </ContainerLottery>

      </ContainerPrizeB>
    </WrapperPrize>
  )
}

export default Prize
