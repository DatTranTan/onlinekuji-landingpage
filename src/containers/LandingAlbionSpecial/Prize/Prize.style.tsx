import styled from 'styled-components'
import { isDesktop } from 'react-device-detect'
import { Button } from 'antd'

const WrapperPrize = styled.div`
  height: 300vh;
`
const ContainerPrizeB = styled.div`
  background: #74daf7;
`
const ContainerPanner = styled.div`
  width: 100%;
  border: 1;
  background-color: #fff;
  border: 1px solid #707070;
`
const ContainerPrize = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  justify-content: center;
  background: linear-gradient(#DDFDFE, #fff 116%);
  border: 1px solid #707070;
  border-top: none;
  padding-bottom: 5%;
  @media (max-width: 500px) {
    padding-bottom: 8%;
    justify-content: space-around;
  }
`
const ContainerIntroduction = styled.div`
  padding-bottom: 8rem;
  @media (max-width: 500px) {
    padding-bottom: 4rem;
  }
`
const ContainerLottery = styled.div`
/* height: 130vh; */
  width: 100%;
  margin-top: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 1100px) and (max-height: 1400px) {
    /* height: 100vh; */
    height: auto;
  }
  @media (max-width: 800px) {
    height: 130vw;
  }
  @media (max-width: 500px) {
    height: 90vh;
  }
`
const ContainerPannerBot = styled.div`
  background-color: #fff;
  height: 80vh;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: 3.5rem;
  padding-right: 3.5rem;
  @media (max-width: 1500px) and (min-height: 800px) {
    height: 60vh;
  }
  @media (max-width: 1400px) and (min-height: 950px) {
    height: 57vh;
  }
  @media (max-width: 1100px) {
    height: 32vh;
  }
  @media (max-width: 1100px) and (max-height: 800px) {
    height: ${isDesktop ? '75vh' : '50vh'};
  }
  @media (max-width: 1100px) and (max-height: 420px) {
    height: ${isDesktop ? '75vh' : '80vh'};
  }
  @media (max-width: 500px) {
    /* padding-left: 2rem;
    padding-right: 2rem; */
    padding: 2rem;
    height: 72vh;
    flex-flow: column;
    justify-content: center;
  }

  @media (max-width: 500px) and (max-height: 760px) {
    /* padding-left: 2rem;
    padding-right: 2rem; */
    padding: 2rem;
    height: 82vh;
    flex-flow: column;
    justify-content: center;
  }
`
const ImgHref = styled.a`
.top-container{
  display:flex;
  background-color: #EA5514;
}
.top-container-item-left{
  width: 55%;
}
.top-container-item-right{
  width: 45%;
  display: flex;
  color: white;
  font-size: 3rem;
  font-weight: 600;
  div{
    margin: auto;
    text-align: center;
  }
}
`

const PannerLeft = styled.div`
  /* padding: 3rem; */
  width: 48%;
  height: 80%;
  background-color: #ffcc5a;
  border-radius: 64px;
  display: block;
  padding: 1rem;
  @media (max-width: 1080px) {
    border-radius: 30px;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 15rem;
    margin-bottom: 1rem;
  }
  @media (max-width: 500px) and (max-height: 760px) {
    width: 100%;
    height: 14rem;
    margin-bottom: 1rem;
  }
  @media (max-width: 350px) {
    height: 12rem;
  }
`
const ImgLeft = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 64px;
  object-fit: unset;
  @media (max-width: 1080px) {
    border-radius: 30px;
    object-fit: unset;
  }
`

const PannerRight = styled.div`
  /* padding: 3rem; */
  width: 48%;
  height: 80%;
  background-color: #ffcc5a;
  border-radius: 64px;
  display: block;
  padding: 1rem;
  @media (max-width: 1080px) {
    border-radius: 30px;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 15rem;
    margin-top: 1rem;
  }
  @media (max-width: 500px) and (max-height: 760px) {
    width: 100%;
    height: 14rem;
    margin-top: 1rem;
  }
  @media (max-width: 350px) {
    height: 12rem;
  }
`
const ImgRight = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 64px;
  object-fit: unset;
  @media (max-width: 1080px) {
    border-radius: 10px;
    object-fit: unset;
  }
`
const Footer = styled.div`
  height: 15rem;
  width: 100%;
  @media (max-width: 1080px) {
    height: 12rem;
  }
  @media (max-width: 500px) {
    height: 8rem;
  }
`
const Content1 = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-flow: column;
  border-top: 1px solid #707070;
  background: #DDFDFE;
  padding-top: 4rem;
`
const LineTop = styled.div`
  background: #74daf7;
  max-height: 70px;
  height: 10vw;
  width: 100%;
  @media (min-width: 600px) {
    height: 6vw;
  }
`
const ContainerTitleSub = styled.div`
  color: #DC8343;
  font-size: 2rem;
  font-weight: 900;
  z-index: 10;
  margin-bottom: 30px;
  margin-top: 30px;
  @media (max-width: 1300px) {
    font-size: 2rem;
  }
  @media (max-width: 500px) {
    font-size: 2rem;
  }
`

const ContainerTitleSubSpin = styled.div`
  color: black;
  font-size: 1.5rem;
  font-weight: 900;
  z-index: 10;
  margin-top: 60px;
  text-align: center;
  @media (max-width: 1300px) {
    font-size: 2rem;
  }
  @media (max-width: 500px) {
    font-size: 2rem;
  }
`
const ContainerTitle = styled.div`
text-align:left;
  color: black;
  font-size: 30px;
  font-weight: 900;
  margin-top:26px;
  margin-left:138px;
  z-index: 10;
  @media (max-width: 1300px) {
    font-size: 2rem;
  }
  @media (max-width: 500px) {
    font-size: 2rem;
  }
`
const LineImg = styled.img`
  width: 400px;
  margin-top: -22px;
  @media (max-width: 1500px) {
    width: 320px;
  }
`
const ImgBackTop = styled.img`
  width: 9rem;
  height: auto;
  @media (max-width: 800px) {
    width: 8rem;
  }
  @media (max-width: 500px) {
    width: 5rem;
  }
`
const Div100vh = styled.div`
  height: 150vh;
  background-color: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 30px;
  border: 10px solid #c3ecff;
  @media (max-width: 1100px) and (max-height: 1400px) {
    /* height: 66vh; */
    /* height: auto; */
  }
  @media (max-width: 850px) {
    height: ${(props) => (props.nowin === true ? '60vw' : '100vw')};
  }
  @media (max-width: 500px) {
    /* height: ${(props) => (props.showPrize === false ? '' : '550px')}; */
    height: ${(props) => (props.nowin === true ? '250px' : '550px')};
  }
`
const WrapperLucky = styled.div`
  width: 100%;
  padding-left: 3.5rem;
  padding-right: 3.5rem;
  font-family: Arial;
  /* background-color: yellow; */
  @media (max-width: 500px) {
    display: flex;
    flex-flow: column;
    justify-content: space-around;
    height: 100%;
    width: 100%;
    padding-left: 1rem;
    padding-right: 1rem;
  }
`
const WrapperCard = styled.div`
  max-width: 35rem;
  width: 40%;
  height: 40rem;
  /* border: 1px solid #707070; */
  /* border-radius: 37px; */
  background-color: #fff;
  display: flex;
  justify-content: space-around;
  align-items: center;
  flex-flow: column;
  margin-top: 5%;
  margin-left: 3%;
  margin-right: 3%;
  /* margin-bottom: 5%; */
  /* box-shadow: 9px 10px rgb(121 103 103 / 23%); */
  @media (max-width: 1650px) {
    margin-left: 2rem;
    margin-right: 2rem;
    margin-top: 0px;
    height: 60rem;
    /* margin-bottom: 4.5rem; */
  }

  @media (max-width: 1300px) {
    margin-top: 3rem;
    width: 35%;
    height: 27rem;
    /* margin-bottom: 3rem; */
  }
  @media (max-width: 1110px) {
    height: 24rem;
    }
    @media (max-width: 950px) {
      height: ${isDesktop ? '20rem' : '22rem'}
    }
  @media (max-width: 850px) {
    width: 40%;
    height: 27rem;
  }
  @media (max-width: 500px) {
    width: 45%;
    margin-left: 0;
    margin-right: 0;
    height: 14rem;
    border-radius: 20px;
    :first-child {
      margin-top: 3rem;
    }
    :nth-child(2) {
      margin-top: 3rem;
    }
    margin-top: 1.2rem;
  }
`
const ButtonToForm = styled(Button)`
    text-align: center;
    margin: auto;
    height: 46px;
    width: 228px;
  border-radius: 100px;
  color: white;
  background-color: #DC8343;
  font-size: 1.5rem;
  font-weight: 400;
  border: none !important;
  &:hover {
    border: 1px solid red !important;
    color: red;
  }  
`
export {
  WrapperPrize,
  ContainerPrizeB,
  ContainerPanner,
  ContainerPrize,
  ContainerIntroduction,
  ContainerLottery,
  ContainerPannerBot,
  PannerLeft,
  PannerRight,
  Footer,
  Content1,
  LineTop,
  ContainerTitle,
  LineImg,
  ImgBackTop,
  ImgLeft,
  ImgRight,
  ImgHref,
  ContainerTitleSub,
  ContainerTitleSubSpin,
  Div100vh,
  WrapperLucky,
  WrapperCard,
  ButtonToForm
}
