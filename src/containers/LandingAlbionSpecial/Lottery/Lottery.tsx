import React from 'react'
import { ContainerLottery } from './Lottery.style'
import LuckyIpad from './LuckyIpad/LuckyIpad'

function Lottery({dataOrderPrizes}) {
  return (
      <LuckyIpad dataOrderPrizes={dataOrderPrizes}/>
  )
}

export default Lottery
