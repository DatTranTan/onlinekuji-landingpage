/* eslint-disable */
import React from "react";
import "../../../style/css/reset.css";
import "../../../style/css/common.css";
import "../../../style/css/sp.css";
import "../../../style/css/tab.css";
import "../../../style/css/pc.css";
import ImgWin from "../../../style/img/lottery/img_bg_lottery01.jpg";
import ImgLost2 from "../../../style/img/lottery/img_lottery_off.png";
import ImgHeader from "../../../style/img/lottery/txt_hd01.svg";
import ImgLost from "../../../style/img/lottery/txt_hd02.svg";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";

var genders = [
  { label: "男性", id: 0 },
  { label: "女性", id: 1 },
  { label: "回答しない", id: 2 },
];
interface EventFormInput {
  name: string;
  contact_number: string;
  email: string;
  reemail: string;
  address: string;
  thirdRequired: string;
  thirdNonRequired: string;
}

type Props = any;
const FormPrize: React.FC<Props> = (props) => {
  const location = useLocation();
  console.log('11111',location)
  const event = queryString.parse(location.search).eventId;
  return (
    <>
      <>
        <div id="wrapper">
          <article>
            <div id="contents">
              <div className="contactForm">
                <div className="inner">
                  <div className="bg">
                    <h2 className="hdM">送信完了</h2>
                    <p className="thanks">ご応募ありがとうございました。</p>
                    <div className="teamBtns">
                      <a href={`landing-page?eventId=${event}`} className="btnLink">
                        ホームに戻る
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* /#contents */}
          </article>
        </div>
        {/* /#wrapper */}
      </>
    </>
  );
};
export default FormPrize;
