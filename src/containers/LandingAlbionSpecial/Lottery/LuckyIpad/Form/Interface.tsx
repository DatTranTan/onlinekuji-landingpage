export interface RegisterCustomerForm {
  name: string
  age: string
  gender: string
  contact_number: string
  email: string
  reemail: string
  address: string
  postalCode: string
  thirdRequired: string
  thirdNonRequired: string
}
