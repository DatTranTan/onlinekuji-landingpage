/* eslint-disable */
import { useQuery } from "@apollo/react-hooks";
import { Checkbox, Modal, Radio, Row, Col } from "antd";
import queryString from "query-string";
import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import {
  Error,
  FormFields,
  FormLabel,
} from "../../../../../../components/FormFields/FormFields";
// import Input from '../../../components/Input/Input'
import Input from "../../../../../../components/Input/Input";
import { QUERY_EVENT } from "../../../../../../graphql/query/event";
const { confirm } = Modal;

type Props = any;
const Questionnaire: React.FC<Props> = (props) => {
  const { data, index, setValue, register, errors, Controller, control } =
    props;
  const [content, setContent] = useState([]);
  const [selected, setSelected] = useState([]);
  useEffect(() => {
    if (data) {
      const list = data.content.split(",");
      setContent(list);
    }
  }, [data]);

  return (
    <>
      <tr>
        <th>
          {data.question}
          <span className="colorRed">*</span>
        </th>
        <td>
          <div className="checkboxStyle">
            {content.map((itemQ, indexQ) => {
              return (
                <label>
                  <input type="checkbox" 
                  value={itemQ} 
                  name={`content${index}`}
                  ref={register({
                    required: true,
                  })}
                  />
                  {itemQ}
                </label>
              );
            })}
          </div>
          {errors[`content${index}`] &&
          errors[`content${index}`].type === "required" && (
            <Error>これは必須の項目です。</Error>
          )}
        </td>
      </tr>
    </>
  );
};
export default Questionnaire;
