/* eslint-disable */
import { useQuery } from "@apollo/react-hooks";
import { Checkbox, Modal, Radio } from "antd";
import queryString from "query-string";
import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import {
  Error,
  FormFields,
  FormLabel,
} from "../../../../../../components/FormFields/FormFields";
// import Input from '../../../components/Input/Input'
import Input from "../../../../../../components/Input/Input";
import { QUERY_EVENT } from "../../../../../../graphql/query/event";
const { confirm } = Modal;

type Props = any;
const Questionnaire: React.FC<Props> = (props) => {
  const { data, index, setValue, register, errors } = props;

  return (
    <>
      <tr>
        <th>
          {data.question}
          {data.content.toUpperCase() === "TRUE" && (
            <>
              <span className="colorRed">*</span>
            </>
          )}
        </th>
        <td >
          <textarea defaultValue={""} 
          ref={register({
            required: data.content.toUpperCase() === "TRUE",
          })}
          name={`content${index}`}
          />
          {errors[`content${index}`] &&
          errors[`content${index}`].type === "required" && (
            <Error>これは必須の項目です。</Error>
          )}
        </td>
      </tr>
    </>
  );
};
export default Questionnaire;
