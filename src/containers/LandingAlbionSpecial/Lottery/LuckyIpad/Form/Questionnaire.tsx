/* eslint-disable */
import { useQuery } from "@apollo/react-hooks";
import { Modal, Row, Col } from "antd";
import queryString from "query-string";
import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import {
  FormFields,
  FormLabel,
} from "../../../../../components/FormFields/FormFields";
import { QUERY_EVENT } from "../../../../../graphql/query/event";
import TypeFirst from "./Components/TypeFirst";
import TypeSecond from "./Components/TypeSecond";
import TypeThird from "./Components/TypeThird";

const { confirm } = Modal;

type Props = any;
const Questionnaire: React.FC<Props> = (props) => {
  const {
    setValue,
    register,
    errors,
    control,
    Controller,
    formData,
    isConfirm,
    questionList,
  } = props;

  return (
    <>
      {questionList?.map((item, index) => {
        return (
          <>
            <div style={{ display: isConfirm ? "block" : "none" }}>
              <table className="full">
                <tbody>
                  {item.type === "First" && (
                    <TypeFirst
                      formData={formData}
                      data={item}
                      index={index}
                      errors={errors}
                      register={register}
                      setValue={setValue}
                      Controller={Controller}
                      control={control}
                    />
                  )}
                  {item.type === "Second" && (
                    <TypeSecond
                      formData={formData}
                      data={item}
                      index={index}
                      errors={errors}
                      register={register}
                      setValue={setValue}
                      Controller={Controller}
                      control={control}
                    />
                  )}
                  {item.type === "Third" && (
                    <TypeThird
                      formData={formData}
                      data={item}
                      index={index}
                      errors={errors}
                      register={register}
                      setValue={setValue}
                    />
                  )}
                </tbody>
              </table>
            </div>
            <tr style={{ display: !isConfirm ? "table-row" : "none" }}>
              <th>{item?.question}</th>
              <td style={{wordBreak:'break-all'}}>
                {typeof formData?.[`content${index}`] == "string"
                  ? formData?.[`content${index}`]
                  : formData?.[`content${index}`]?.join()}
              </td>
            </tr>
          </>
        );
      })}
    </>
  );
};
export default Questionnaire;
