/* eslint-disable */
import React, { useState } from "react";
import {
  Wrapper,
  TextHeader,
  BlockCenter,
  BlockButton,
  ButtonRight,
  BlockVideo,
  WrapperVideo,
  WrapperPrize,
  BlockImg,
  VideoXXX,
  Div100vh,
  // ButtonForm,
  Ablank,
  DivText,
  ImgArrow,
  SvgStyle,
  PrizeContainer,
  ButtonLost,
} from "./LuckyIpad.style";
import { useMutation, useQuery } from "@apollo/client";
import { useLocation } from "react-router-dom";
import { MUTATION_SPIN } from "../../../../graphql/mutation/spin";
import { Notification } from "../../../../components/Notification/NotificationCustom";
import { QUERY_EVENT } from "../../../../graphql/query/event";
import BackgroundNoPrize from "./VideoPrize/image/background_hazure.png";
import GiftBackground from "./VideoPrize/image/item.png";
import Prize from "./VideoPrize/image/prize.png";
import * as qs from "query-string";
import { Spin, Button } from "antd";
import { use100vh } from "react-div-100vh";
import { v4 as uuidv4 } from "uuid";
import Cookies from "universal-cookie";
import {
  MUTATION_INCREASE_WIN,
  MUTATION_SPUN,
} from "../../../../graphql/mutation/spun";
import Forward from "./VideoPrize/image/forward.png";
import Gift from "../../../../assets/images/box1.png";
import BtnRedirectToForm from "./BtnRedirectToForm";
import PrizeAlbion from "../../../../components/PrizeAlbion2/PrizeAlbionSpin";
import BackgroundBannerSmall from "../../img/hamamacho/banner-footer.png";
import NoPrize from "../../img/hamamacho/lose.png";
import PRODUCTSINFO from "../../constants/constant";
import RoleBackground from "../../img/hamamacho/role-background.png";
import ImgHeader from "../../style/img/top/kv.jpg";
import ImgSource from "../../style/img/top/kv_sp.jpg";
import ImgSample from "../../style/img/top/img_freebie01.png";
import ImgLottery from "../../style/img/common/background_white.png";
import ImgPageTop from "../../style/img/common/pagetop.svg";
import "../../style/css/reset.css";
import "../../style/css/common.css";
import "../../style/css/sp.css";
import "../../style/css/tab.css";
import "../../style/css/pc.css";

function LuckyIpad({ dataOrderPrizes }) {
  const NewID = uuidv4();
  const cookies = new Cookies();
  let customerID = "";
  async function clickCookies() {
    let newDate = await cookies.get("roleDay");
    let newCustomerId = await cookies.get("userID");
    if (newCustomerId) {
      if (newDate == new Date().getDay()) {
        customerID = newCustomerId;
      } else {
        customerID = NewID;
        cookies.set("userID", NewID);
        cookies.set("roleDay", new Date().getDay());
      }
    } else {
      customerID = NewID;
      cookies.set("userID", NewID);
      cookies.set("roleDay", new Date().getDay());
    }
  }
  // const myFunction = () => {
  //   let element;
  //   window.innerWidth > 1050
  //     ? (element = document.getElementById("ViewVideo"))
  //     : (element = document.getElementById("ViewVideoMobile"));
  //   element.scrollIntoView();
  // };

  const height = use100vh();
  const location = useLocation();
  const eventId = qs.parse(location.search).eventId;
  const [showPrize, setShowPrize] = React.useState(true);
  const [play, setPlay] = useState(false);
  const [dataSpinByEvent, setDataSpinByEvent] = useState<any>();
  // const [idspin, setIdspin] = useState('')
  const [visibleButton, setVisibleButton] = useState(false);
  const [idSpin, setIdSpin] = useState();
  const [winPrize, setWinPrize] = useState(false);
  // const [onClickButton, setOnClickButton] = useState(false)
  const videoRef = React.useRef<HTMLVideoElement>(null);

  const [videoDimension, setVideoDimension] = useState({
    width: 0,
    height: 0,
  });
  const [loadingVideo, setLoadingVideo] = useState(true);
  const [customerLost, setCustomerLost] = useState("");
  const [style, setStyle] = React.useState({});
  const [showVideo, setShowVideo] = useState("");
  const [onStartVideo, setOnStartVideo] = useState(false);
  const [spinCircle, { loading }] = useMutation(MUTATION_SPIN, {
    fetchPolicy: "no-cache",
  });

  const [checkSpun] = useMutation(MUTATION_SPUN, {
    fetchPolicy: "no-cache",
  });
  const [increaseTotalWin] = useMutation(MUTATION_INCREASE_WIN, {
    fetchPolicy: "no-cache",
  });

  const { data, loading: loader } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventId,
    },
  });
  const reloadButton = () => {
    checkSpun({
      variables: {
        eventId,
        userId: customerID,
      },
    }).then(({ data }) => {
      // console.log(data, 'checkSpun')
      data?.checkSpun && setPlay(false);
      setLoadingVideo(false);
    });
    spinCircle({
      variables: {
        eventId,
        userId: customerID,
      },
    })
      .then(({ data }) => {
        setCustomerLost(data?.spin?.event?.customerLost);
        setDataSpinByEvent(data.spin);
        setIdSpin(data?.spin?.SpinPrizeID);
        if (
          data.spin.rank !== 0 ||
          (data.spin.rank === 0 && data.spin.event.customerLost === "Win")
        ) {
          setWinPrize(true);
          increaseTotalWin({
            variables: {
              eventId,
            },
          });
        }
        if (data.spin.rank === 0) {
          setWinPrize(true);
        }

        // setIdspin(data.spin.id)
      })
      // .then(() => window.location.reload())
      .catch(({ graphQLErrors }) =>
        // Notification({
        //   type: 'error',
        //   message: ' エラー',
        //   description: graphQLErrors[0]?.message
        // })
        {
          const mess = graphQLErrors.map((item) => item.message);
          console.log("graphQLErrors", mess);
          if (mess.includes("このイベントはまだ開始されていません")) {
            Notification({
              type: "error",
              message: " エラー",
              description: "イベントはまだ開始されていません。",
            });
            setShowPrize(true);
            setPlay(false);
          } else if (mess.includes("このイベントは既に終了しました")) {
            Notification({
              type: "error",
              message: " エラー",
              description: "このイベントは既に終了しました",
            });
            setShowPrize(true);
            setPlay(false);
          } else if (mess[0].includes("抽選しました")) {
            Notification({
              type: "error",
              message: " エラー",
              description: "本日は抽選済みです！１日１回チャレンジできます。",
            });
            setShowPrize(true);
            setPlay(false);
          } else {
            Notification({
              type: "error",
              message: " エラー",
              description: graphQLErrors[0]?.message,
            });
          }
        }
      );
    // .catch(({ graphQLErrors }) => {
    //   const mess = graphQLErrors.map((item) => item.message)
    //   if (mess.includes('このイベントはまだ開始されていません')) {
    //     Notification({
    //       type: 'error',
    //       message: ' エラー',
    //       description: 'イベントはまだ開催されていません。'
    //     })
    //     setShowPrize(true)
    //     setPlay(false)
    //   } else if (mess.includes('このイベントは既に終了しました')) {
    //     Notification({
    //       type: 'error',
    //       message: ' エラー',
    //       description: 'このイベントは既に終了しました'
    //     })
    //     setShowPrize(true)
    //     setPlay(false)
    //   } else {
    //     Notification({
    //       type: 'error',
    //       message: ' エラー',
    //       description: graphQLErrors[0]?.message
    //     })
    //   }
    // })
    // console.log(data, 'CheckSpin')
    setVisibleButton(false);
  };

  React.useEffect(() => {
    if (videoDimension?.width && videoDimension?.height) {
      if (videoDimension.width > videoDimension.height) {
        setStyle({ width: "100%", height: "auto", position: "absolute" });
      } else setStyle({ width: "100%", height: "auto", position: "absolute" });
    }
  }, [videoDimension]);

  React.useEffect(() => {
    if (dataSpinByEvent) {
      let video;
      if (
        data?.event.video !== null &&
        data?.event.video !== undefined &&
        dataSpinByEvent?.rank !== 0
      ) {
        video = data?.event.video?.url;
      } else if (
        data?.event.video !== null &&
        data?.event.video !== undefined &&
        dataSpinByEvent?.rank === 0
      ) {
        video =
          data?.event.noPrizeVideo === null ||
          data?.event.noPrizeVideo?.url === ""
            ? data?.event.video?.url
            : data?.event.noPrizeVideo.url;
      } else {
        video = "https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4";
      }
      setShowVideo(video);
    }
  }, [dataSpinByEvent]);

  // React.useEffect(() => {
  //     let virtualElement = document.querySelector('myVideo')
  //     virtualElement.setAttribute('src', showVideo)
  //     if(!isIOS){
  //       virtualElement.addEventListener('loadeddata', () => {
  //         if(virtualElement.offsetWidth && virtualElement.offsetHeight){
  //           setVideoDimension({
  //             width: virtualElement.offsetWidth,
  //             height: virtualElement.offsetHeight
  //           })
  //           setLoadingVideo(false)
  //           virtualElement.remove()
  //         }
  //       })
  //     } else {
  //       setVideoDimension({
  //         width: virtualElement.offsetWidth,
  //         height: virtualElement.offsetHeight
  //       })
  //       setLoadingVideo(false)
  //       virtualElement.remove()
  //     }
  //     document.getElementById('root').appendChild(virtualElement)
  // }, [showVideo])

  React.useEffect(() => {
    if (showVideo.length > 0 && dataSpinByEvent && videoRef.current) {
      videoRef.current.play();
      videoRef.current.muted = false;
    }
  }, [showVideo, dataSpinByEvent, videoRef]);

  const BannerPrize = () => (
    <div
      style={{
        background: "url(" + Prize + ")",
        backgroundRepeat: "no-repeat",
        width: "100%",
        height: "50%",
        objectFit: "cover",
        backgroundPosition: "center",
        backgroundSize: "contain",
      }}
    ></div>
  );

  const handleEndVideo = () => {
    // setShowPrize(false);
    // setTimeout(() => {
    //   setPlay(false);
    //   setVisibleButton(true);
    // }, 0);
    if (dataSpinByEvent) {
      localStorage.setItem("prize", "true");
      localStorage.setItem("prizeRank", dataSpinByEvent?.rank);
      localStorage.setItem("customerLost", customerLost);
      window.open(
        `${window.location.origin}/lottery?spin=${idSpin}&event=${eventId}`,
        "_self"
      );
    }
  };

  return (
    <Wrapper>
      <div id="lottery" className="lotteryStart">
        <h2 className="hdL small">
          <span className="jap font_cp_reven">
            ボタンを押して
            <br />
            抽選スタート！
          </span>
        </h2>
        <div className="inner">
          {play === false ? (
            <>
              <div className="lottery">
                <img
                  src={data?.event?.videoBackground || ImgLottery}
                  alt="抽選"
                />
              </div>
              <form
                method="post"
                action="lottery_off.html"
                id="form"
                name="form"
              >
                <p className="button">
                  <button
                    className="font_cp_reven"
                    disabled={play}
                    onClick={async () => {
                      await clickCookies();
                      reloadButton();
                      setShowPrize(true);
                      setPlay(true);
                      // myFunction();
                    }}
                  >
                    抽選スタート
                  </button>
                </p>
              </form>
            </>
          ) : (
            <>
              {loading || loadingVideo ? (
                <div className="lottery">
                  <img
                    src={data?.event?.videoBackground || ImgLottery}
                    alt="抽選"
                  />
                  <Spin
                    style={{
                      zIndex: 2,
                      position: "absolute",
                      top: "50%",
                      left: "50%",
                    }}
                  />
                </div>
              ) : (
                <div className="lottery">
                  <video
                    id="myVideo"
                    onPlay={() =>
                      setTimeout(() => {
                        setOnStartVideo(true);
                      }, 1000)
                    }
                    onLoadedData={() => {
                      setVideoDimension({
                        width: videoRef.current.offsetWidth,
                        height: videoRef.current.offsetHeight,
                      });
                      setLoadingVideo(false);
                    }}
                    ref={videoRef}
                    src={showVideo}
                    // src={"https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4"}
                    autoPlay={true}
                    playsInline={true}
                    muted={true}
                    preload="yes"
                    controls={false}
                    onEnded={() => handleEndVideo()}
                  />
                </div>
              )}
            </>
          )}
        </div>
      </div>
    </Wrapper>
  );
}

export default LuckyIpad;
