import React from "react";
import { Ablank, ButtonToForm } from "./LuckyIpad.style";

export default function BtnRedirectToForm({
  idSpin,
  eventId,
  setShowPrize,
  setPlay,
  Forward,
}) {
  // console.log(idSpin,eventId,se)
  return (
    <div>
      <ButtonToForm
        onClick={() => {
          setShowPrize(true)
          setPlay(false)
          localStorage.setItem('prize','true')
          window.open(`${window.location.origin}/hamamatsuri-form-prize2?spin=${idSpin}&event=${eventId}`,'_blank')
        }}
        style={{
          border: 'none',
          // backgroundColor: '#fff',
          backgroundColor: 'transparent',
          height: '5rem'
        }}
      >
        <Ablank
          // href={`${window.location.origin}/hamamatsuri-form-prize2?spin=${idSpin}&event=${eventId}`}
          target="_blank"
        >
          プレゼント発送情報を入力
        </Ablank>
      </ButtonToForm>
    </div>
  );
}
