/* eslint-disable */
import { useMutation, useQuery } from "@apollo/react-hooks";
import queryString from "query-string";
import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { MUTATION_VISIT } from "../../graphql/mutation/total-visit";
import { QUERY_EVENT } from "../../graphql/query/event";
import ImgHeader from "./style/img/top/kv.jpg";
import ImgSource from "./style/img/top/kv_sp.jpg";
import ImgSample from "./style/img/top/image_white_600x500.png";
import ImgLottery from "./style/img/common/img_lottery.jpg";
import ImgPageTop from "./style/img/common/pagetop.svg";
import "./style/css/reset.css";
import "./style/css/common.css";
import "./style/css/sp.css";
import "./style/css/tab.css";
import "./style/css/pc.css";
import IntroductionCampaign from "./Introduction/IntroductionCampaign";
import Lottery from "./Lottery/Lottery";

function LandingAlbion() {
  // const slideImages = [
  //   './img/background_hazure.png',
  //   './img/background_atari.png'
  // ]
  const location = useLocation();
  const eventID = queryString.parse(location.search).eventId;
  const { data } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventID,
    },
  });

  let dataPrizes = eventID && data?.event.prizes;
  let losePrizes = dataPrizes && dataPrizes?.filter((item) => item.rank === 0);
  let winPrizes = dataPrizes && dataPrizes?.filter((item) => item.rank !== 0);
  const dataOrderPrizes = dataPrizes && [...winPrizes, ...losePrizes];

  const [increaseTotalVisit] = useMutation(MUTATION_VISIT, {
    fetchPolicy: "no-cache",
  });

  useEffect(() => {
    increaseTotalVisit({
      variables: {
        eventId: eventID,
      },
    });
  }, []);
  return (
    <div id="top">
      <div id="wrapper">
        <article>
          <div id="mainVisual">
            <picture>
              <img
                src={data?.event.banner}
                alt="SPECIAL PRESENT CAMPAIGN"
                style={
                  data?.event?.bannerUrl === "" ? {} : { cursor: "pointer" }
                }
                onClick={() => {
                  if (data?.event?.bannerUrl !== "")
                    window.open(data?.event?.bannerUrl, "_blank");
                }}
              />
            </picture>
            <a href="#lottery" className="scrollTxt">
              すぐ抽選!
            </a>
            <a href="#lottery" className="scroll" />
          </div>
          {/* /.mainVisual */}
          <div id="contents">
            <div className="prizeList">
              <h2 className="hdL">
                <span className="jap font_cp_reven">賞品一覧</span>
              </h2>
              <div className="inner">
                <ul>
                  {dataOrderPrizes
                    ?.filter((item) =>
                      data?.event.customerLost === "Win"
                        ? item
                        : item?.rank !== 0
                    )
                    ?.map((item, index) => (
                      <li>
                        <p className="tit font_cp_reven">{item?.name}</p>
                        <p>
                          <img
                            src={item?.imageUrl || ImgSample}
                            alt="景品名サンプル"
                          />
                        </p>
                        <p className="people font_cp_reven">
                          <span
                            className="num"
                            style={{ fontFamily: "CP_Revenge" }}
                          >
                            {item?.rank !== 0 ? item?.quantity : ""}
                          </span>
                          {item?.rank !== 0 ? "名様" : ""}
                        </p>
                      </li>
                    ))}
                </ul>
              </div>
            </div>

            <Lottery dataOrderPrizes={dataOrderPrizes} />

            <div className="campaignOverview">
              <h2 className="hdL">
                <span className="jap font_cp_reven">キャンペーン概要</span>
              </h2>
              <div className="inner">
                <div className="txt">
                  <IntroductionCampaign data={data} />
                </div>
              </div>
            </div>
          </div>
          {/* /#contents */}
        </article>
        <p className="pageTop">
          <a href="#">
            <img src={ImgPageTop} alt="" />
          </a>
        </p>
      </div>
      {/* /#wrapper */}
    </div>
  );
}

export default LandingAlbion;
