/* eslint-disable */
import { Modal } from "antd";
import queryString from "query-string";
import React, { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { QUERY_EVENT } from "../../graphql/query/event";
import { useMutation, useQuery } from "@apollo/react-hooks";
import "./style/css/reset.css";
import "./style/css/common.css";
import "./style/css/sp.css";
import "./style/css/tab.css";
import "./style/css/pc.css";
import ImgWin from "./style/img/lottery/img_bg_lottery01.jpg";
import ImgWhite from "./style/img/lottery/image_white.png";
import ImgPart from "./style/img/lottery/img_bg_lottery02.jpg";
import ImgLost2 from "./style/img/lottery/img_lottery_off.png";
import ImgHeader from "./style/img/lottery/txt_hd01.svg";
import ImgLost from "./style/img/lottery/txt_hd02.svg";
import ImgHeaderPart from "./style/img/lottery/txt_hd03.svg";
import { Spin } from "antd";
const { confirm } = Modal;

function LandingAlbion() {
  const location = useLocation();
  const history = useHistory();
  const spinId = queryString.parse(location.search).spin;
  const eventId = queryString.parse(location.search).event;
  const [prize, setPrize] = React.useState(null);
  const { data: dataEvent, loading } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventId,
    },
  });

  useEffect(() => {
    if (dataEvent) {
      setPrize(
        dataEvent?.event?.prizes.filter(
          (item) => item.rank === Number(localStorage.getItem("prizeRank"))
        )
      );
    }
  }, [dataEvent]);
  return (
    <div id="wrapper">
      <div id="contents">
        {!localStorage.getItem("prize") ? (
          <div className="lotteryScreen off">
            <div className="inner" style={{ fontSize: "4rem", color: "white" }}>
              抽選結果を取得できませんでした。{" "}
            </div>
          </div>
        ) : (
          <article>
            {Number(localStorage.getItem("prizeRank")) === 0 &&
            localStorage.getItem("customerLost") === "Win" ? (
              <div className="lotteryScreen price">
                <div className="inner">
                  <div className="bg">
                    <h2 className="hd04">
                      <img src={ImgHeaderPart} alt="参加賞" />
                    </h2>
                    <p>
                      {loading ? (
                        <Spin
                          style={{
                            zIndex: 2,
                          }}
                        />
                      ) : (
                        <img
                          src={
                            prize?.[0]?.imageUrl
                              ? prize?.[0]?.imageUrl
                              : ImgPart
                          }
                          alt="参加賞"
                        />
                      )}
                    </p>
                    <p className="another">ご参加ありがとうございます！</p>
                  </div>
                  <p className="link">
                    <a
                      href={`${window.location.origin}/form-prize?spin=${spinId}&event=${eventId}`}
                      className="btnLink"
                    >
                      プレゼント発送方法を入力
                    </a>
                  </p>
                </div>
              </div>
            ) : (
              <>
                {Number(localStorage.getItem("prizeRank")) !== 0 ? (
                  <div className="lotteryScreen per">
                    <div className="inner">
                      <h2 className="hd01">
                        <img src={ImgHeader} alt="当選" />
                      </h2>
                      <div className="winner">
                        <p>
                          {loading ? (
                            <Spin
                              style={{
                                zIndex: 2,
                              }}
                            />
                          ) : (
                            <img
                              src={prize?.[0]?.imageUrl || ImgWhite}
                              alt="当選"
                            />
                          )}
                        </p>
                      </div>
                      <h2 className="hd02" style={{ fontFamily: "emoji" }}>
                        {prize?.[0]?.name}
                      </h2>
                      <p className="link">
                        <a
                          href={`${window.location.origin}/form-prize?spin=${spinId}&event=${eventId}`}
                          className="btnLink"
                        >
                          プレゼント発送方法を入力
                        </a>
                      </p>
                    </div>
                  </div>
                ) : (
                  <div className="lotteryScreen off">
                    <div className="inner">
                      <div className="bg">
                        <h2 className="hd03">
                          <span className="pre">残念！</span>
                          <img src={ImgLost} alt="当選" />
                        </h2>
                        <p>
                          <img src={ImgLost2} alt="残念" />
                        </p>
                        <p className="another">
                          またの挑戦をお待ちしております！
                        </p>
                      </div>
                      <p className="link">
                        <a
                          href={`${window.location.origin}/landing-page?event=${eventId}`}
                          className="btnLink"
                        >
                          ホームへ戻る
                        </a>
                      </p>
                    </div>
                  </div>
                )}
              </>
            )}
            {/* /#contents */}
          </article>
        )}
      </div>
    </div>
    /* /#wrapper */
  );
}

export default LandingAlbion;
