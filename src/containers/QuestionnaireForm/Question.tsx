/* eslint-disable */
import React, { useCallback, useState, useEffect } from 'react'
import Input from '../../components/Input/Input'
import { Row, Col } from '../../components/FlexBox/FlexBox'
import {
  FormFields,
  FormLabel,
  Error
} from '../../components/FormFields/FormFields'
import { Radio } from 'antd'
import { QuestionBox } from '../Questionnaire/Questionnaire.style'
import {
  AddIcon,
  EditIcon,
  DeleteIcon
} from '../../components/ActionIcon/ActionIcon'

type Props = any

const UserForm: React.FC<Props> = (props) => {
  const {
    data,
    index,
    disabled,
    register,
    errors,
    setQuestionList,
    questionList,
    setValue
  } = props
  const [questionType, setQuestionType] = useState('First')
  const [typeThird, setTypeThird] = useState('TRUE')
  const [questionInput, setQuestionInput] = useState('')
  const [contentInput, setContentInput] = useState('')

  useEffect(() => {
    if (data.question.length > 0) {
      setQuestionType(data.type)
      setTypeThird(data.content)
      // fix bug type change reset the data
      if (questionInput.length === 0) {
        setValue(`question${index}`, data.question)
      } else {
        setValue(`question${index}`, questionInput)
      }
      if (contentInput.length === 0 && questionInput.length === 0) {
        if (
          data.content?.toUpperCase() === 'TRUE' ||
          data.content?.toUpperCase() === 'FALSE'
        ) {
          setValue(`content${index}`, '')
        } else {
          setValue(`content${index}`, data.content)
        }
      } else {
        setValue(`content${index}`, contentInput)
      }
      setValue(`type${index}`, data.type)
    }
  }, [data,questionList,contentInput,questionInput])

  const QuestionType = (e) => {
    setQuestionType(e.target.value)
    const newItem = questionList.map((item) => ({
      question: item.question,
      content: item.content,
      type: item.type
    }))
    newItem[index].type = e.target.value
    setQuestionList(newItem)
  }
  const TypeThird = (e) => {
    setTypeThird(e.target.value)
    const newItem = questionList.map((item) => ({
      question: item.question,
      content: item.content,
      type: item.type
    }))
    newItem[index].content = e.target.value
    setQuestionList(newItem)
  }

  const onChangeQuestion = (e) => {
    setValue(`question${index}`, e.target.value)
    setQuestionInput(e.target.value)
    const newItem = questionList.map((item) => ({
      question: item.question,
      content: item.content,
      type: item.type
    }))
    newItem[index].question = e.target.value
    setQuestionList(newItem)
  }

  const onChangeContent = (e) => {
    setValue(`content${index}`, e.target.value)
    setContentInput(e.target.value)
    const newItem = questionList.map((item) => ({
      question: item.question,
      content: item.content,
      type: item.type
    }))
    newItem[index].content = e.target.value
    setQuestionList(newItem)
  }

  const questionDelete = (e) => {
    const newList = [...questionList]
    newList.splice(e, 1)
    setQuestionList(newList)
  }

  useEffect(() => {
    setContentInput(data.content)
    setQuestionInput(data.question)
  },[questionList])

  return (
    <>
      <Row style={{ margin: '0 0px 30px' }}>
        <FormLabel>
          アンケート項目{index + 1}
          {!disabled && (
            <DeleteIcon
              color='#f00'
              onClick={() => questionDelete(index)}
              title={'アンケート項目削除'}
            />
          )}
        </FormLabel>
        <QuestionBox>
          <FormFields>
            <FormLabel>
              項目名&nbsp;<span style={{ color: 'red' }}>*</span>
            </FormLabel>
            <Input
              name={`question${index}`}
              inputRef={register({ required: true, maxLength: 100 })}
              onChange={onChangeQuestion}
            />
            {errors[`question${index}`] &&
              errors[`question${index}`].type === 'required' && (
                <Error>(*)は必須の項目です。</Error>
              )}
            {errors[`question${index}`] &&
              errors[`question${index}`].type === 'maxLength' && (
                <Error>項目名は100文字以下設定してください。</Error>
              )}
          </FormFields>
          <FormFields>
            <FormLabel>入力形式</FormLabel>
            <Radio.Group onChange={QuestionType} value={questionType}>
              <Radio value={'First'}>選択形式(複数選択)</Radio>
              <Radio value={'Second'}>選択形式(単一選択)</Radio>
              <Radio value={'Third'}>自由入力形式</Radio>
            </Radio.Group>
          </FormFields>
            <div style={{display:questionType === 'Third'?'none':''}}>
            <FormFields >
              <FormLabel>
                選択項目(カンマ区切り)&nbsp;
                <span style={{ color: 'red' }}>*</span>
              </FormLabel>
              <Input
                name={`content${index}`}
                inputRef={register({ required: true, maxLength: 200 })}
                onChange={onChangeContent}
              />
              {errors[`content${index}`] &&
                errors[`content${index}`].type === 'required' && (
                  <Error>(*)は必須の項目です。</Error>
                )}
              {errors[`content${index}`] &&
                errors[`content${index}`].type === 'maxLength' && (
                  <Error>選択項目は200文字以下設定してください。</Error>
                )}
            </FormFields>
            </div>
            <div style={{display:questionType !== 'Third'?'none':''}}>
            <FormFields>
              <FormLabel>必須入力</FormLabel>
              <Radio.Group onChange={TypeThird} value={typeThird}>
                <Radio value={'TRUE'}>必須</Radio>
                <Radio value={'FALSE'}>必須ではない</Radio>
              </Radio.Group>
            </FormFields>
            </div>
        </QuestionBox>
      </Row>
    </>
  )
}
export default UserForm
