/* eslint-disable */
import React, { useCallback, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks'
import { Scrollbars } from 'react-custom-scrollbars'
// import { AuthContext, Role } from '../../context/auth'
import { useDrawerDispatch, useDrawerState } from '../../context/DrawerContext'
import Input from '../../components/Input/Input'
import Button, { KIND } from '../../components/Button/Button'
import DrawerBox from '../../components/DrawerBox/DrawerBox'
import { Row, Col } from '../../components/FlexBox/FlexBox'
import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
  RequiredCol
} from '../DrawerItems/DrawerItems.style'
import {
  FormFields,
  FormLabel,
  Error
} from '../../components/FormFields/FormFields'
import { MUTATION_CREATE_QUESTIONNAIRE } from '../../graphql/mutation/create-question'
import { MUTATION_UPDATE_QUESTIONNAIRE } from '../../graphql/mutation/update-question'
import Select from '../../components/Select/Select'
import {
  OverideCancel,
  OverideSave,
  OverideSelect,
  OverideAdd
} from './Overrides'
import { Notification } from '../../components/Notification/NotificationCustom'
import { User, UserFormInput, UserType } from './Interface'
import { Value } from 'baseui/select'
import { QUERY_AGENCYS } from '../../graphql/query/users'
import { Radio } from 'antd'
import Question from './Question'

type Props = any
// const roleOption = [
//   { id: '1', role: 'admin', name: 'ADMIN' },
//   { id: '2', role: 'agency', name: '代理店' },
//   { id: '3', role: 'client', name: '企業' }
// ]
const roleAdmin = [
  { id: '2', role: 'agency', name: '代理店' },
  { id: '3', role: 'client', name: '企業' }
]
const roleAgency = [{ id: '3', role: 'client', name: '企業' }]

const UserForm: React.FC<Props> = (props) => {
  const dispatch = useDrawerDispatch()
  const isCreate = useDrawerState('actionType') === 'create'
  const isEdit = useDrawerState('actionType') === 'edit'
  const { id, question, questionList } = useDrawerState('data') ?? []

  const [questionListState, setQuestionListState] = useState([
    { question: '', type: 'First', content: '' }
  ])

  const { register, handleSubmit, setValue, errors, getValues } = useForm()

  const closeDrawer = useCallback(() => {
    dispatch({ type: 'CLOSE_DRAWER' })
    dispatch({ type: 'SUCCESS' })
  }, [dispatch])

  const [createQuestion] = useMutation(MUTATION_CREATE_QUESTIONNAIRE, {
    fetchPolicy: 'no-cache'
  })
  const [updateQuestion] = useMutation(MUTATION_UPDATE_QUESTIONNAIRE, {
    fetchPolicy: 'no-cache'
  })

  const onSubmit = (data: any) => {
    const list = questionListState.map((item) => ({
      question: item.question,
      content: item.content,
      type: item.type
    }))
    list.map((item, index) => {
      item.question = data[`question${index}`]
      item.content =
        item.type !== 'Third' ? data[`content${index}`] : item.content || 'TRUE'
    })

    if (isCreate) {
      let values = {
        question: data.question,
        type: 'First',
        content: null,
        questionList: list
      }
      createQuestion({
        variables: {
          createQuestionnaireInput: values
        }
      })
        .then((value) => {
          Notification({
            type: 'success',
            message: '成功',
            description: 'アンケート作成に成功しました。'
          })
          closeDrawer()
        })
        .catch(({ graphQLErrors }) =>
          Notification({
            type: 'error',
            message: ' エラー',
            description: graphQLErrors[0]?.message
          })
        )
    } else if (isEdit) {
      let values = {
        id: id,
        question: data.question,
        type: 'First',
        content: null,
        questionList: list
      }
      updateQuestion({
        variables: {
          updateQuestionnaireInput: values
        }
      })
        .then(() => {
          Notification({
            type: 'success',
            message: '成功',
            description: 'アンケート更新に成功しました。'
          })
          closeDrawer()
        })
        .catch(({ graphQLErrors }) => {
          Notification({
            type: 'error',
            message: ' エラー',
            description:
              graphQLErrors[0]?.message &&
              'アンケートを追加した人しか編集出来ません。'
          })
        })
    }
  }

  React.useEffect(() => {
    if (isEdit) {
      setValue('question', question)
      setQuestionListState(questionList)
    }
  }, [isEdit])

  React.useEffect(() => {
    register({ name: 'question' })
  }, [register])

  const questionAdd = () => {
    setQuestionListState([
      ...questionListState,
      { question: '', type: 'First', content: '' }
    ])
  }

  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>
          {isCreate ? 'アンケート作成' : 'アンケート編集'}
        </DrawerTitle>
      </DrawerTitleWrapper>

      <Form onSubmit={handleSubmit(onSubmit)} style={{ height: '100%' }}>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: 'hidden' }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: 'none' }}
              className='track-horizontal'
            />
          )}
        >
          <Row>
            <RequiredCol lg={3}>
              <FieldDetails>
                必要な情報を記入して下さい。
                <br />
                <span style={{ color: 'red' }}>(*)は必須の項目です。</span>
              </FieldDetails>
            </RequiredCol>

            <Col lg={9}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>
                    アンケート名&nbsp;<span style={{ color: 'red' }}>*</span>
                  </FormLabel>
                  <Input
                    name='question'
                    inputRef={register({
                      required: true,
                      maxLength: 100
                    })}
                  />
                  {errors.question && errors.question.type === 'required' && (
                    <Error>(*)は必須の項目です。</Error>
                  )}
                  {errors.question && errors.question.type === 'maxLength' && (
                    <Error>アンケート名は100文字以下に設定してください。</Error>
                  )}
                </FormFields>
                {questionListState?.map((item, index) => {
                  return (
                    <Question
                      key={index}
                      data={item}
                      index={index}
                      disabled={questionListState.length === 1}
                      errors={errors}
                      register={register}
                      setValue={setValue}
                      questionList={questionListState}
                      setQuestionList={setQuestionListState}
                      // questionDelete={(e) => questionDelete(e)}
                    />
                  )
                })}
                <Button
                  overrides={OverideAdd}
                  type='button'
                  onClick={questionAdd}
                >
                  項目を追加するボタン
                </Button>
              </DrawerBox>
            </Col>
          </Row>
        </Scrollbars>

        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={OverideCancel}
          >
            キャンセル
          </Button>
          <Button type='submit' overrides={OverideSave}>
            {isCreate ? '追加' : '更新する'}
          </Button>
        </ButtonGroup>
      </Form>
    </>
  )
}
export default UserForm
