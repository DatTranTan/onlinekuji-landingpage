/* eslint-disable */
import {
  colTitleRender,
  renderCreateDate,
  renderRowCenter,
  renderRowText,
  renderRowNumber,
  renderRowPopover
} from '../../components/TableCell/TableCellRender'

export const columns: any = [
  {
    title: colTitleRender('項番'),
    dataIndex: 'idx',
    width: 60,
    ellipsis: true,
    align: 'center',
    render: (value: any) => renderRowCenter(value)
  },
  {
    title: colTitleRender('アンケート名'),
    dataIndex: 'question',
    width: '60%',
    ellipsis: true,
    render: (value: any) => renderRowText(value)
  },
  {
    title: colTitleRender('作成日'),
    dataIndex: 'createdAt',
    width: '10%',
    ellipsis: true,
    align: 'center',
    render: (value: any) => renderCreateDate(value)
  },
  {
    title: colTitleRender('数量'),
    dataIndex: 'questionList',
    width: '10%',
    ellipsis: true,
    render: (value: any) => renderRowNumber(value.length)
  },
  {
    title: colTitleRender('内容'),
    dataIndex: 'questionList',
    width: 95,
    ellipsis: false,
    render: (value: any) => renderRowPopover(value)
  }
]

