/* eslint-disable */
import React, { useState, useCallback, useRef } from 'react'
import { useDrawerDispatch, useDrawerState } from '../../context/DrawerContext'
import { useDeviceType } from '../../settings/useDeviceType'
import { Table, Tooltip, Badge, Row, Col } from 'antd'
import {
  Header,
  Heading,
  SearchWrapper,
  SelectWrapper,
  TableWrapper,
  Container
} from '../Questionnaire/Questionnaire.style'
import {
  ActionCol,
  ActionWrapper,
  TitleCol
} from '../Questionnaire/Questionnaire.style'
import Input from '../../components/Input/Input'
import {
  // HomeOutlined,
  DeleteOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
  EyeInvisibleOutlined,
  EyeOutlined,
  PlusCircleFilled
} from '@ant-design/icons'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { QUERY_QUESTIONNAIRE } from '../../graphql/query/questionnaire'
import { DELETE_QUESTIONNAIRE } from '../../graphql/mutation/delete-question'
import confirm from 'antd/lib/modal/confirm'
import SeachText from '../../components/SeachText/BaseSeachText'
import { Notification } from '../../components/Notification/NotificationCustom'
import dayjs from 'dayjs'
import InputPassword from '../../components/Input/PasswordInput'
import {
  AddIcon,
  EditIcon,
  DeleteIcon
} from '../../components/ActionIcon/ActionIcon'
import Select from '../../components/Select/HeaderSelect'
import { columns } from './Columns'

export default function Users() {
  const [searchText, setSearchText] = useState('')
  const isFinished = useDrawerState('isFinished')
  const [selectValue, setSelectValue] = useState([])
  const [disableEdit, setDisableEdit] = useState(true)
  const [disableDelete, setDisableDelete] = useState(true)
  const [selectedRows, setSelectedRows] = useState([])
  const [pageIndex, setPageIndex] = useState(0)

  const { tablet } = useDeviceType(window.navigator.userAgent)
  const pageSize = tablet ? 12 : 10
  const { data, loading, error, refetch } = useQuery(QUERY_QUESTIONNAIRE, {
    variables: {
      name: searchText,
      offset: pageSize * pageIndex,
      limit: pageSize
    },
    fetchPolicy: 'network-only'
  })

  const [deleteQestion] = useMutation(DELETE_QUESTIONNAIRE, {
    fetchPolicy: 'no-cache'
  })
  const questionnaireData = data?.getAllQuestionnaire?.questionnaires.map(
    (item, idx) => ({
      ...item,
      idx: pageSize * pageIndex + idx + 1,
      key: item.id
    })
  )

  const total = data?.getAllQuestionnaire.count ?? 0

  const dispatch = useDrawerDispatch()

  const openEditQuestionForm = useCallback(
    (data) => {
      dispatch({
        type: 'OPEN_DRAWER',
        drawerComponent: 'QUESTIONNAIRE_FORM',
        data: data,
        actionType: 'edit'
      })
    },
    [dispatch]
  )

  const openAddQuestionForm = useCallback(
    () =>
      dispatch({
        type: 'OPEN_DRAWER',
        drawerComponent: 'QUESTIONNAIRE_FORM',
        data: {},
        actionType: 'create'
      }),
    [dispatch]
  )

  const onSelectChange = (value: any) => {
    setSelectValue(value)
    if (pageIndex && pageIndex !== 0) setPageIndex(0)
  }

  const rowSelection = {
    selectedRowKeys: selectedRows.map((item) => item.key),
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRows(selectedRows)

      if (selectedRowKeys.length > 1) {
        setDisableEdit(true)
        setDisableDelete(false)
      } else if (selectedRowKeys.length > 0) {
        setDisableEdit(false)
        setDisableDelete(false)
      } else {
        setDisableEdit(true)
        setDisableDelete(true)
      }
    }
  }

  const deleteHandler = () => {
    confirm({
      type: 'warning',
      title: '確認',
      content:
        'アンケートを削除すると関連アンケートも削除されます。よろしいですか？',
      icon: <ExclamationCircleOutlined />,
      cancelText: 'キャンセル',
      okText: 'OK',
      onOk: () =>
        deleteQestion({
          variables: {
            ids: selectedRows.map((item) => item.id)
          }
        })
          .then(() => {
            setSelectedRows([])
            setDisableDelete(true)
            setDisableEdit(true)
            refetch()
            Notification({
              type: 'success',
              message: '成功',
              description: '削除に成功しました。'
            })
          })
          .catch(({ graphQLErrors }) =>
            Notification({
              type: 'error',
              message: ' エラー',
              description: graphQLErrors[0]?.message.includes(
                'a foreign key constraint fails'
              )
                ? 'このアンケートはイベントに存在していますから、削除出来ません。'
                : graphQLErrors[0]?.message.includes('can not delete') &&
                  'アンケートを追加した人しか編集出来ません。'
            })
          )
    })
  }

  React.useEffect(() => {
    refetch()
    setSelectedRows([])
    setDisableEdit(true)
    setDisableDelete(true)
  }, [isFinished, refetch])

  return (
    <>
      <Header>
        <TitleCol xl={6} lg={6} md={4}>
          {/* <Heading>アカウント管理</Heading> */}
          <ActionWrapper>
            <AddIcon
              color={'#00C58D'}
              title={'アンケート追加'}
              onClick={openAddQuestionForm}
            />
            <EditIcon
              disabled={disableEdit}
              color={disableEdit && '#ccc'}
              title={'アンケート編集'}
              onClick={
                !disableEdit
                  ? () => openEditQuestionForm(selectedRows[0])
                  : undefined
              }
            />
            <DeleteIcon
              disabled={disableDelete}
              color={disableDelete ? '#ccc' : '#f00'}
              onClick={!disableDelete ? () => deleteHandler() : undefined}
              title={'アンケート削除'}
            />
          </ActionWrapper>
        </TitleCol>
        <ActionCol xl={6} lg={6} md={8}>
          <SearchWrapper>
            <SeachText
              setText={setSearchText}
              pageIndex={pageIndex}
              setPageIndex={setPageIndex}
            />
          </SearchWrapper>
        </ActionCol>
      </Header>
      <TableWrapper>
        <Table
          bordered
          loading={loading}
          style={{
            overflow: 'auto',
            background: 'white',
            border: '1px solid #ddd'
          }}
          rowSelection={{
            type: 'checkbox',
            ...rowSelection
          }}
          columns={columns}
          dataSource={questionnaireData}
          // bordered
          pagination={{
            total: total,
            pageSize: pageSize,
            current: pageIndex + 1,
            onChange: (value) => {
              refetch({ offset: pageSize * value - 1, limit: pageSize }).then(
                () => {
                  setPageIndex(value - 1)
                }
              )
            },
            showSizeChanger: false,
            // onShowSizeChange: (current, size) => {
            //   setPageSize(size)
            //   console.log(current, size)
            // },
            position: ['bottomCenter']
          }}
        />{' '}
      </TableWrapper>
    </>
  )
}
