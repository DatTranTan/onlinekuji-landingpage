import { styled } from 'baseui'
import AOS from 'aos'
import 'aos/dist/aos.css'
AOS.init()

export const WrapperBlock = styled('div', () => ({
  width: '5rem',
  height: '5rem',
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '2rem',
  marginLeft: '1rem',
  marginRight: '1rem',
  backgroundColor: '#52c41ac2',
  borderRadius: '40px',
  // borderRadius: '30px',
  boxShadow: 'inset -2px -2px 0 rgba(0, 0, 0, 0.15)',
  fontFamily: 'Roboto',
  // '@media only screen and (max-width: 1024px)': {
  //   width: '29%'
  // }
  '@media only screen and (max-width: 768px)': {
    marginLeft: '0.5rem'
  },
  '@media only screen and (max-width: 414px)': {
    borderRadius: '30px',
    width: '7rem',
    height: '7rem',
    marginTop: '1rem'
  }
}))

export const WrapperIntroduction = styled('div', () => ({
  display: 'flex',
  width: '100%',
  justifyContent: 'center',
  marginTop: '3rem',
  '@media only screen and (max-width: 768px)': {
    flexFlow: 'column'
  }
}))
export const BoxLeft = styled('div', () => ({
  display: 'flex',
  justifyContent: 'center',
  width: '33.33333%',
  '@media only screen and (max-width: 768px)': {
    width: '100%'
  },
  '@media only screen and (max-width: 414px)': {
    flexFlow: 'column',
    alignItems: 'center'
  }
}))
export const BoxMid = styled('div', () => ({
  display: 'flex',
  justifyContent: 'center',
  width: '33.33333%',
  '@media only screen and (max-width: 768px)': {
    width: '100%'
  },

  '@media only screen and (max-width: 414px)': {
    flexFlow: 'column',
    alignItems: 'center'
  }
}))
export const BoxRight = styled('div', () => ({
  display: 'flex',
  justifyContent: 'center',
  width: '33.33333%',
  '@media only screen and (max-width: 768px)': {
    width: '100%'
  },

  '@media only screen and (max-width: 414px)': {
    flexFlow: 'column',
    alignItems: 'center'
  }
}))
export const BoxText = styled('div', () => ({
  width: '60%',
  fontFamily: 'Roboto',
  // fontWeight: 600
  lineHeight: '1.5rem',
  '@media only screen and (max-width: 414px)': {
    width: '90%',
    textAlign: 'center',
    marginTop: '1.5rem'
  }
}))
export const Step = styled('h1', () => ({
  color: ' rgb(95, 82, 220)',
  fontFamily: 'Roboto',
  fontWeight: 600,
  fontSize: '1.5rem'
}))
export const Text = styled('p', () => ({
  fontFamily: 'Roboto',
  fontWeight: 700,
  fontSize: '1rem'
}))
