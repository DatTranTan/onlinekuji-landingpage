import React from 'react'
import {
  WrapperBlock,
  WrapperIntroduction,
  BoxLeft,
  BoxMid,
  BoxRight,
  BoxText,
  Step,
  Text
} from './Introduction.style'
import Box from '../../../assets/images/box.png'
import Box1 from '../../../assets/images/box3.png'
import Box2 from '../../../assets/images/box1.png'
import AOS from 'aos'
import 'aos/dist/aos.css'

function Introduction() {
  AOS.init()
  return (
    <WrapperIntroduction>
      <BoxLeft
        data-aos={'fade-up'}
        data-aos-duration={'1000'}
        data-aos-easing={'ease-in-sine'}
      >
        <WrapperBlock data-aos={'zoom-in'} data-aos-duration={'1000'}>
          <img src={Box} alt='' width='60%' height='60%' />
        </WrapperBlock>
        <BoxText>
          <Step>ステップ1</Step>
          <Text>ウェブサイトでプレゼントリストをご覧ください。</Text>
          {/* <p>Trying and relaxing with feel of you !!!</p> */}
        </BoxText>
      </BoxLeft>
      <BoxMid
        data-aos={'fade-up'}
        data-aos-duration={'1300'}
        data-aos-easing={'ease-in-sine'}
      >
        <WrapperBlock data-aos={'zoom-in'} data-aos-duration={'1000'}>
          <img src={Box1} alt='' width='60%' height='60%' />
        </WrapperBlock>
        <BoxText>
          <Step>ステップ2</Step>
          <Text>
            以下の抽選ボタンをお押ししてから、抽選結果をお待ちください。
          </Text>
          {/* <p>Trying and relaxing with feel of you !!!</p> */}
        </BoxText>
      </BoxMid>
      <BoxRight
        data-aos={'fade-up'}
        data-aos-duration={'1600'}
        data-aos-easing={'ease-in-sine'}
      >
        <WrapperBlock data-aos={'zoom-in'} data-aos-duration={'1000'}>
          <img src={Box2} alt='' width='60%' height='60%' />
        </WrapperBlock>
        <BoxText>
          <Step>ステップ3</Step>
          <Text>当選された方には以下のフォームに入力してください。</Text>
          <Text>フォームに入力された住所へプレゼントを配送します。</Text>
        </BoxText>
      </BoxRight>
    </WrapperIntroduction>
  )
}

export default Introduction
