/* eslint-disable */
import React from 'react'
import { FormBody } from '../LotteryPage.style'
import { WrapperFooter, FooterLayout, ImgLeft, ImgRight } from './Footer.styled'
import WaveLeft from '../../../assets/images/imgLeft.png'
import WaveRight from '../../../assets/images/imgRight.png'
import { useStyletron } from 'baseui'

function Footer() {
  const [css] = useStyletron()
  return (
    <>
      <div
        style={{
          width: '100%',
          display: 'flex',
          justifyContent: 'space-between',
          position: 'relative',
          bottom: '-9rem'
        }}
      >
        <ImgLeft
        // style={{
        //   backgroundImage: 'url(' + Wave + ')',
        //   backgroundSize: 'cover',
        //   width: '507.57px',
        //   height: '381.531px'
        // }}
        >
          <img
            src={WaveLeft}
            alt=''
            className={css({
              width: '507.57px',
              height: '280px',
              objectFit: 'cover',
              '@media only screen and (max-width: 850px) ': {
                width: '200px',
                height: '225px'
              },
              '@media only screen and (max-width: 520px) ': {
                width: '11rem',
                height: '225px'
              },
              '@media only screen and (max-width: 350px) ': {
                width: '9rem',
                height: '225px'
              }
            })}
          ></img>
        </ImgLeft>
        <ImgRight
        // style={{
        //   backgroundImage: 'url(' + WaveRight + ')',
        //   backgroundSize: 'cover',
        //   width: '507.57px',
        //   height: '280px'
        // }}
        >
          <img
            src={WaveRight}
            alt=''
            className={css({
              width: '507.57px',
              height: '280px',
              objectFit: 'cover',
              '@media only screen and (max-width: 850px) ': {
                width: '200px',
                height: '225px'
              },
              '@media only screen and (max-width: 520px) ': {
                width: '11rem',
                height: '225px'
              },
              '@media only screen and (max-width: 350px) ': {
                width: '9.5rem',
                height: '225px'
              }
            })}
          ></img>
        </ImgRight>
      </div>
      <WrapperFooter>
        <FormBody>
          <FooterLayout>
            <span style={{ zIndex: 1000 }}>
              <a href='https://vermuda.jp/' target='_blank'>
                合同会社Vermuda
              </a>
              によって供給された{' '}
            </span>
          </FooterLayout>
        </FormBody>
      </WrapperFooter>
    </>
  )
}

export default Footer
