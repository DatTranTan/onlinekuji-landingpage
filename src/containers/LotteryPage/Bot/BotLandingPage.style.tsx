import { styled } from 'baseui'
export const WrapperBot = styled('div', () => ({
  display: 'flex',
  height: '28rem',
  flexWrap: 'wrap',
  justifyContent: 'center',
  marginTop: '3.5rem',
  '@media only screen and (max-width: 1440px)': {
    height: '33vw',
    marginTop: '1.5rem'
  },
  '@media only screen and (max-width: 1024px)': {
    height: '36vw',
    marginTop: '2.5rem'
  },
  '@media only screen and (max-width: 768px)': {
    height: '20.5rem',
    marginTop: '1.5rem'
  }
}))
export const LeftInfor = styled('div', () => ({
  width: '80%',
  height: 'auto',
  '@media only screen and (max-width:500px)': {
    width: '90%'
  },
  '@media only screen and (max-width: 330px)': {
    width: '97%'
  }
}))
export const RightInfor = styled('div', () => ({
  width: '83%',
  textAlign: 'center',
  // marginTop: '8rem'
  '@media only screen and (max-width: 500px)': {
    width: '70%'
  }
}))

export const WrapperBlock = styled('div', () => ({
  width: '45%',
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '2rem',
  boxShadow: 'rgba(251, 23, 23, 0.46) 7px 13px 13px -5px',
  marginLeft: '1rem',
  marginRight: '1rem',
  height: '350px',
  // backgroundColor: '#e52f37',
  background:
    'linear-gradient(to right bottom, rgb(255 56 56), rgb(251 131 225))',
  borderRadius: '30px',
  fontFamily: 'Roboto',
  '@media only screen and (max-width: 1440px)': {
    height: '26vw'
  },
  '@media only screen and (max-width: 768px)': {
    height: '15rem'
  },
  '@media only screen and (max-width: 500px)': {
    width: '90%'
  }
}))
export const WrapperBlockDesktop = styled('div', () => ({
  width: '45%',
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '2rem',
  boxShadow: 'rgba(251, 23, 23, 0.46) 7px 13px 13px -5px',
  marginLeft: '1rem',
  marginRight: '1rem',
  height: '350px',
  // backgroundColor: '#e52f37',
  background:
    'linear-gradient(to right bottom, rgb(255 56 56), rgb(251 131 225))',
  borderRadius: '30px',
  fontFamily: 'Roboto',
  '@media only screen and (max-width: 1440px)': {
    height: '26vw'
  },
  '@media only screen and (max-width: 768px)': {
    height: '15rem'
  },
  '@media only screen and (max-width: 500px)': {
    display: 'none'
  }
}))
