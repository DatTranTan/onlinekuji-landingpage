/* eslint-disable */
import React from 'react'
import {
  WrapperBot,
  LeftInfor,
  RightInfor,
  WrapperBlock,
  WrapperBlockDesktop
} from './BotLandingPage.style'
import gold1 from '../../../assets/images/Untitled-3.png'
import gift from '../../../assets/images/gift2.png'
import gift1 from '../../../assets/images/box.png'
import gift2 from '../../../assets/images/box1.png'
import gift3 from '../../../assets/images/box2.png'
import gift4 from '../../../assets/images/box3.png'
import gift5 from '../../../assets/images/box4.png'
import gift6 from '../../../assets/images/box5.png'
import AOS from 'aos'
import 'aos/dist/aos.css'

function BotLandingPage() {
  AOS.init()
  return (
    <>
      <WrapperBot>
        <WrapperBlock>
          <LeftInfor
            data-aos={'zoom-in'}
            data-aos-duration={'1000'}
            data-aos-easing={'ease'}
          >
            <img src={gift5} alt='' width='100%' height='100%' />
          </LeftInfor>
        </WrapperBlock>
        <WrapperBlockDesktop>
          <RightInfor
            data-aos={'zoom-in'}
            data-aos-duration={'1300'}
            data-aos-easing={'ease'}
          >
            <img src={gift6} alt='' width='100%' height='100%' />
          </RightInfor>
        </WrapperBlockDesktop>
      </WrapperBot>
    </>
  )
}

export default BotLandingPage
