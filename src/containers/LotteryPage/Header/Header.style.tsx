import { styled } from 'baseui'

export const HeaderWrapper = styled('div', () => ({
  width: '100%',
  background: 'white',
  display: 'flex',
  justifyContent: 'center',
  position: 'fixed',
  boxShadow: '0 8px 16px rgba(0, 0, 0, 0.1)',
  height: '90px',
  alignItems: 'center',

  zIndex: 1000,
  '@media only screen and (max-width : 768px)': {
    textAlign: 'center '
  },
  '@media only screen and (max-width : 414px)': {
    height: '65px'
  }
}))

export const LeftTitle = styled('h1', () => ({
  color: 'rgb(244 25 25)',
  fontSize: '2.1em',
  fontWeight: 600,
  cursor: 'pointer',
  marginTop: '0.5em',
  '@media only screen and (max-width : 1500px)': {
    marginLeft: '8rem'
  },
  '@media only screen and (max-width : 850px)': {
    marginLeft: '0'
  },
  '@media only screen and (max-width : 414px)': {
    fontSize: '1.7em'
  },
  '@media only screen and (max-width : 320px)': {
    fontSize: '1.5em'
  }
}))
export const RightTitle = styled('span', () => ({
  color: '#5f52dc',
  fontSize: '1em',
  fontWeight: 600
}))
