import { styled } from 'baseui'
// import Img from 'react-image'
// import { use100vh } from 'react-div-100vh'
// const height = use100vh()
export const FormBody = styled('div', () => ({
  maxWidth: '1300px',
  width: '100%',
  '@media only screen and (max-width : 1280px)': {
    maxWidth: '1100px'
  }
  // '@media only screen and (max-width : 1024px)': {
  //   maxWidth: '900px'
  // }
}))

export const ImgLink = styled('a', () => ({}))

export const ImgCustom = styled('img', () => ({
  marginTop: '90px',
  width: '100%',
  maxHeight: '786px',
  objectFit: 'cover',
  '@media only screen and (max-width: 414px)': {
    marginTop: '65px'
  }
}))

export const PrizeName = styled('div', () => ({
  textAlign: 'center',
  fontFamily: 'Roboto',
  color: '#272652',
  marginTop: '2.5rem',
  marginBottom: '1rem',
  fontSize: '2.5rem',
  fontWeight: '900',
  // textShadow: '3px 2px 2px rgb(62 169 245 / 92%)',
  '@media only screen and (max-width: 768px)': {
    fontSize: '2rem'
  },
  '@media only screen and (max-width: 320px)': {
    marginTop: '1.5rem',
    fontSize: '1.5rem',
    fontWeight: '600'
  }
}))

export const WrapperBody = styled('div', () => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'center'
}))

export const BlockTopElement = styled('div', () => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  marginBottom: '2rem',
  '@media only screen and (max-width: 768px)': {
    flexWrap: 'wrap'
  },
  '@media only screen and (max-width: 500px)': {
    flexDirection: 'column'
  }
}))

export const BlockMidElement = styled('div', () => ({
  // backgroundColor: 'red',
  marginTop: '3rem',
  width: '100%',
  // height: '40rem',
  display: 'block'
  // position: 'absolute'
}))

export const BlockBotElement = styled('div', () => ({
  marginTop: '1rem',
  '@media only screen and (max-width: 768px)': {
    marginBottom: '-2rem'
  }
}))

export const BlockLoading = styled('div', () => ({
  width: '100%',
  height: `calc(100vh - 90px)`,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
}))
