import { styled } from 'baseui'

export const Form = styled('form', ({ $theme }) => ({
  // height: '70vh',
  height: '30.8rem',
  backgroundColor: $theme.colors.backgroundF0,
  '@media only screen and (max-width: 500px)': {
    // height: '25.6rem'
    height: '22.9rem'
  },
  '@media only screen and (max-width: 400px)': {
    // height: '25.6rem'
    height: '20.9rem'
  },
  '@media only screen and (max-width: 320px)': {
    height: '20.8rem'
  }
}))

export const ButtonGroup = styled('div', ({ $theme }) => ({
  // position: 'absolute',
  bottom: 0,
  padding: '24px 0',
  display: 'flex',
  width: '100%',
  alignItems: 'center',
  // width: 'calc(100% - 48px)',
  backgroundColor: '#ffffff',
  // boxShadow: '0 0 3px rgba(0, 0, 0, 0.1)'
  borderTop: '1px solid rgba(0, 0, 0, 0.1)'
}))

export const Container = styled('div', () => ({
  overflowX: 'hidden',
  display: 'flex',
  flexDirection: 'row',
  // height: '100%',
  // height: '50rem',
  overflowY: 'auto',

  '@media only screen and (max-width: 1280px)': {
    flexDirection: 'column'
  }
}))

export const Wrapper = styled('div', () => ({
  display: 'flex',
  position: 'relative',
  flexDirection: 'column',
  // height: '50rem',
  paddingBottom: '1rem',
  width: '100%',
  // overflowY: 'scroll',
  '@media only screen and (max-width: 1024px)': {
    width: '100%'
  }
}))

export const RequiredWrapper = styled('div', () => ({
  display: 'flex',
  width: '100%',
  margin: '0.1rem 0 0.5rem 1.25rem ',
  zIndex: 10,
  flexDirection: 'row',
  alignItems: 'flex-end',
  justifyContent: 'flex-start'
}))

export const FieldDetails = styled('span', ({ $theme }) => ({
  ...$theme.typography.font14,
  padding: '28px 0 15px',
  color: $theme.colors.textNormal,
  display: 'block',

  '@media only screen and (max-width: 991px)': {
    padding: '30px 0'
  }
}))
