export interface RegisterCustomerForm {
  name: string
  age: string
  gender: string
  contact_number: string
  email: string
  address: string
  postalCode: string
}
