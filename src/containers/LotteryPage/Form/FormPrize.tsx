/* eslint-disable */
import React, { useCallback } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation } from '@apollo/react-hooks'
import { Scrollbars } from 'react-custom-scrollbars'
import Input from '../../../components/Input/Input'
import Button, { KIND } from '../../../components/Button/Button'
// import { Input } from 'antd'
import { Form, ButtonGroup, Container, Wrapper } from './FormPrize.style'
import queryString from 'query-string'
import { useLocation } from 'react-router-dom'
import {
  FormFields,
  FormLabel,
  Error
} from '../../../components/FormFields/FormFields'
import { Notification } from '../../../components/Notification/NotificationCustom'
import { OverideSave, OverideCancel } from './Overrides'
import { MUTATION_CREATE_CUSTOMER } from '../../../graphql/mutation/create-customer'
import { RegisterCustomerForm } from './Interface'

interface EventFormInput {
  name: string
  contact_number: string
  email: string
  address: string
}

type Props = any
const FormPrize: React.FC<Props> = (props) => {
  const location = useLocation()
  const eventID = queryString.parse(location.search).eventId
  const userRef = React.useRef<any>(null)
  const { register, handleSubmit, setValue, errors, reset } =
    useForm<RegisterCustomerForm>()
  const [text, setText] = React.useState('')

  const [AddCustomer] = useMutation(MUTATION_CREATE_CUSTOMER, {
    fetchPolicy: 'no-cache'
  })

  const onSubmit = (data: RegisterCustomerForm) => {
    console.log(data)
    // Notification({
    //   type: 'success',
    //   message: 'ユーザ作成に成功しました。'
    // })
    // props.setVisible(false)
    // Form.presetFields()
    AddCustomer({
      variables: {
        createCustomerInput: {
          name: data.name,
          age: parseFloat(data.age),
          gender: data.gender,
          tel: data.contact_number,
          email: data.email,
          address: data.address,
          postalCode: data.postalCode,
          prizeId: props.idspin
        }
      }
    })
      .then(() => {
        Notification({
          type: 'success',
          message: 'ご応募頂き有難うございました。'
        })
        props.setVisible(false)
        reset()
      })
      .catch((err) => Notification({ type: 'error', message: err.toString() }))
  }
  React.useEffect(() => {
    register({ name: 'name', required: true, maxLength: 50 })
    register({ name: 'tel', required: true, maxLength: 20 })
    register({ name: 'email', required: true, maxLength: 100 })
    register({ name: 'address', required: true })
    register({ name: 'age', required: true })
    register({ name: 'gender', required: true })
    register({ name: 'postalCode', required: true })
  }, [register])

  console.log(props.idspin, 'idspin')

  React.useEffect(() => {
    if (userRef.current) userRef.current.focus()
  }, [userRef])

  return (
    <>
      <Form onSubmit={handleSubmit(onSubmit)} id='myForm'>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflow: 'auto' }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: 'none' }}
              className='track-horizontal'
            />
          )}
        >
          <Wrapper>
            <p style={{ color: 'rgb(102, 109, 146)' }}>
              必要な情報を記入して下さい。
              <span style={{ color: 'red' }}>(*)は必須の項目です</span>
            </p>
            <FormFields>
              <FormLabel>
                氏名&nbsp;<span style={{ color: 'red' }}>*</span>
              </FormLabel>
              <Input
                name='name'
                clearOnEscape
                inputRef={(e: any) => {
                  register(e, {
                    required: true,
                    maxLength: 50,
                    minLength: 4
                  })
                  userRef.current = e
                }}
              />
              {errors.name && errors.name.type === 'required' && (
                <Error>これは必須の項目です。</Error>
              )}
              {errors.name && errors.name.type === 'minLength' && (
                <Error>氏名は4文字以上設定してください。</Error>
              )}
              {errors.name && errors.name.type === 'maxLength' && (
                <Error>氏名は50文字以下設定してください。</Error>
              )}
            </FormFields>
            <FormFields>
              <FormLabel>
                年齢&nbsp;<span style={{ color: 'red' }}>*</span>
              </FormLabel>
              <Input
                type='number'
                inputRef={register({ max: 200, maxLength: 50, required: true })}
                name='age'
              />
              {errors.age && errors.age.type === 'required' && (
                <Error>これは必須の項目です。</Error>
              )}
              {errors.age && errors.age.type === 'max' && (
                <Error>年齢に２００歳以下入力してください</Error>
              )}
              {errors.age && errors.age.type === 'maxLength' && (
                <Error>お届け先は50文字以下</Error>
              )}
            </FormFields>
            <FormFields>
              <FormLabel>
                性別&nbsp;<span style={{ color: 'red' }}>*</span>
              </FormLabel>
              <Input
                inputRef={register({ maxLength: 50, required: true })}
                name='gender'
              />
              {errors.gender && errors.gender.type === 'required' && (
                <Error>これは必須の項目です。</Error>
              )}
              {errors.gender && errors.gender.type === 'maxLength' && (
                <Error>お届け先は50文字以下</Error>
              )}
            </FormFields>
            <FormFields>
              <FormLabel>
                住所&nbsp;<span style={{ color: 'red' }}>*</span>
              </FormLabel>
              <Input
                inputRef={register({ maxLength: 50, required: true })}
                name='address'
              />
              {errors.address && errors.address.type === 'required' && (
                <Error>これは必須の項目です。</Error>
              )}
              {errors.address && errors.address.type === 'maxLength' && (
                <Error>お届け先は50文字以下</Error>
              )}
            </FormFields>
            <FormFields>
              <FormLabel>
                メールアドレス&nbsp;<span style={{ color: 'red' }}>*</span>
              </FormLabel>
              <Input
                inputRef={register({
                  required: true,
                  maxLength: 100,
                  pattern:
                    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                })}
                name='email'
              />
              {errors.email && errors.email.type === 'required' && (
                <Error>これは必須の項目です。</Error>
              )}
              {errors.email && errors.email.type === 'pattern' && (
                <Error>メールアドレスの形式は正しくありません。</Error>
              )}
              {errors.email && errors.email.type === 'maxLength' && (
                <Error>メールアドレス100文字以下設定してください。</Error>
              )}
            </FormFields>
            <FormFields>
              <FormLabel>
                電話番号&nbsp;<span style={{ color: 'red' }}>*</span>
              </FormLabel>
              <Input
                name='contact_number'
                inputRef={register({ maxLength: 20, required: true })}
              />
              {errors.contact_number &&
                errors.contact_number.type === 'required' && (
                  <Error>これは必須の項目です。</Error>
                )}
              {errors.contact_number &&
                errors.contact_number.type === 'maxLength' && (
                  <Error>電話番号は20文字以下設定してください。</Error>
                )}
            </FormFields>

            <FormFields>
              <FormLabel>
                郵便番号&nbsp;<span style={{ color: 'red' }}>*</span>
              </FormLabel>
              <Input
                inputRef={register({ maxLength: 50, required: true })}
                name='postalCode'
              />
              {errors.postalCode && errors.postalCode.type === 'required' && (
                <Error>これは必須の項目です。</Error>
              )}
              {errors.postalCode && errors.postalCode.type === 'maxLength' && (
                <Error>お届け先は50文字以下</Error>
              )}
            </FormFields>
          </Wrapper>
        </Scrollbars>
        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={() => props.setVisible(false)}
            overrides={OverideCancel}
          >
            キャンセル
          </Button>
          <Button type='submit' overrides={OverideSave}>
            追加
          </Button>
        </ButtonGroup>
      </Form>
    </>
  )
}
export default FormPrize
