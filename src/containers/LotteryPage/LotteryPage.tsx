/* eslint-disable */
import React, { useEffect } from 'react'
import Header from './Header/Header'
import Footer from './Footer/Footer'
import {
  ImgCustom,
  PrizeName,
  FormBody,
  WrapperBody,
  BlockTopElement,
  BlockMidElement,
  BlockBotElement,
  BlockLoading
} from './LotteryPage.style'
import { use100vh } from 'react-div-100vh'
// import backGround from '../../assets/images/Panner.png'
import backGround from './assets/Panner.png'
// import bannerDefault from '../../assets/images/Panner.png'
import line from '../../assets/images/line.png'

import TopLandingPage from '../../components/TopLandingPage/TopLandingPage'
import CongratulationsPC from './LuckyDay/LuckyOpen'
import LuckyOpenIpad from './Spinning/LuckyOpenIpad'
import BotLandingPage from './Bot/BotLandingPage'
import { QUERY_EVENT } from '../../graphql/query/event'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { useLocation } from 'react-router-dom'
import queryString from 'query-string'
import Introduction from './Introduction/Introduction'
import { animations } from 'react-animations'
import { FlapperSpinnerLoading } from './Spinning/LuckyOpenIpad.style'
import AOS from 'aos'
import 'aos/dist/aos.css'
import backgroundInline from '../../assets/images/backgroundInline.png'
import {
  BalloonsTest,
  BalloonsRight,
  DivBalloons,
  DivBackTop,
  ImgScrollTop
} from './Spinning/LuckyOpenIpad.style'
import Balloon from '../../assets/images/ball (6).png'
import TitleVermuda from '../../assets/images/Title.png'
import { BackTop } from 'antd'
import BackToTop from '../../assets/images/backtop.png'
import { MUTATION_VISIT } from '../../graphql/mutation/total-visit'

function LotteryPage() {
  AOS.init()
  const height = use100vh()
  const location = useLocation()
  const eventID = queryString.parse(location.search).eventId

  const { data, loading } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventID
    }
  })

  const [increaseTotalVisit] = useMutation(MUTATION_VISIT, {
    fetchPolicy: 'no-cache'
  })

  useEffect(() => {
    increaseTotalVisit({
      variables: {
        eventId: eventID
      }
    })
  }, [])

  return (
    <div
      style={{
        backgroundImage: 'url(' + backgroundInline + ')',
        backgroundColor: 'rgb(255, 245, 220)'
      }}
    >
      <Header />
      <WrapperBody>
        {loading ? (
          <BlockLoading>
            <FlapperSpinnerLoading
              size={50}
              // color={'black'}
              // loading={true}
              sizeUnit={'px'}
            />
          </BlockLoading>
        ) : (
          <FormBody>
            <div>
              {
                <ImgCustom
                  src={
                    data?.event.banner === null ||
                    data?.event.banner === undefined
                      ? backGround
                      : data?.event.banner
                    // backGround
                  }
                  alt=''
                />
              }
            </div>
            <DivBalloons>
              <BalloonsTest>
                <img
                  src={Balloon}
                  alt=''
                  style={{
                    objectFit: 'contain',
                    width: '100%',
                    height: 'auto'
                  }}
                />
              </BalloonsTest>
              <BalloonsRight>
                <img
                  src={Balloon}
                  alt=''
                  style={{
                    objectFit: 'contain',
                    width: '100%',
                    height: 'auto'
                  }}
                />
              </BalloonsRight>
            </DivBalloons>
            <PrizeName>当選者へのプレゼント</PrizeName>
            {/* <PrizeName>
              <img
                src={TitleVermuda}
                alt=""
                style={{ width: "25rem", height: "auto" }}
              />
            </PrizeName> */}
            <BlockTopElement>
              {data?.event.prizes
                .filter((x) => x.rank !== 0)
                .map((item, index) => (
                  <TopLandingPage
                    key={index}
                    {...item}
                    name={item.name}
                    quantity={item.quantity}
                    rank={item.rank}
                    imageUrl={item.imageUrl}
                  />
                ))}
            </BlockTopElement>
            <PrizeName>抽選システム案内 </PrizeName>
            <Introduction />
            <BlockMidElement>
              <LuckyOpenIpad />
            </BlockMidElement>
            <BlockBotElement>
              <BotLandingPage />
            </BlockBotElement>
          </FormBody>
        )}
      </WrapperBody>
      <BackTop>
        <DivBackTop alt='asss'>
          <ImgScrollTop
            src={BackToTop}
            alt=''
            // style={{ width: '100%', height: 'auto' }}
          />
        </DivBackTop>
      </BackTop>
      <div style={{ marginTop: '-9rem' }}>
        <Footer />
      </div>
    </div>
  )
}

export default LotteryPage
