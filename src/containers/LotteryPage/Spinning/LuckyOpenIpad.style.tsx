import styled, { keyframes } from 'styled-components'
import { Button } from 'antd'
import Modal from 'antd/lib/modal/Modal'
import FlapperSpinner from '../../../components/Loading/index'

export const Wrapper = styled.div`
  text-align: center;
  background-color: #fff;
  background: radial-gradient(circle, rgb(227 64 33) 47%, rgb(190 8 34) 80%);
`

export const ButtonCustom = styled(Button)`
  &.google {
    background-color: #4285f4;
  }

  &.facebook {
    background-color: #4267b2;
    margin-bottom: 10px;
  }

  @media (max-width: 767px) {
    font-size: 15px;
  }
`

export const ContainerVideo = styled.div`
  height: 60%;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 1366px) and (max-height: 1024px) {
    height: 55%;
  }
  @media (max-width: 768px) {
    height: 50%;
  }
`

export const TriangleTop = styled.div`
  width: 0px;
  height: 0px;
  border-top: 180px solid #ead608;
  border-right: 800px solid transparent;

  @media (max-width: 1024px) {
    width: 0px;
    height: 0px;
    border-top: 130px solid #ead608;
    border-right: 600px solid transparent;
  }
  @media (max-width: 768px) {
    width: 0px;
    height: 0px;
    border-top: 150px solid #ead608;
    border-right: 600px solid transparent;
  }
  @media (max-width: 500px) {
    width: 0px;
    height: 0px;
    border-top: 6rem solid #ead608;
    border-right: 23rem solid transparent;
  }
  @media (max-width: 375px) {
    width: 0px;
    height: 0px;
    border-top: 100px solid #ead608;
    border-right: 23rem solid transparent;
  }
  @media (max-width: 320px) {
    width: 0px;
    height: 0px;
    border-top: 100px solid #ead608;
    border-right: 320px solid transparent;
  }
`
export const TriangleBottom = styled.div`
  // position: absolute;
  bottom: 0px;
  right: 0px;
  width: 0px;
  height: 0px;
  border-bottom: 180px solid #ead608;
  border-left: 800px solid transparent;
  @media (max-width: 1024px) {
    // position: absolute;
    bottom: 0px;
    right: 0px;
    width: 0px;
    height: 0px;
    border-bottom: 130px solid #ead608;
    border-left: 600px solid transparent;
  }
  @media (max-width: 768px) {
    // position: absolute;
    bottom: 0px;
    right: 0px;
    width: 0px;
    height: 0px;
    border-bottom: 150px solid #ead608;
    border-left: 600px solid transparent;
  }
  @media (max-width: 500px) {
    bottom: 0px;
    right: 0px;
    width: 0px;
    height: 0px;
    border-bottom: 6rem solid #ead608;
    border-left: 23rem solid transparent;
  }
`
export const WrapperTitleEvent = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  flex-flow: column;
  margin-top: -2.5rem;
  @media (max-width: 768px) {
    margin-top: -1rem;
  }
`
export const NameEvent = styled.div`
  // position: absolute;
  text-align: center;
  top: 2vh;
  font-size: 3.5rem;
  color: #fd0711;
  font-weight: bold;
  text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff,
    1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff;
  @media (min-width: 1366px) {
    margin-top: -90px;
  }
  @media (max-width: 1024px) {
    /* font-size: 3.5rem; */
    margin-top: -5px;
  }
  @media (max-width: 768px) {
    top: 2vh;
    /* font-size: 3.5rem; */
    margin-top: -2rem;
    margin-bottom: 1rem;
  }
  @media (max-width: 500px) {
    font-size: 2.3rem;
    margin-bottom: 0;
  }
`
export const StatusEvent = styled.div`
  font-size: 2rem;
  height: 4rem;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #26578a;
  font-weight: bold;
  margin-bottom: 30px;
  text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff,
    1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff;
  @media (max-width: 768px) {
    margin-bottom: 20px;
    font-size: 2rem;
    padding-left: 4rem;
    padding-right: 4rem;
  }
  @media (max-width: 500px) {
    font-size: 1.2rem;
    margin-top: -2px;
    margin-bottom: 18px;
    padding-left: 0.5rem;
    padding-right: 0.5rem;
  }
`

export const EventEndsTime = styled.div`
  font-size: 2rem;
  font-weight: bold;
  color: #fd0711;
  // position: absolute;
  bottom: 40px;
  right: 60px;
  text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff,
    1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff;
  @media (max-width: 768px) {
    font-size: 1.5rem;
  }
  @media (max-width: 1024px) and (max-height: 768px) {
    font-size: 1.5rem;
  }
`

export const WrapperVideo = styled.div`
  height: 30rem;
  border: 0px solid #fff;
  /* box-shadow: 2px 2px 30px 10px #f37378, 0px 1px 1px 0px rgba(0, 0, 0, 0.14),
    0px 1px 3px 0px rgba(0, 0, 0, 0.12); */
  @media (max-width: 768px) {
    height: 24rem;
    /* width: 28rem; */
  }
  @media (max-width: 500px) {
    height: 15rem;
  }
`
export const WrapperButton = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 2.5rem;
  margin-bottom: -5rem;

  .ant-btn-dangerous.ant-btn-primary[disabled]:active {
    background-color: 'red';
  }
  .ant-btn-dangerous.ant-btn-primary {
    box-shadow: rgba(0, 0, 0, 0.08) -2px -2px 0px inset;
  }
  @media (max-width: 1024px) {
    margin-bottom: -2rem;
  }
  @media (max-width: 768px) {
    margin-bottom: 0;
  }
`

export const WrapperPrize = styled.div`
  position: relative;
  width: 50rem;
  height: 30rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: #ead608;
  @media (max-width: 768px) {
    width: 40rem;
    height: 25rem;
  }
  @media (max-width: 500px) {
    width: 22rem;
    height: 15rem;
  }
  @media (max-width: 375px) {
    width: 19rem;
    height: 15rem;
  }
`

export const WrapperArmorial = styled.div`
  position: absolute;
  width: 150px;
  height: 150px;
  top: 20px;
  right: 0;

  @media (max-width: 768px) {
    width: 8rem;
    height: 8rem;
    top: 15px;
  }
  @media (max-width: 500px) {
    width: 5rem;
    height: 5rem;
    top: 1rem;
    right: 1rem;
  }
`

export const Ranking = styled.div`
  position: absolute;
  width: 100px;
  display: flex;
  justify-content: center;
  top: 30px;
  right: 25px;
  color: #ef3e46;
  font-weight: bold;
  font-size: 2rem;
  @media (max-width: 768px) {
    top: 1.3rem;
    right: 1rem;
  }
  @media (max-width: 500px) {
    top: 0.7rem;
    right: -1.6rem;
    font-size: 1rem;
  }
`
export const WrapperImg = styled.div`
  width: 370px;
  height: 320px;
  @media (max-width: 768px) {
    width: 18rem;
    height: 14rem;
  }
  @media (max-width: 500px) {
    width: 13rem;
    height: 8rem;
  }
`

export const RankPrize = styled.div`
  font-size: 2rem;
  color: #fd0711;
  font-weight: 600;
  @media (max-width: 500px) {
    font-size: 1.5rem;
  }
`
export const NamePrize = styled.div`
  font-size: 2.5rem;
  color: #26578a;
  font-weight: 500;
  @media (max-width: 768px) {
    font-size: 2rem;
  }
  @media (max-width: 500px) {
    font-size: 1.5rem;
  }
`

export const SpinnerContainer = styled.div`
  // padding-top: 30rem !important;
  display: flex !important;
  flex-direction: column !important;
  height: auto !important;
  weight: 100% !important;
  top: 29rem !important;
  left: 10rem !important;
  /* width: 100vw; */
  @media (max-width: 768px) {
    padding: 5px 5px 0;
  }
  .eSwYtm {
    top: 1rem !important;
    left: 22rem !important;
  }
  .wheel > div:last-child {
    width: 50% !important;
    max-height: 700px !important;
  }
`

export const InputContainer = styled.div`
  padding: 5px 5px 0;
  z-index: 1;
  @media (max-width: 768px) {
    padding: 5px 5px 0;
  }
`

export const LogoWrapper = styled.div`
  margin-bottom: 30px;

  img {
    max-width: 160px;
  }
`

export const Heading = styled.h3`
  color: #009e7f;
  margin-bottom: 10px;
  font-family: 'Poppins', sans-serif;
  font-size: 21px;
  font-weight: 700;

  @media (max-width: 767px) {
    font-size: 15px;
  }
`

export const SubHeading = styled.span`
  margin-bottom: 40px;
  font-family: 'Lato', sans-serif;
  font-size: 15px;
  font-weight: 400;
  color: #77798c;
  display: block;

  @media (max-width: 767px) {
    font-size: 15px;
  }
`

export const OfferSection = styled.div`
  padding: 20px;
  background-color: #f7f7f7;
  color: #009e7f;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const Offer = styled.p`
  font-family: 'Lato', sans-serif;
  font-size: 15px;
  font-weight: 400;
  margin: 0;

  @media (max-width: 767px) {
    font-size: 15px;
  }
`

export const HelperText = styled.p`
  font-family: 'Lato', sans-serif;
  font-size: 15px;
  font-weight: 400;
  color: #77798c;
  margin: 0;
  text-align: center;
  width: 100%;

  a {
    font-weight: 700;
    color: #4285f4;
    text-decoration: underline;
  }
`

export const Input = styled.input`
  width: 100%;
  height: 48px;
  border-radius: 0px;
  background-color: #f7f7f7;
  border: 1px solid#E6E6E6;
  font-family: 'Lato', sans-serif;
  font-size: 15px;
  font-weight: 400;
  color: #0d1136;
  line-height: 19px;
  padding: 0 18px;
  box-sizing: border-box;
  transition: border-color 0.25s ease;
  margin-bottom: 20px;

  &:hover,
  &:focus {
    outline: 0;
  }

  &:focus {
    border-color: #009e7f;
  }

  &::placeholder {
    color: #77798c;
    font-size: 14px;
  }

  &::-webkit-inner-spin-button,
  &::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  &.disabled {
    .inner-wrap {
      cursor: not-allowed;
      opacity: 0.6;
    }
  }

  @media (max-width: 767px) {
    font-size: 14px;
  }
`
export const VideoXXX = styled.video`
  /* width: 50vw; */
  height: 100%;
  display: block;
  object-fit: cover;
  z-index: 100;

  @media (max-width: 1024px) {
    /* width: calc(100vw - 1rem); */
    width: 80vw;
  }
  @media (max-width: 500px) {
    width: 90vw;
    height: 15rem;
  }
`
export const ImageXXX = styled.img`
  object-fit: contain !important;
  width: 100%;
  height: 100%;

  @media (max-width: 500px) {
    width: 80%;
    height: 80%;
    margin-right: -2rem;
  }
`
export const ImageGift = styled.img`
  width: 30rem;
  height: 30rem;
  position: absolute;
  z-index: 10;

  /* animation: shake 4s cubic-bezier(0.36, 0.07, 0.19, 0.97) both; */
  /* animation: shake 1.5s alternate infinite ease-in;
  animation-iteration-count: infinite; */
  /* transform: translate3d(0, 0, 0); */
  /* transform: scale(1.1); */

  /* backface-visibility: hidden; */
  /* perspective: 1000px; */

  @keyframes shake {
    0% {
      transform: scale(1);
    }
    100% {
      /* transform: translate3d(-1px, 0, 0); */
      transform: scale(1.1);
    }
  }

  @media (max-width: 768px) {
    width: 24rem;
    height: 24rem;
  }
  @media (max-width: 500px) {
    width: 16rem;
    height: 16rem;
  }
`
export const ModalCustom = styled(Modal)`
  padding: 0;

  .ant-modal-body {
    height: 38rem;
    padding: 12px 24px;
    @media (max-width: 500px) {
      /* height: 32rem; */
      height: 30rem;
    }
    @media (max-width: 400px) {
      /* height: 32rem; */
      height: 28rem;
    }
    @media (max-width: 320px) {
      height: 28rem;
      display: block;
    }
  }
  .ant-modal-header {
    height: 4.8rem;
    display: flex;
    align-items: center;
    padding: 16px 0;
    margin-left: 1.5rem;
    margin-right: 1.5rem;
    @media (max-width: '500px') {
      height: 4rem;
    }
  }
`
export const FlapperSpinnerLoading = styled(FlapperSpinner)``

const move = () => keyframes`
   0%, 100%, 50% {
        opacity: 1;
   };
   25%, 75%{
     opacity:0
   }
`
const zoom = () => keyframes`
   0% {
        opacity: 0;
        transform: scale(1)
   };
   100%{
     opacity:0.2;
     transform: scale(1.05)
   }
`

export const DemoDiv = styled.div`
  mix-blend-mode: screen;
  will-change: transform;
  /* will-change: opacity; */
  animation: ${() => move()} 6s infinite;
`
export const DemoDiv1 = styled.div`
  mix-blend-mode: screen;
  will-change: transform;
  /* will-change: opacity; */
  animation: ${() => move()} 6s infinite;
  animation-delay: 1s;
`
export const DemoDiv2 = styled.div`
  mix-blend-mode: screen;
  will-change: transform;
  /* will-change: opacity; */
  animation: ${() => move()} 6s infinite;
  animation-delay: 2s;
`
export const DemoDiv3 = styled.div`
  mix-blend-mode: screen;
  will-change: transform;
  /* will-change: opacity; */
  animation: ${() => move()} 6s infinite;
  animation-delay: 3s;
`

export const BlockSunLight = styled.div`
  display: flex;
  justify-content: center;
`
export const ImgSun = styled.img`
  mix-blend-mode: screen;
  width: 30rem;
  height: 30rem;
  animation: ${() => zoom()} 1.2s infinite;
  @media (max-width: 768px) {
    width: 24rem;
    height: 24rem;
  }
  @media (max-width: 500px) {
    width: 16rem;
    height: 16rem;
  }
  /* will-change: transform; */
  /* will-change: opacity; */
`
export const LightImage = styled.img`
  width: 15rem;
  height: 7rem;
  position: absolute;
  left: 20%;
  margin-top: -10%;
`
export const LightImage1 = styled.img`
  width: 20rem;
  height: 10rem;
  position: absolute;
  @media (max-width: 500px) {
    width: 10rem;
    height: 5rem;
  }
`
export const LightImage2 = styled.img`
  width: 10rem;
  height: 5rem;
  position: absolute;
  left: 20%;
  margin-top: -10%;
`
export const LightImage3 = styled.img`
  width: 17rem;
  height: 8rem;
  position: absolute;
  margin-top: -5%;
  @media (max-width: 500px) {
    width: 10rem;
    height: 5rem;
  }
`

const bounceInUp = () => keyframes`
            0% {
               opacity: 0;
               transform: translateY(1600px);
            };
            60% {
               opacity: 1;
               transform: translateY(-30px);
            };
            80% {
               transform: translateY(10px);
            };
            100% {
               transform: translateY(0);
            }
`
export const Balloons = styled.div`
  position: absolute;
  width: 122px;
  height: 378px;
  animation: ${() => bounceInUp()} 10s infinite;
  animation-fill-mode: both;
`
export const BalloonsTest = styled.div`
  position: absolute;
  margin-left: -14rem;
  width: 400px;
  height: auto;
  animation: ${() => bounceInUp()} 10s infinite;
  animation-fill-mode: both;
  @media (max-width: 1600px) {
    margin: 0;
    max-width: 250px;
    height: auto;
  }
  // @media (max-width: 1300px) {
  //   width: 200px;
  //   height: auto;
  // }
`
export const BalloonsRight = styled.div`
  position: absolute;
  margin-right: -14rem;
  margin-top: 15rem;
  width: 400px;
  height: auto;
  animation: ${() => bounceInUp()} 10s infinite;
  animation-delay: 4s;
  animation-fill-mode: both;
  right: 0;
  @media (max-width: 1600px) {
    margin-right: 0;
    margin-top: 22rem;
    max-width: 250px;
    height: auto;
  }
  @media (max-width: 500px) {
    display: none;
  }
  // @media (max-width: 1300px) {
  //   width: 200px;
  //   height: auto;
  // }
`

export const DivBalloons = styled.div`
  max-width: 1300px;
  display: flex;
  justify-content: space-between;
  position: relative;
`

const backtop = () => keyframes`
            0% {
               transform: translateX(0)
            };
            15% {
               transform: translateX(-25%) rotate(-5deg);
            };
            30% {
               transform: translateX(20%) rotate(3deg);
            };
            45% {
               transform: translateX(-15%) rotate(-3deg);
            };
            60% {
              transform: translateX(10%) rotate(2deg);
            };75% {
               transform: translateX(-5%) rotate(-1deg);
            };100% {
               transform: translateX(0);
            };
`
export const DivBackTop = styled.div`
  width: 5rem;
  height: auto;
  .ant-back-top {
    right: 120px;
    bottom: 120px;
    @media (max-width: 500px) {
      right: 60px;
      bottom: 60px;
    }
  }
  @media (max-width: 500px) {
    width: 3rem;
  }
`

export const ImgScrollTop = styled.img`
  width: 100%;
  height: auto;
  animation: ${() => backtop()} 5s infinite;
`

const Swing = () => keyframes`
 20%{
  transform: rotate(15deg);
 };
 40%{
   transform:  rotate(-10deg);
 };
 60%{
   transform: rotate(5deg);
 };
 80%{
   transform:  rotate(-5deg);
 };
 100%{
   transform:  rotate(0);
 };
 
`

export const DivLamp = styled.div`
  max-width: 1300px;
  display: flex;
  position: relative;
`

export const LampOut = styled.img`
  width: 3rem;
  height: auto;
  position: absolute;
  right: 6rem;
  margin-top: -233px;
  animation: ${() => Swing()} 12s infinite;
  @media (max-width: 800px) {
    right: 3rem;
  }
  @media (max-width: 500px) {
    right: 1.5rem;
    width: 1.5rem;
    margin-top: -110px;
  }
`
export const LampIn = styled.img`
  width: 2rem;
  height: auto;
  position: absolute;
  right: 11rem;
  margin-top: -155px;
  animation: ${() => Swing()} 12s infinite;
  animation-delay: 1s;
  @media (max-width: 800px) {
    right: 7rem;
  }
  @media (max-width: 500px) {
    right: 3.5rem;
    width: 1rem;
    margin-top: -75px;
  }
`
