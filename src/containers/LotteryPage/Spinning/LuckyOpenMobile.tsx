/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import Confetti from 'react-confetti'
import {
  ButtonCustom,
  Input,
  Heading,
  Wrapper,
  Container,
  Divider,
  SpinnerContainer,
  InputContainer
} from './Roultte.style'
import { Wheel } from 'react-custom-roulette'
import { Button } from 'baseui/button'
// import Fade from 'react-reveal/Fade'
import { data as prizeData } from './data'
import gold from '../../../assets/images/gold.png'
import Cat from '../../../assets/images/meo1.png'
import { useMutation } from '@apollo/react-hooks'
import { MUTATION_SPIN } from '../../../graphql/mutation/spin'
import { useLocation } from 'react-router-dom'
import queryString from 'query-string'
import { Notification } from '../../../components/Notification/NotificationCustom'

const converPostion = {
  0: '一',
  1: '二',
  2: '三',
  3: 'Good luck'
}
var useCanvas: any
export default function Congratulations() {
  const location = useLocation()
  const eventId = queryString.parse(location.search).eventId
  const [spinning, setSpinning] = React.useState(false)
  const [prize, setPrize] = React.useState(false)
  const [position, setPosition] = React.useState(null)
  const [toggleReload, setToggleReload] = React.useState(false)
  const [spinCount, setSpinCount] = React.useState(0)
  const [prizeValue, setPrizeValue] = useState(1)
  const canvas = React.useRef()
  const [spinCircle] = useMutation(MUTATION_SPIN, {
    fetchPolicy: 'no-cache'
  })
  useEffect(() => {
    useCanvas = canvas.current
  }, [])

  const postSerialCode = () => {
    // setToggleReload(!toggleReload)
    if (spinning) return
    setSpinning(true)
    setSpinCount(spinCount + 1)
  }

  useEffect(() => {
    spinCircle({
      variables: {
        eventId
      }
    })
      .then(({ data }) => {
        // for (let i = 0; i < prizeData.length; i++) {
        const idx = data.spin.campaign.prizes
          ?.map((item) => item.id)
          .findIndex((x) => x === data.spin.prize?.id)

        console.log(
          data.spin.campaign.prizes
            ?.map((item) => item.id)
            .findIndex((x) => x === data.spin.prize?.id)
        )
        if (idx > -1) {
          setPosition(idx)
          // break
        } else if (data.spin.prize === null) {
          setPosition(3)
          // break
        }

        setSpinning(true)
        postSerialCode()
        setPrizeValue(data)
      })
      .catch((err) => {
        Notification({ type: 'error', message: err.toString() })
      })
  }, [])
  // console.log(position)

  const drawCanvas = (
    text1: string,
    text2: string
    // text3: any,
    // text4: string
  ) => {
    var ctx: any
    if (useCanvas.getContext) {
      ctx = useCanvas.getContext('2d')
      ctx.font = 'bold 40px Helvetica, Arial'
      ctx.fillStyle = 'blue'
      ctx.fillText(text1, 40, 140)
      ctx.fillText(text2, 100, 140)

      // Point
      ctx.fillStyle = 'blue'
      ctx.font = 'bold 31px Helvetica, Arial'

      // if (parseInt(text3) < 10) {
      //   ctx.fillText(text3, 40, 57)
      // } else {
      //   ctx.fillText(text3, 34, 57)
      // }
      // //point
      // ctx.font = 'bold 14px Helvetica, Arial'
      // ctx.fillText(text4, 23, 73)
      // ctx.fillStyle = 'red'

      ctx.lineWidth = 1
      ctx.restore()
    }
  }
  const clearCanvas = () => {
    const ctx = useCanvas.getContext('2d')
    ctx.save()
    // Use the identity matrix while clearing the canvas
    ctx.setTransform(1, 0, 0, 1, 0, 0)
    ctx.clearRect(0, 0, useCanvas.width, useCanvas.height)
    // Restore the transform
    ctx.restore()
  }

  return (
    <Wrapper
      style={{
        background: 'url(' + gold + ') no-repeat center center/cover',
        width: '100%',
        height: '100vh'
      }}
    >
      <img
        src={Cat}
        alt=''
        style={
          {
            // marginLeft: '8rem',
            // marginTop: '8rem',
            // width: '30rem'
          }
        }
      />
      <canvas
        style={{
          position: 'absolute',
          top: '61%',
          left: '33%',
          zIndex: 14
          // height: '50vh',
          // width: '100%'
        }}
        ref={canvas}
      />
      <Container>
        {!prize ? (
          <div>
            <SpinnerContainer>
              <Wheel
                mustStartSpinning={spinning}
                prizeNumber={position}
                data={prizeData}
                fontSize={20}
                textDistance={60}
                backgroundColors={['#3e3e3e', '#df3428']}
                textColors={['#ffffff']}
                onStopSpinning={function () {
                  setTimeout(function () {
                    setPrize(true)
                    setSpinning(false)
                  }, 1000)
                }}
              />
            </SpinnerContainer>
          </div>
        ) : (
          <div
            style={{
              display: 'flex',
              width: '100%',
              // height: '68vh',
              position: 'absolute',
              bottom: 0,
              justifyContent: 'center',
              alignItems: 'flex-end',
              alignContent: 'center'
            }}
          >
            <Confetti
              id='confetti'
              gravity={0.1}
              run={prize}
              tweenDuration={2}
              style={{
                height: '100%',
                width: '100%',
                zIndex: 1
              }}
            />
            <div>
              <video
                // height={'50vh'}
                width={'100%'}
                src='https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4'
                autoPlay={true}
                playsInline={true}
                muted={true}
                preload='yes'
                // controls={true}
                style={{
                  height: '50vh',
                  width: '100vw',
                  display: 'block',
                  objectFit: 'cover'
                }}
                onEnded={() => {
                  setToggleReload(true)
                  drawCanvas(
                    converPostion[position],
                    position !== 3 ? '等賞' : ''
                  )
                }}
              ></video>
            </div>
          </div>
        )}
        {/* 
        <div
          style={{
            display: 'flex',
            width: '100%',
            flexDirection: 'column',
            alignContent: 'center',
            alignItems: 'center',
          }}
        >
          <span style={{ marginLeft: 4, marginRight: 4 }}>
            <FormattedMessage
              id='orTextd'
              defaultMessage='シリアルコードの入力'
            />
          </span>
        </div> */}
        {/* {!toggleReload && (
          <Button onClick={() => window.location.reload()}>Reload</Button>
        )} */}
        {toggleReload && (
          <InputContainer>
            <Button
              onClick={() => {
                window.location.reload()
              }}
              overrides={{
                BaseButton: {
                  style: ({ $theme }) => ({
                    borderTopLeftRadius: '3px',
                    borderTopRightRadius: '3px',
                    borderBottomRightRadius: '3px',
                    borderBottomLeftRadius: '3px'
                  })
                }
              }}
            >
              {toggleReload ? '再度抽選する' : '抽選スタート'}
            </Button>
          </InputContainer>
        )}
      </Container>
    </Wrapper>
  )
}
