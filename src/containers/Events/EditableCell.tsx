import React, { memo } from 'react'
import { SwitcherOutlined } from '@ant-design/icons'

interface EditableCellProps {
  title: React.ReactNode
  editable: boolean
  children: React.ReactNode
  dataIndex: any
  record: any
  setEventId: any
  setIsVisible: any
}

const EditableCell: React.FC<EditableCellProps> = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  setEventId,
  setIsVisible,
  ...restProps
}) => {
  let childNode = children

  if (editable) {
    childNode = (
      <SwitcherOutlined
        title='賞品確認'
        style={{ fontSize: 20 }}
        onClick={() => {
          setIsVisible(true)
          setEventId(record.id)
        }}
      />
    )
  }

  return <td {...restProps}>{childNode}</td>
}
export default memo(EditableCell)
