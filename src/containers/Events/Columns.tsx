/* eslint-disable */
import React from "react";
import {
  colTitleRender,
  renderCreateDate,
  renderRowCenter,
  renderRowNumber,
  renderRowText,
  renderRowLink,
  renderRowImage,
} from "../../components/TableCell/TableCellRender";
import { Tooltip } from "antd";

export const PrizeColumns: any = [
  {
    title: colTitleRender("#"),
    dataIndex: "idx",
    width: 45,
    ellipsis: true,
    align: "center",
    render: function Render(text) {
      return <div style={{ cursor: "pointer" }}>{text}</div>;
    },
  },
  {
    title: colTitleRender("賞品名"),
    dataIndex: "name",
    width: "25%",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowText(value),
  },

  {
    title: colTitleRender("ランク"),
    dataIndex: "rank",
    align: "center",
    width: "17%",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowNumber(value),
  },
  {
    title: colTitleRender("割合"),
    dataIndex: "rate",
    width: "15%",
    align: "center",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowCenter(`${value}%`),
  },
  {
    title: colTitleRender("数量"),
    dataIndex: "quantity",
    width: "15%",
    align: "center",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowNumber(value),
  },
  {
    title: colTitleRender("作成日"),
    dataIndex: "createdAt",
    width: "25%",
    ellipsis: {
      showTitle: false,
    },
    align: "center",
    render: (value: any) => renderCreateDate(value),
  },
  {
    title: colTitleRender("画像"),
    dataIndex: "imageUrl",
    width: "15%",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowImage(value),
  },
];

export const FullPrizeColumns: any = [
  {
    title: colTitleRender("#"),
    dataIndex: "idx",
    width: 60,
    ellipsis: true,
    align: "center",
    render: function Render(text) {
      return <div style={{ cursor: "pointer" }}>{text}</div>;
    },
  },
  {
    title: colTitleRender("賞品名"),
    dataIndex: "name",
    width: "20%",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowText(value),
  },
  {
    title: colTitleRender("ランク"),
    dataIndex: "rank",
    align: "center",
    width: "10%",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowNumber(value),
  },
  {
    title: colTitleRender("割合"),
    dataIndex: "rate",
    width: "10%",
    align: "center",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowCenter(`${value}%`),
  },
  {
    title: colTitleRender("数量"),
    dataIndex: "quantity",
    width: "10%",
    align: "center",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowNumber(value),
  },
  {
    title: colTitleRender("作成日"),
    dataIndex: "createdAt",
    width: "15%",
    ellipsis: {
      showTitle: false,
    },
    align: "center",
    render: (value: any) => renderCreateDate(value),
  },
  {
    title: colTitleRender("画像URL"),
    dataIndex: "imageUrl",
    width: "20%",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowLink(value),
  },
  {
    title: colTitleRender("動画名"),
    dataIndex: "videoName",
    width: "15%",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowText(value),
  },
];

// export const SelectOptions = [
//   { value: 0, label: '賞品名' },
//   { value: 1, label: '作成日' },
//   { value: 2, label: 'ランク' },
//   { value: 3, label: '割合' }
// ]

export const EventColumns: any = [
  {
    title: colTitleRender("項番"),
    dataIndex: "idx",
    width: 60,
    ellipsis: true,
    align: "center",
    render: function Render(text: string) {
      return <div style={{ cursor: "pointer" }}>{text}</div>;
    },
  },
  {
    title: colTitleRender("イベントリンク"),
    dataIndex: "link",
    width: "20%",
    ellipsis: true,
    render: function Render(text) {
      let link = `${window.location.origin}${text}`;
      return (
        <Tooltip placement="topLeft" title={link}>
          <a
            href={link}
            target="_blank"
            style={{ cursor: "pointer", textAlign: "left" }}
          >
            {link}
          </a>
        </Tooltip>
      );
    },
  },
  {
    title: colTitleRender("確認"),
    dataIndex: "id",
    width: "10%",
    ellipsis: {
      showTitle: false,
    },
    align: "center",
    editable: true,
    // render: (text: string) => renderButtonConfirm(text)
  },
  {
    title: colTitleRender("イベント名"),
    dataIndex: "name",
    width: "20%",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowText(value),
  },
  {
    title: colTitleRender("抽選回数"),
    dataIndex: "numberOfLotteryPeople",
    width: "10%",
    align: "right",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowNumber(value),
  },
  {
    title: colTitleRender("作成日"),
    dataIndex: "createdAt",
    width: "12%",
    ellipsis: true,
    align: "center",
    render: (value: any) => renderCreateDate(value),
  },
  {
    title: colTitleRender("開始時間"),
    dataIndex: "startTime",
    width: "12%",
    ellipsis: {
      showTitle: false,
    },
    align: "center",
    render: (value: any) => renderCreateDate(value),
  },
  {
    title: colTitleRender("終了時間"),
    dataIndex: "endTime",
    width: "12%",
    ellipsis: {
      showTitle: false,
    },
    align: "center",
    // render: (value: any) => renderCreateDate(parseFloat(value) + 86340000)
    render: (value: any) => renderCreateDate(value),
  },
  {
    title: colTitleRender("所有者"),
    dataIndex: "owner",
    width: "15%",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowText(value),
  },
  {
    title: colTitleRender("メモ"),
    dataIndex: "memo",
    width: "15%",
    ellipsis: {
      showTitle: false,
    },
    render: (value: any) => renderRowText(value),
  },
];
