import { styled } from 'baseui'
import Button from '../../components/Button/Button'
import { Col as Column } from '../../components/FlexBox/FlexBox'
import { Table } from 'antd'
import { Link } from 'react-router-dom'
export const Header = styled('header', () => ({
  display: 'flex',
  alignItems: 'center',
  flex: '0 1 auto',
  flexDirection: 'row',
  flexWrap: 'wrap',
  backgroundColor: '#FFF',
  padding: '0 1rem',
  marginBottom: '1.5rem',
  minHeight: '70px',
  boxShadow: '0 0 8px rgba(0, 0 ,0, 0.1)',

  '@media only screen and (max-width: 1024px)': {
    padding: '0 0.5rem'
  },

  '@media only screen and (max-width: 990px)': {
    marginBottom: '1rem'
  },

  '@media only screen and (max-width: 767px)': {
    marginBottom: '0.5rem'
  }
}))

export const TableWrapper = styled('div', () => ({
  paddingBottom: '15px'
}))

export const TableCustom = styled(Table, () => ({
  overflow: 'auto',
  background: 'white',
  border: '1px solid #ddd',
  height: 'auto'
}))

export const Heading = styled(Link, ({ $theme }) => ({
  ...$theme.typography.fontBold18,
  color: $theme.colors.textDark,
  marginLeft: '16px',

  '@media only screen and (max-width: 768px)': {
    marginLeft: '0'
  }
}))

export const ActionWrapper = styled('div', () => ({
  display: 'flex',
  alignItems: 'center',

  span: {
    cursor: 'pointer',
    marginLeft: '1rem'
  },

  'span:hover': {
    color: '#40a9ff'
  }
}))

export const ButtonCustom = styled(Button, () => ({
  paddingTop: '0.4rem !important',
  paddingBottom: '0.4rem !important',
  paddingLeft: '0.8rem !important',
  paddingRight: '0.8rem !important',
  // marginLeft: '7px !important',
  marginTop: '0.7rem !important',
  borderRadius: '10px !important',
  boxShadow: 'inset -2px -2px 0 rgba(0, 0, 0, 0.15) !important',
  width: '5rem !important',
  height: '2.4rem !important',
  marginRight: '1rem !important'
}))

export const PrizeConfirmWrapper = styled('div', () => ({
  maxHeight: '30rem',
  overflowY: 'auto'
}))

export const PrizeItem = styled('div', () => ({
  display: 'flex',
  overflowY: 'hiden',
  justifyContent: 'space-between',
  alignItems: 'center',
  padding: '1rem',
  marginBottom: '0.5rem',
  marginTop: '0.5rem',
  // border: '1px solid #ddd',
  backgroundColor: '#f6f7f8',
  borderRadius: '10px'
  // boxShadow: '5px 5px 6px rgba(0,0,0,0.2)'
}))
export const PrizeName = styled('div', () => ({
  fontSize: '1rem',
  fontWeight: '700',
  width: '100%',
  display: 'flex'
}))

export const ButtonConfirm = styled(Button, ({ disabled }) => ({
  border: disabled ? '1px solid #ddd' : 0,
  width: '150px',
  borderRadius: '10px !important',
  boxShadow: 'inset -2px -2px 0 rgba(0, 0, 0, 0.15)'
  // width: '6rem !important',
  // height: '2.5rem !important'
}))

export const NameCol = styled(Column, () => ({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',

  '@media only screen and (max-width: 767px)': {
    marginBottom: '10px',

    ':last-child': {
      marginBottom: 0
    }
  },
  height: '100%'
}))
export const ActionCol = styled(Column, () => ({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-end',
  alignItems: 'center',

  '@media only screen and (max-width: 767px)': {
    marginBottom: '10px',

    ':last-child': {
      marginBottom: 0
    }
  },

  height: '100%'
}))
export const SearchWrapper = styled('div', () => ({
  width: '40%',
  marginRight: '1rem',

  '@media only screen and (max-width: 1024px)': {
    marginRight: '0.5rem'
  }
}))

export const SelectWrapper = styled('div', () => ({
  width: '30%',
  marginRight: '1rem',

  ':last-child': {
    marginRight: 0
  },

  '@media only screen and (max-width: 1024px)': {
    marginRight: '0.5rem'
  }
}))

export const QRExportWrapper = styled('a', () => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
}))
