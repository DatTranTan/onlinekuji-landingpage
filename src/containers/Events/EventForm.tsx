/* eslint-disable */
import { useLazyQuery, useMutation, useQuery } from "@apollo/react-hooks";
import { Checkbox, Row, Col } from "antd";
import { useStyletron } from "baseui";
import ja from "date-fns/locale/ja";
import dayjs from "dayjs";
import $ from "jquery";
// import { Scrollbars } from 'react-custom-scrollbars'
import * as qs from "query-string";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useHistory, useLocation } from "react-router-dom";
import { KIND } from "../../components/Button/Button";
// import { Datepicker } from 'baseui/datepicker'
import { DatePicker } from "../../components/DatePicker/DatePicker";
import {
  Error,
  FormFields,
  FormLabel,
} from "../../components/FormFields/FormFields";
import { QuillEditorWrapper } from "./ConfigQuillEditor.style";
import Input from "../../components/Input/Input";
import { Notification } from "../../components/Notification/NotificationCustom";
import Select from "../../components/Select/Select";
import Textarea from "../../components/Textarea/TextareaCustom";
import BannerUploader from "../../components/Uploader/BannerUploader";

import { QUERY_CAMPAIGNS, QUERY_CAMPAIGN } from "../../graphql/query/campaigns";
import { MUTATION_CREATE_CAMP } from "../../graphql/mutation/create-camp";
import { MUTATION_CREATE_EVENT } from "../../graphql/mutation/create-event";
import { MUTATION_UPDATE_EVENT } from "../../graphql/mutation/update-event";
import { QUERY_EVENT } from "../../graphql/query/event";
import { QUERY_QUESTIONNAIRE } from "../../graphql/query/questionnaire";
import { QUERY_TEMPLATES } from "../../graphql/query/template";
import { QUERY_VIDEO } from "../../graphql/query/video";
import { EVENT } from "../../settings/constants";
import addPhoto from "../../utils/S3/upload";
import { OverideSelect } from "../UserForm/Overrides";
import ConfirmPrize from "./ConfirmPrize";
import {
  ButtonCustom,
  ButtonGroup,
  ButtonWrapper,
  Column,
  Container,
  DateField,
  DateFieldsWrapper,
  EventFormWrapper,
  Form,
  Header,
  Heading2,
  InfoWrapper,
  PrizeWrapper,
  RequiredWrapper,
  Wrapper,
  GroupFieldWrapper,
} from "./EventForm.style";
import { Heading } from "./Events.style";
import { EventFormInput, PrizeInput } from "./Interface";
import { OverideCancel, OverideSave } from "./Override";
import PrizeForm from "./PrizeForm";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import QuillToolbar, {
  modules,
  formats,
} from "../../components/ToolbarQuill/ToolbarQuill";
import { CustomQuill } from "../../components/ToolbarQuill/CustomQuill";
import { isIE } from "react-device-detect";

var myToday = dayjs().toDate();

export default function EventForm() {
  const history = useHistory();
  const location = useLocation();
  const [css] = useStyletron();
  let search = qs.parse(location.search);
  const isCreate = search.type === "create";
  const isEdit = search.type === "edit";
  const userRef = React.useRef<any>(null);
  const memoRef = React.useRef<any>(null);
  const selectVideoRef = React.useRef<HTMLInputElement | HTMLDivElement>(null);
  const selectCampaignRef = React.useRef<HTMLInputElement | HTMLDivElement>(
    null
  );
  const [dateStart, setDateStart] = useState<any>(myToday);
  const [dateEnd, setDateEnd] = useState<any>(myToday);
  const [prizes, setPrizes] = useState<[PrizeInput]>(null);
  const [videoLink, setVideoLink] = React.useState<any>(null);
  const [noPrizeVideoLink, setNoPrizeVideoLink] = useState<any[]>(null);
  const [question, setQuestion] = useState<any[]>(null);
  const [template, setTemplate] = useState<any[]>(null);
  // const [videoFailed, setVideoFailed] = useState<Value>(null)
  const [urlImages, setUrlImages] = useState(null);
  const [urlVideoBackground, setUrlVideoBackground] = useState(null);
  const [footerUrl1, setFooterUrl1] = useState(null);
  const [footerUrl2, setFooterUrl2] = useState(null);
  const [skip, setSkip] = useState("");
  const [searchTemplate, setSearchTemplate] = useState("");

  const [memoText, setMemoText] = useState("");
  const [searchText, setSearchText] = useState("");
  const [timeOut, setTimeOut] = useState(null);
  const [searchNoPrizeVideo, setSearchNoPrizeVideo] = useState("");
  const [searchQuestion, setSearchQuestion] = useState("");
  const [isUploading, setIsUploading] = useState(false);
  const [isUploading2, setIsUploading2] = useState(false);
  const [isUploading3, setIsUploading3] = useState(false);
  const [isUploading4, setIsUploading4] = useState(false);
  const UniqueID = Date.now() + "_" + Math.random().toString(36).substr(2, 34);
  const [albumName, setAlbumName] = useState(UniqueID);
  const [loadData, setLoadData] = useState({ loaded: 0, total: 0, unit: "" });
  const [loadData2, setLoadData2] = useState({ loaded: 0, total: 0, unit: "" });
  const [loadData3, setLoadData3] = useState({ loaded: 0, total: 0, unit: "" });
  const [loadData4, setLoadData4] = useState({ loaded: 0, total: 0, unit: "" });
  const [dataCamp, setDataCamp] = useState(null);
  //campaign state
  const [selectOption, setSelectOption] = useState<any>(null);
  const [dataEditor, setDataEditor] = useState<any>(null);
  const [fileName, setFileName] = useState<any>(null);
  const [searchCampaign, setSearchCampaign] = useState<any>(null);
  const [customerLostState, setCustomerLostState] = useState(false);

  const {
    register,
    handleSubmit,
    setValue,
    setError,
    errors,
    control,
    getValues,
  } = useForm<EventFormInput>({
    defaultValues: isCreate && {
      videoid: "",
      // videoLoseId: '',
      bannerUrl: "",
      videoBackground: "",
      footerUrl1: null,
      footerUrl2: null,
      startdate: Date.parse(myToday.toDateString()),
      enddate: Date.parse(myToday.toString()),
      checkDateStartandEnd: true,
      // footerBannerUrl: [],
      topBannerUrl: "",
      leftFooterBannerUrl: "",
      rightFooterBannerUrl: "",
      noPrizeVideoId: "",
      questionnaireId: "",
      customerLost: "",
      timeOut: null,
      campaignId: "",
    },
  });
  const callback1 = {
    onSuccess: (urls: any) => {
      setUrlImages(urls);
      setValue("bannerUrl", urls);
      setIsUploading(false);
    },
    onError: (urls: any) => {
      setIsUploading(false);
    },
    onLoad: (loaded: number, total: number, unit: string) => {
      setLoadData({ loaded: loaded, total: total, unit: unit });
    },
  };
  const callback2 = {
    onSuccess: (urls: any) => {
      setFooterUrl1(urls);
      setValue("footerUrl1", urls);
      setIsUploading2(false);
    },
    onError: (urls: any) => {
      setIsUploading2(false);
    },
    onLoad: (loaded: number, total: number, unit: string) => {
      setLoadData2({ loaded: loaded, total: total, unit: unit });
    },
  };
  const callback3 = {
    onSuccess: (urls: any) => {
      setFooterUrl2(urls);
      setValue("footerUrl2", urls);
      setIsUploading3(false);
    },
    onError: (urls: any) => {
      setIsUploading3(false);
    },
    onLoad: (loaded: number, total: number, unit: string) => {
      setLoadData3({ loaded: loaded, total: total, unit: unit });
    },
  };
  const callback4 = {
    onSuccess: (urls: any) => {
      setUrlVideoBackground(urls);
      setValue("videoBackground", urls);
      setIsUploading4(false);
    },
    onError: (urls: any) => {
      setIsUploading4(false);
    },
    onLoad: (loaded: number, total: number, unit: string) => {
      setLoadData4({ loaded: loaded, total: total, unit: unit });
    },
  };
  // const handleUploaderBanner = (files: any) => {
  //   setIsUploading(true)
  //   addPhoto(albumName, 'vermuda-images/', files, callback1)
  // }

  const handleUploader = (files: any, type: any) => {
    if (type === "banner") {
      setIsUploading(true);
      addPhoto(albumName, "vermuda-images/", files, callback1);
    }
    if (type === "footer1") {
      setIsUploading2(true);
      addPhoto(albumName, "vermuda-images/", files, callback2);
    }
    if (type === "footer2") {
      setIsUploading3(true);
      addPhoto(albumName, "vermuda-images/", files, callback3);
    }
    if (type === "videoBackground") {
      setIsUploading4(true);
      addPhoto(albumName, "vermuda-images/", files, callback4);
    }
  };

  // const handleUploaderFooter = (files: any, item: any) => {
  //   setIsUploading(true)
  //   if (item === 'footer1')
  //     addPhoto(albumName, 'vermuda-images/', files, callbackFooter)
  //   else if (item === 'footer2')
  //     addPhoto(albumName, 'vermuda-images/', files, callbackFooter1)
  // }

  const { data: questionnaire, loading: loadingQuestion } = useQuery(
    QUERY_QUESTIONNAIRE,
    {
      variables: {
        name: searchQuestion,
        offset: 0,
        limit: 0,
      },
      fetchPolicy: "network-only",
    }
  );
  const {
    data: dataCampaigns,
    loading: loadingCampaign,
    refetch: refetchCampaigns,
  } = useQuery(QUERY_CAMPAIGNS, {
    variables: {
      name: searchCampaign,
      offset: 0,
      limit: 0,
    },
    fetchPolicy: "no-cache",
  });
  const listCampaigns = dataCampaigns?.Campaigns?.campaign;
  const {
    data: dataDetail,
    loading: loadingDataDetail,
    refetch: refetchCampaignDetail,
  } = useQuery(QUERY_CAMPAIGN, {
    variables: {
      Id: "",
    },
    fetchPolicy: "no-cache",
  });

  const { data: templates, loading: loadingTemplates } = useQuery(
    QUERY_TEMPLATES,
    {
      variables: {
        name: "",
        offset: 0,
        limit: 0,
      },
      fetchPolicy: "network-only",
    }
  );

  const [createCamp, { loading: loadingCreateCampain }] = useMutation(
    MUTATION_CREATE_CAMP,
    {
      fetchPolicy: "no-cache",
    }
  );

  const listQuestion = questionnaire?.getAllQuestionnaire.questionnaires;
  const listTemplate = templates?.Templates?.templates;

  const [createEvent, { loading: createLoading }] = useMutation(
    MUTATION_CREATE_EVENT,
    {
      fetchPolicy: "no-cache",
    }
  );

  const [updateEvent, { loading: editLoading }] = useMutation(
    MUTATION_UPDATE_EVENT,
    {
      fetchPolicy: "no-cache",
    }
  );

  // const { data: eventdata } = useQuery(QUERY_EVENT, {
  //   variables: {
  //     id: search.eventId,
  //   },
  // });

  const [queryEventInfo, { data: eventdata }] = useLazyQuery(QUERY_EVENT, {
    fetchPolicy: "network-only",
    onCompleted: (data) => {
      let {
        name,
        startTime,
        endTime,
        video,
        noPrizeVideo,
        memo,
        banner,
        prizes,
        footerBanner,
        bannerUrl,
        videoBackground,
        footerBannerUrl,
        questionnaire,
        customerLost,
        timeOut,
        template,
        campaign,
      } = data?.event ?? {};
      setPrizes(
        prizes?.map((item: any) => ({
          id: item.id,
          name: item.name,
          rank: parseFloat(item.rank),
          rate: parseFloat(item.rate),
          quantity: parseFloat(item.quantity),
          message: item.message,
          imageUrl: item.imageUrl,
          createdAt:
            item.createdAt === item.updatedAt ? item.createdAt : item.updatedAt,
        }))
      );
      let startTimeParse = parseFloat(startTime);
      let endTimeParse = parseFloat(endTime);
      setValue("name", name);
      setValue("startdate", startTimeParse);
      setValue("enddate", endTimeParse);
      setDateStart(dayjs(startTimeParse).toDate());
      // setDateEnd(dayjs(endTimeParse).toDate())
      setDateEnd(dayjs(endTimeParse).toDate());
      setValue("videoid", video?.id);
      setVideoLink(video ? [video] : []);
      setValue("noPrizeVideoId", noPrizeVideo?.id);
      setNoPrizeVideoLink(noPrizeVideo ? [noPrizeVideo] : []);
      setValue("questionnaireId", questionnaire?.id);
      setQuestion(questionnaire ? [questionnaire] : []);
      setTemplate(template ? [template] : []);
      setValue("bannerUrl", banner);
      setValue("videoBackground", videoBackground);
      setValue("timeOut", timeOut);
      setTimeOut(timeOut);
      setUrlImages(banner);
      setUrlVideoBackground(videoBackground);
      setValue(
        "footerUrl1",
        data?.event.footerBanner !== null && footerBanner?.[0]
      );
      setFooterUrl1(data?.event.footerBanner !== null && footerBanner?.[0]);
      setFooterUrl2(data?.event.footerBanner !== null && footerBanner?.[1]);
      setValue(
        "footerUrl2",
        data?.event.footerBanner !== null && footerBanner?.[1]
      );
      setValue("topBannerUrl", bannerUrl);
      setValue(
        "leftFooterBannerUrl",
        data?.event.footerBannerUrl === null
          ? footerBannerUrl
          : footerBannerUrl[0]
      );
      setValue(
        "rightFooterBannerUrl",
        data?.event.footerBannerUrl === null
          ? footerBannerUrl
          : footerBannerUrl[1]
      );
      setValue("memo", memo);
      setMemoText(memo);
      setValue("customerLost", customerLost === "Win" ? true : false);
      setCustomerLostState(customerLost === "Win" ? true : false);
      setValue("campaignId", campaign?.id ? campaign?.id : null);
      setDataCamp(campaign);
    },
  });
  
  useEffect(() => {
    setSelectOption(dataCamp ? [dataCamp] : []);
    setDataEditor(dataCamp?.content);
  }, [dataCamp]);

  useEffect(() => {
    let data = dataDetail?.Campaign.content;
    if (data) {
      setDataEditor(data);
    }
  }, [dataDetail]);

  const { data, loading } = useQuery(QUERY_VIDEO, {
    variables: {
      name: searchText,
      offset: 0,
      limit: 0,
    },
    fetchPolicy: "no-cache",
  });

  $("#button-submit").click(function (e) {
    setSkip("submit");
  });
  $("#button-childe").click(function (e) {
    setSkip("");
  });

  const handleCreateCamp = () => {
    if (!fileName || fileName.length === 0) return;
    createCamp({
      variables: {
        createCampaignInput: {
          name: fileName,
          content: dataEditor,
        },
      },
    })
      .then((res) => {
        setSelectOption(
          res?.data?.createCampaign ? [res?.data?.createCampaign] : []
        );
        setValue("campaignId", res?.data?.createCampaign.id);
        setDataCamp(res?.data?.createCampaign);
        refetchCampaigns();
        refetchCampaignDetail({
          Id: res?.data?.createCampaign.id,
        });
        let El: any = document.getElementById("inputFileName");
        El.value = null;
        setFileName(null);
        Notification({
          type: "success",
          message: "成功",
          description: "概要テンプレート保存に成功しました。",
        });
      })
      .catch(({ graphQLErrors }) => {
        Notification({
          type: "error",
          message: " エラー",
          description: graphQLErrors[0]?.message,
        });
      });
  };

  const handleCheck = async (data: EventFormInput) => {
    if (skip === "submit") {
      onSubmit(data);
    }
  };

  const ChangeContent = (e: any) => {
    setDataEditor(e);
  };

  const handleFileName = (e: any) => {
    setFileName(e.target.value);
  };

  const onSubmit = (data: EventFormInput) => {
    const prizeAdd = prizes.map((item, index) => ({
      id: item.id,
      name: item.name,
      rank: item.rank,
      rate: item.rate,
      quantity: item.quantity,
      message: item.message,
      imageUrl: item.imageUrl ?? "",
    }));

    const values = {
      id: search.eventId,
      name: data.name,
      memo: data.memo,
      startTime: data.startdate.toString(),
      // endTime: data.enddate.toString(),
      endTime: data.enddate.toString(),
      videoId: data.videoid,
      noPrizeVideoId: data.noPrizeVideoId,
      questionnaireId: data.questionnaireId ?? null,
      banner: data.bannerUrl,
      videoBackground: data.videoBackground,
      footerBanner: [
        data.footerUrl1 ||
          "https://vermuda-images.s3-ap-northeast-1.amazonaws.com/vermuda-images/1641367747673_gpbq4sbblb/download.jpg",
        data.footerUrl2 ||
          "https://vermuda-images.s3-ap-northeast-1.amazonaws.com/vermuda-images/1641367747673_gpbq4sbblb/download.jpg",
      ],
      prizes: prizeAdd,
      bannerUrl: data.topBannerUrl,
      // bannerUrl: test(),
      footerBannerUrl: [data.leftFooterBannerUrl, data.rightFooterBannerUrl],
      // customerLost: data.customerLost ? "Win" : "Lost",
      customerLost: customerLostState ? "Win" : "Lost",
      timeOut: Number(data.timeOut) > 0 ? Number(data.timeOut) : null,
      templateId: template?.[0]?.id || null,
      campaignId: dataCamp?.id ? dataCamp?.id : null,
    };

    // var jun = moment(data.enddate)
    // console.log(
    //   Date.parse(jun.tz('America/Los_Angeles').format()),
    //   'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh'
    // )

    // console.log(values.startTime, 'checkData')
    // console.log('data add', data.enddate)

    let checkStart = dayjs(data.startdate).isValid();
    let checkEnd = dayjs(data.enddate).isValid();

    let checkDateStartandEndBoolean = true;
    if (!checkStart) setError("startdate", "required");
    if (!checkEnd) setError("enddate", "required");
    // if (Date.parse(data.startdate) > Date.parse(data.enddate)) {
    //   checkDateStartandEnd = false
    // }
    if (data.startdate > data.enddate) {
      setError("checkDateStartandEnd", "change");
      checkDateStartandEndBoolean = false;
    }
    if (isCreate && checkStart && checkEnd && checkDateStartandEndBoolean) {
      createEvent({
        variables: {
          createEventInput: {
            ...values,
          },
        },
      })
        .then(() => {
          Notification({
            type: "success",
            message: "成功",
            description: " イベント追加に成功しました。",
          });
          history.push(EVENT);
        })
        .catch(({ graphQLErrors }) =>
          Notification({
            type: "error",
            message: " エラー",
            description: graphQLErrors[0]?.message,
          })
        );
    }
    if (isEdit && checkStart && checkEnd && checkDateStartandEndBoolean) {
      updateEvent({
        variables: {
          updateEventInput: {
            ...values,
            numberOfLotteryPeople: eventdata?.event?.numberOfLotteryPeople,
          },
        },
      })
        .then(() => {
          Notification({
            type: "success",
            message: "成功",
            description: "イベントの更新に成功しました。",
          });
          history.push(EVENT);
        })
        .catch(({ graphQLErrors }) =>
          Notification({
            type: "error",
            message: " エラー",
            description: graphQLErrors[0]?.message,
          })
        );
    }
  };

  useEffect(() => {
    if (localStorage.getItem("role") === "agency") {
      history.push("/event");
    }
  }, []);
  useEffect(() => {
    if (isEdit && search.eventId) {
      queryEventInfo({
        variables: {
          id: search.eventId,
        },
      });
    }
  }, [isEdit]);

  useEffect(() => {
    if (userRef.current) userRef.current.focus();
  }, [userRef]);

  useEffect(() => {
    register({ name: "name" }, { required: true, maxLength: 100 });
    register({ name: "startdate" }, { required: true });
    register({ name: "enddate" }, { required: true });
    register({ name: "videoid" }, { required: true });
    register({ name: "noPrizeVideoId" });
    register({ name: "footerUrl1" });
    register({ name: "footerUrl2" });
    register({ name: "bannerUrl" }, { required: true });
    register({ name: "videoBackground" }, { required: true });
    register({ name: "topBannerUrl" }, { maxLength: 500 });
    register({ name: "memo" }, { maxLength: 200 });

    register({ name: "leftFooterBannerUrl" }, { maxLength: 500 });
    register({ name: "rightFooterBannerUrl" }, { maxLength: 500 });
    register({ name: "questionnaireId" });
    register({ name: "campaignId" }, { required: true });
    // register({ name: 'timeOut' })
  }, [register]);

  const debounce = (fn: any, delay: number) => {
    return (args: any) => {
      clearTimeout(fn.id);

      fn.id = setTimeout(() => {
        fn.call(this, args);
      }, delay);
    };
  };
  const searchHandler = (search: any) => {
    // setSearchText(value)
    if (search?.input === 0) {
      setSearchText(search?.value);
    }
    if (search?.input === 1) {
      setSearchNoPrizeVideo(search?.value);
    }
    if (search?.input === 2) {
      setSearchQuestion(search?.value);
    }
    if (search?.input === 3) {
      setSearchCampaign(search?.value);
    }
  };

  const debounceAjax = debounce(searchHandler, 600);
  return (
    <Container>
      <div>
        <Header>
          <Column lg={6} md={12}>
            <Heading to="/event">イベント管理 /&nbsp;</Heading>
            <Heading2>
              {isCreate ? "イベントの作成" : "イベントの更新"}
            </Heading2>
          </Column>
        </Header>
      </div>
      <div>
        <Wrapper>
          <RequiredWrapper>
            必要な情報を記入して下さい。
            <span style={{ color: "red" }}>(*)は必須の項目です。</span>
          </RequiredWrapper>
          <EventFormWrapper>
            <Form id="parent-form" onSubmit={handleSubmit(handleCheck)}>
              <InfoWrapper>
                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>
                      イベント名&nbsp;<span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Input
                      name="name"
                      inputRef={(e: any) => {
                        register(e, { required: true, maxLength: 100 });
                        userRef.current = e; // you can still assign to ref
                      }}
                    />
                    {errors.name && errors.name.type === "required" && (
                      <Error>(*)は必須の項目です。</Error>
                    )}
                    {errors.name && errors.name.type === "maxLength" && (
                      <Error>イベント名はは100文字以下設定してください。</Error>
                    )}
                  </FormFields>
                  <DateFieldsWrapper>
                    <DateField>
                      <FormFields>
                        <FormLabel>
                          開始日&nbsp;<span style={{ color: "red" }}>*</span>
                        </FormLabel>
                        <DatePicker
                          locale={ja}
                          value={dateStart}
                          formatString="yyyy.MM.dd"
                          placeholder="YYYY.MM.DD"
                          minDate={new Date()}
                          onChange={({ date }) => {
                            const dateParse = Array.isArray(date)
                              ? date[0]
                              : date;
                            if (dayjs(dateParse).isValid()) {
                              setValue(
                                "startdate",
                                Date.parse(dateParse.toDateString())
                              );
                              setDateStart(date);
                            }
                            if (errors.checkDateStartandEnd?.type) {
                              errors.checkDateStartandEnd.type = "changenew";
                            }
                          }}
                        />
                        {errors.startdate &&
                          errors.startdate.type === "required" && (
                            <Error>(*)は必須の項目です。</Error>
                          )}
                        {errors.checkDateStartandEnd &&
                          errors.checkDateStartandEnd.type === "change" && (
                            <Error>
                              開始日は終了日より前の日付にしてください。
                            </Error>
                          )}
                      </FormFields>
                    </DateField>
                    <DateField>
                      <FormFields>
                        <FormLabel>
                          終了日&nbsp;<span style={{ color: "red" }}>*</span>
                        </FormLabel>
                        <DatePicker
                          locale={ja}
                          value={dateEnd}
                          formatString="yyyy.MM.dd"
                          placeholder="YYYY.MM.DD"
                          minDate={isEdit ? new Date(dateStart) : new Date()}
                          onChange={({ date }) => {
                            const dateParse = Array.isArray(date)
                              ? date[0]
                              : date;
                            if (dayjs(dateParse).isValid()) {
                              setValue(
                                "enddate",
                                // Date.parse(dateParse.toDateString())
                                Date.parse(dateParse)
                                // 1616750684000
                              );
                              setDateEnd(date);
                              // setDateEnd(1616750684000)
                            }
                            if (errors.checkDateStartandEnd?.type) {
                              errors.checkDateStartandEnd.type = "changenew";
                            }
                          }}
                        />
                        {errors.enddate &&
                          errors.enddate.type === "required" && (
                            <Error>(*)は必須の項目です。</Error>
                          )}{" "}
                        {errors.checkDateStartandEnd &&
                          errors.checkDateStartandEnd.type === "change" && (
                            <Error>
                              終了日は開始日より後の日付にしてください。
                            </Error>
                          )}
                      </FormFields>
                    </DateField>
                  </DateFieldsWrapper>
                </GroupFieldWrapper>
                {/* Win price video */}
                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>
                      当選時演出動画&nbsp;
                      <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <Select
                      labelKey="name"
                      valueKey="id"
                      value={videoLink}
                      placeholder={
                        videoLink?.length > 0 ? "" : "当選時演出動画を選択する"
                      }
                      options={!loading && data?.Videos.videos}
                      onInputChange={(event: any) =>
                        debounceAjax({ search: event.target.value, input: 0 })
                      }
                      onChange={({ value }) => {
                        setValue("videoid", value[0]?.id);
                        setVideoLink(value);
                      }}
                      controlRef={(e: any) => {
                        register(e, { required: true });
                        selectVideoRef.current = e;
                      }}
                      overrides={OverideSelect}
                    />
                    {errors.videoid && errors.videoid.type === "required" && (
                      <Error>(*)は必須の項目です。</Error>
                    )}
                  </FormFields>

                  {/* Win price video */}

                  {/* No Price video */}
                  <FormFields>
                    <FormLabel>
                      ハズレ時演出動画 &nbsp;
                      {/* <span style={{ color: 'red' }}>*</span> */}
                    </FormLabel>
                    <Select
                      labelKey="name"
                      valueKey="id"
                      value={noPrizeVideoLink}
                      placeholder={
                        noPrizeVideoLink?.length > 0
                          ? ""
                          : "ハズレ時演出動画を選択する"
                      }
                      options={!loading && data?.Videos.videos}
                      onInputChange={(event: any) =>
                        debounceAjax({ search: event.target.value, input: 1 })
                      }
                      onChange={({ value }) => {
                        setValue(
                          "noPrizeVideoId",
                          value[0]?.id === undefined ? null : value[0]?.id
                        );
                        setNoPrizeVideoLink(value);
                      }}
                      controlRef={(e: any) => {
                        register(e);
                        selectVideoRef.current = e;
                      }}
                      overrides={OverideSelect}
                    />
                    {errors.noPrizeVideoId &&
                      errors.noPrizeVideoId.type === "required" && (
                        <Error>(*)は必須の項目です。</Error>
                      )}
                  </FormFields>
                  <FormFields>
                    <FormLabel>
                      背景画像&nbsp;
                      <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <BannerUploader
                      disabled={isUploading4}
                      onChange={(files: any) =>
                        handleUploader(files, "videoBackground")
                      }
                      onDelete={() => {
                        setUrlVideoBackground(null);
                        setValue("videoBackground", null);
                      }}
                      imageURL={urlVideoBackground}
                      progress={loadData4}
                      isEdit={isEdit}
                    />
                    {errors.videoBackground &&
                      errors.videoBackground.type === "required" && (
                        <Error>(*)は必須の項目です。</Error>
                      )}
                  </FormFields>
                </GroupFieldWrapper>
                {/*End No Price video */}

                {/* questionnaire */}
                <GroupFieldWrapper>
                  <FormFields>
                    <div style={{ marginBottom: "10px" }}>
                      <span style={{ marginRight: "1rem" }}>
                        <FormLabel>アンケート</FormLabel>
                      </span>
                      {/* <Controller
                      as={
                        <Checkbox>
                          <FormLabel>参加賞機能</FormLabel>{" "}
                        </Checkbox>
                      }
                      name="customerLost"
                      control={control}
                      onChange={([e]) => {
                        return e.target.checked;
                      }}
                    /> */}
                    </div>

                    <Select
                      labelKey="question"
                      valueKey="id"
                      value={question}
                      placeholder={
                        question?.length > 0 ? "" : "アンケートを選択する"
                      }
                      options={!loadingQuestion && listQuestion}
                      onInputChange={(event: any) =>
                        debounceAjax({ value: event.target.value, input: 2 })
                      }
                      onChange={({ value }) => {
                        setValue("questionnaireId", value[0]?.id);
                        setQuestion(value);
                      }}
                      controlRef={(e: any) => {
                        register(e);
                        selectVideoRef.current = e;
                      }}
                      overrides={OverideSelect}
                    />
                  </FormFields>
                </GroupFieldWrapper>
                {/* end questionnaire */}

                {/* template */}
                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>テンプレート</FormLabel>
                    <Select
                      labelKey="name"
                      valueKey="id"
                      value={template}
                      placeholder={
                        template?.length > 0 ? "" : "テンプレートを選択する"
                      }
                      options={!loadingTemplates && listTemplate}
                      // onInputChange={(event: any) =>
                      //   debounceAjax({ value: event.target.value, input: 2 })
                      // }
                      onChange={({ value }) => {
                        setValue("templateId", value[0]?.id);
                        setTemplate(value);
                      }}
                      // controlRef={(e: any) => {
                      //   register(e);
                      //   selectTemplateRef.current = e;
                      // }}
                      overrides={OverideSelect}
                    />
                  </FormFields>
                </GroupFieldWrapper>
                {/* end template */}
                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>
                      トップーバナー&nbsp;
                      <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <BannerUploader
                      disabled={isUploading}
                      onChange={(files: any) => handleUploader(files, "banner")}
                      onDelete={() => {
                        setUrlImages(null);
                        setValue("bannerUrl", null);
                      }}
                      imageURL={urlImages}
                      progress={loadData}
                      isEdit={isEdit}
                    />
                    {errors.bannerUrl &&
                      errors.bannerUrl.type === "required" && (
                        <Error>(*)は必須の項目です。</Error>
                      )}
                  </FormFields>

                  <FormFields>
                    <FormLabel>URL</FormLabel>
                    <Input
                      name="topBannerUrl"
                      inputRef={register({
                        maxLength: 500,
                      })}
                    />
                    {/* {errors.topBannerUrl &&
                    errors.topBannerUrl.type === 'pattern' && (
                      <Error>URL is Invalid</Error>
                    )} */}
                    {errors.topBannerUrl &&
                      errors.topBannerUrl.type === "maxLength" && (
                        <Error>URLは500文字以下設定してください。</Error>
                      )}
                  </FormFields>

                  {/* <DateFieldsWrapper>
                    <DateField>
                      <FormFields>
                        <FormLabel>
                          フッターバナー１&nbsp;
                          <span style={{ color: "red" }}>*</span>
                        </FormLabel>
                        <BannerUploader
                          disabled={isUploading2}
                          onChange={(files: any) =>
                            handleUploader(files, "footer1")
                          }
                          onDelete={() => {
                            setFooterUrl1(null);
                            setValue("footerUrl1", null);
                          }}
                          imageURL={footerUrl1}
                          progress={loadData2}
                          hidePlaceholder={true}
                          isEdit={isEdit}
                        />
                        {errors.footerUrl1 &&
                          errors.footerUrl1.type === "required" && (
                            <Error>(*)は必須の項目です。</Error>
                          )}
                      </FormFields>
                    </DateField>
                    <DateField>
                      <FormFields>
                        <FormLabel>
                          フッターバナー2&nbsp;
                          <span style={{ color: "red" }}>*</span>
                        </FormLabel>
                        <BannerUploader
                          disabled={isUploading3}
                          onChange={(files: any) =>
                            handleUploader(files, "footer2")
                          }
                          onDelete={() => {
                            setFooterUrl2(null);
                            setValue("footerUrl2", null);
                          }}
                          imageURL={footerUrl2}
                          progress={loadData3}
                          hidePlaceholder={true}
                          isEdit={isEdit}
                        />
                        {errors.footerUrl2 &&
                          errors.footerUrl2.type === "required" && (
                            <Error>(*)は必須の項目です。</Error>
                          )}
                      </FormFields>
                    </DateField>
                  </DateFieldsWrapper> */}

                  {/* <DateFieldsWrapper>
                    <DateField>
                      <FormFields>
                        <FormLabel>
                          URL&nbsp;
                        </FormLabel>
                        <Input
                          name="leftFooterBannerUrl"
                          inputRef={register({
                            maxLength: 500,
                          })}
                        />
                        {errors.leftFooterBannerUrl &&
                          errors.leftFooterBannerUrl.type === "maxLength" && (
                            <Error>URLは500文字以下設定してください。</Error>
                          )}
                      </FormFields>
                    </DateField>
                    <DateField>
                      <FormFields>
                        <FormLabel>
                          URL&nbsp;
                        </FormLabel>
                        <Input
                          name="rightFooterBannerUrl"
                          inputRef={register({
                            maxLength: 500,
                          })}
                        />
                        {errors.rightFooterBannerUrl &&
                          errors.rightFooterBannerUrl.type === "maxLength" && (
                            <Error>URLは500文字以下設定してください。</Error>
                          )}
                      </FormFields>
                    </DateField>
                  </DateFieldsWrapper> */}
                </GroupFieldWrapper>

                <GroupFieldWrapper>
                  <FormFields>
                    <FormLabel>メモ</FormLabel>
                    <Textarea
                      rows={4}
                      inputRef={(e: any) => {
                        register(e, { maxLength: 200 });
                        memoRef.current = e; // you can still assign to ref
                      }}
                      name="memo"
                      clearOnEscape
                      value={memoText}
                      onChange={(e) => {
                        let value = e.target.value;
                        setValue("memo", value);
                        setMemoText(value);
                      }}
                    />
                    {errors.memo && errors.memo.type === "maxLength" && (
                      <Error>メモは200文字以下入力してください。</Error>
                    )}
                  </FormFields>
                </GroupFieldWrapper>

                <ButtonWrapper>
                  <ButtonGroup>
                    <ButtonCustom
                      type="button"
                      kind={KIND.minimal}
                      onClick={() => history.push("/event")}
                      overrides={OverideCancel}
                    >
                      キャンセル
                    </ButtonCustom>
                    <ButtonCustom
                      isLoading={createLoading || editLoading}
                      id="button-submit"
                      type="submit"
                      overrides={OverideSave}
                    >
                      {isCreate ? "追加する" : "更新する"}
                    </ButtonCustom>
                  </ButtonGroup>
                </ButtonWrapper>
              </InfoWrapper>

              <PrizeWrapper>
                <div>
                  <FormLabel>賞品</FormLabel>
                  <PrizeForm
                    prizeList={prizes ?? []}
                    setPrizes={setPrizes}
                    control={control}
                    setCustomerLost={setCustomerLostState}
                    customerLost={customerLostState}
                  />
                </div>
                <GroupFieldWrapper style={{}}>
                  <FormFields>
                    <FormLabel>当選者情報入力制限時間（分）</FormLabel>
                    <Input
                      name="timeOut"
                      type="number"
                      min={1}
                      max={999999}
                      inputRef={register({})}
                    />
                  </FormFields>
                  <>
                    ＊この制限時間は当選時点から当選者情報(住所やアンケート等)を受け付けるまでの制限時間となります。
                  </>
                  <br></br>
                  <>
                    ＊極端に短い時間を入力されますと当選しても情報入力までの時間で制限時間となり、当選者が当選者情報を入力できなくなりますのでご注意ください。
                  </>
                  <br></br>
                  <>
                    ＊未入力の場合はイベント終了後でも当選者情報が入力できる無制限状態となります。
                  </>
                </GroupFieldWrapper>
                <div>
                  <GroupFieldWrapper
                    style={{ marginBottom: isIE ? "80px" : "0px" }}
                  >
                    <FormLabel>
                      概要テンプレート&nbsp;
                      <span style={{ color: "red" }}>*</span>
                    </FormLabel>
                    <FormFields>
                      <Select
                        labelKey="name"
                        valueKey="id"
                        value={selectOption}
                        options={!loadingCampaign && listCampaigns}
                        placeholder={"テンプレートを選択する"}
                        onInputChange={(event: any) =>
                          debounceAjax({ search: event.target.value, input: 3 })
                        }
                        onChange={({ value }) => {
                          setSelectOption(value);
                          setDataCamp(value[0]);
                          setValue("campaignId", value[0]?.id);
                          refetchCampaignDetail({
                            Id: value[0]?.id,
                          });
                        }}
                        overrides={OverideSelect}
                        controlRef={(e: any) => {
                          register(e, { required: true });
                          selectCampaignRef.current = e;
                        }}
                      />
                      {selectOption &&
                        selectOption.length === 0 &&
                        errors.campaignId &&
                        errors.campaignId.type === "required" && (
                          <Error>(*)は必須の項目です。</Error>
                        )}
                    </FormFields>

                    <QuillEditorWrapper>
                      <CustomQuill>
                        <QuillToolbar />
                        <ReactQuill
                          theme="snow"
                          modules={modules}
                          formats={formats}
                          value={dataEditor ? dataEditor : ""}
                          onChange={ChangeContent}
                        />
                      </CustomQuill>
                    </QuillEditorWrapper>

                    <FormFields>
                      <FormLabel>概要テンプレート名&nbsp;</FormLabel>
                      <Row>
                        <Col xs={18}>
                          <Input
                            id="inputFileName"
                            style={{
                              borderColor: "#00bd87",
                              zIndex: 0,
                              background: "#eeeeee",
                            }}
                            maxLength={100}
                            onChange={handleFileName}
                          />
                        </Col>
                        <Col xs={6}>
                          <ButtonCustom
                            isLoading={loadingCreateCampain}
                            onClick={handleCreateCamp}
                            disabled={fileName ? false : true}
                            type="button"
                            style={{
                              width: "100%",
                              height: "45px",
                              backgroundColor: fileName ? "#00bd87" : "gray",
                              color: "white",
                            }}
                          >
                            保存する
                          </ButtonCustom>
                        </Col>
                      </Row>
                    </FormFields>
                  </GroupFieldWrapper>
                </div>
              </PrizeWrapper>
            </Form>
          </EventFormWrapper>
        </Wrapper>
      </div>
      {/* <ConfirmPrize /> */}
    </Container>
  );
}
