import { styled } from 'baseui'

export const BodyWrapper = styled('div', () => ({}))
export const PrizeBlock = styled('div', () => ({
  display: 'flex',
  justifyContent: 'space-between'
}))
export const TitleBlock = styled('div', () => ({
  display: 'flex'
}))
export const TitlePrize = styled('h1', () => ({
  margin: 0
}))
export const Amount = styled('h2', () => ({
  margin: 'auto'
}))
