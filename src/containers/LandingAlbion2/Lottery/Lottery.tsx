import React from 'react'
import { ContainerLottery } from './Lottery.style'
import LuckyIpad from './LuckyIpad/LuckyIpad'

function Lottery({dataOrderPrizes}) {
  return (
    <ContainerLottery>
      <LuckyIpad dataOrderPrizes={dataOrderPrizes}/>
    </ContainerLottery>
  )
}

export default Lottery
