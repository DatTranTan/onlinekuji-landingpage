/* eslint-disable */
import { useQuery } from '@apollo/react-hooks'
import { Checkbox, Modal, Radio, Row, Col } from 'antd'
import queryString from 'query-string'
import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  Error,
  FormFields,
  FormLabel
} from '../../../../../../components/FormFields/FormFields'
// import Input from '../../../components/Input/Input'
import Input from '../../../../../../components/Input/Input'
import { QUERY_EVENT } from '../../../../../../graphql/query/event'
const { confirm } = Modal

type Props = any
const Questionnaire: React.FC<Props> = (props) => {
  const { data, index, setValue, register, errors, Controller, control } = props
  const [content, setContent] = useState([])
  useEffect(() => {
    if (data) {
      const list = data.content.split(',')
      setContent(list)
    }
  }, [data])

  return (
    <>
      <FormFields>
        <FormLabel>
          {data.question}&nbsp;<span style={{ color: 'red' }}>*</span>
        </FormLabel>
        <Controller
          as={({ onChange, value }) => (
            <Radio.Group
              value={value}
              onChange={(e) => onChange(e.target.value)}
            >
              <Row>
                {content.map((itemQ, indexQ) => {
                  return (
                    <Col span={24}>
                      <Radio style={{color:'#333'}} value={itemQ}>{itemQ}</Radio>
                    </Col>
                  )
                })}
              </Row>
            </Radio.Group>
          )}
          name={`content${index}`}
          control={control}
          rules={{ required: true }}
        />
        {errors[`content${index}`] &&
          errors[`content${index}`].type === 'required' && (
            <Error>これは必須の項目です。</Error>
          )}
      </FormFields>
    </>
  )
}
export default Questionnaire
