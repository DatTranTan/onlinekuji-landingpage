// /* eslint-disable */
// import { useQuery } from '@apollo/react-hooks'
// import { Modal } from 'antd'
// import queryString from 'query-string'
// import React, { useState, useEffect } from 'react'
// import { useHistory, useLocation } from 'react-router-dom'
// import {
//   FormFields,
//   FormLabel
// } from '../../../../../components/FormFields/FormFields'
// import { QUERY_EVENT } from '../../../../../graphql/query/event'
// import TypeFirst from './Components/TypeFirst'
// import TypeSecond from './Components/TypeSecond'
// import TypeThird from './Components/TypeThird'

// const { confirm } = Modal

// type Props = any
// const Questionnaire: React.FC<Props> = (props) => {
//   const { setValue, register, errors, control, Controller } = props
//   const location = useLocation()
//   const event = queryString.parse(location.search).event

//   const { data } = useQuery(QUERY_EVENT, {
//     variables: {
//       id: event
//     }
//   })

//   const [questionList, setQuestionList] = useState([])
//   useEffect(() => {
//     if (data?.event?.questionnaire) {
//       setQuestionList(data?.event.questionnaire.questionList)
//     }
//   }, [data])
//   let cloneQuestionList = [...questionList]
//   const questionListOrderByASC = cloneQuestionList?.sort(
//     (a, b) => a?.id - b?.id
//   )

//   return (
//     <>
//       {questionListOrderByASC?.map((item, index) => {
//         return (
//           <>
//             {item.type === 'First' && (
//               <TypeFirst
//                 data={item}
//                 index={index}
//                 errors={errors}
//                 register={register}
//                 setValue={setValue}
//                 Controller={Controller}
//                 control={control}
//               />
//             )}
//             {item.type === 'Second' && (
//               <TypeSecond
//                 data={item}
//                 index={index}
//                 errors={errors}
//                 register={register}
//                 setValue={setValue}
//                 Controller={Controller}
//                 control={control}
//               />
//             )}
//             {item.type === 'Third' && (
//               <TypeThird
//                 data={item}
//                 index={index}
//                 errors={errors}
//                 register={register}
//                 setValue={setValue}
//               />
//             )}
//           </>
//         )
//       })}
//     </>
//   )
// }
// export default Questionnaire


/* eslint-disable */
import { useQuery } from '@apollo/react-hooks'
import { Modal, Row, Col } from 'antd'
import queryString from 'query-string'
import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  FormFields,
  FormLabel
} from '../../../../../components/FormFields/FormFields'
import { QUERY_EVENT } from '../../../../../graphql/query/event'
import TypeFirst from './Components/TypeFirst'
import TypeSecond from './Components/TypeSecond'
import TypeThird from './Components/TypeThird'

const { confirm } = Modal

type Props = any
const Questionnaire: React.FC<Props> = (props) => {
  const { setValue, register, errors, control, Controller, formData, isConfirm, questionList } = props

  return (
    <>
      {questionList?.map((item, index) => {
        return (
          <>
            <div style={{ display: isConfirm ? 'block' : 'none', padding: "5px 0" }}>
              {item.type === 'First' && (
                <TypeFirst
                  formData={formData}
                  data={item}
                  index={index}
                  errors={errors}
                  register={register}
                  setValue={setValue}
                  Controller={Controller}
                  control={control}
                />
              )}
              {item.type === 'Second' && (
                <TypeSecond
                  formData={formData}
                  data={item}
                  index={index}
                  errors={errors}
                  register={register}
                  setValue={setValue}
                  Controller={Controller}
                  control={control}
                />
              )}
              {item.type === 'Third' && (
                <TypeThird
                  formData={formData}
                  data={item}
                  index={index}
                  errors={errors}
                  register={register}
                  setValue={setValue}
                />
              )}
            </div>
            <div style={{ display: !isConfirm ? 'block' : 'none' }}>
              <FormFields>
                <FormLabel>
                  <Col><Row>質問:&nbsp;{item?.question}</Row></Col>
                  <Col style={{ marginBottom: '7px' }}><Row>答え:&nbsp;{typeof (formData?.[`content${index}`]) == 'string' ? formData?.[`content${index}`] :
                    formData?.[`content${index}`]?.join()}</Row></Col>
                </FormLabel>
              </FormFields>
            </div>
          </>
        )
      })}
    </>
  )
}
export default Questionnaire
