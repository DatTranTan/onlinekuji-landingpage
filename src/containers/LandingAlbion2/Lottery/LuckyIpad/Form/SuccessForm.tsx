import React from "react";
import { BlockImg, PrizeContainer } from "../LuckyIpad.style";
import { WrapperAlbion, ContainerAlbion } from "../../../LandingAlbion.style";
import BackgroundBannerSmall from "../../../img/hamamacho/banner-footer.png";

function SuccessForm() {
  return (
    <WrapperAlbion style={{ minHeight: "100vh" }}>
      <ContainerAlbion>
        <PrizeContainer>
          <div className="prize-title" style={{ paddingTop: "30px" }}>
            その場であたる！浜祭2021プレゼントキャンペーン{" "}
          </div>
          <BlockImg form={true}>
            <div
              className="block-title"
              style={{ paddingTop: "30px", fontSize: "2rem" }}
            >
              送信完了しました！{" "}
            </div>
            <img
              src={BackgroundBannerSmall}
              alt=""
              style={{
                padding: "30px",
                objectFit: "unset",
                width: "100%",
                cursor: "pointer",
              }}
              onClick={() =>
                window.open("https://www.joqr.co.jp/hama_matsuri/", "_blank")
              }
            />
          </BlockImg>
          <div className="prize-title"> </div>
        </PrizeContainer>
      </ContainerAlbion>
    </WrapperAlbion>
  );
}
export default SuccessForm;
