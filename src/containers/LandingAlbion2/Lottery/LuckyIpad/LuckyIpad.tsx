/* eslint-disable */
import React, { useState } from "react";
import {
  WrapperLucky,
  TextHeader,
  BlockCenter,
  BlockButton,
  ButtonRight,
  BlockVideo,
  WrapperVideo,
  WrapperPrize,
  BlockImg,
  VideoXXX,
  Div100vh,
  // ButtonForm,
  Ablank,
  DivText,
  ImgArrow,
  SvgStyle,
  PrizeContainer,
  ButtonLost,
} from "./LuckyIpad.style";
import { useMutation, useQuery } from "@apollo/client";
import { useLocation } from "react-router-dom";
import { MUTATION_SPIN } from "../../../../graphql/mutation/spin";
import { Notification } from "../../../../components/Notification/NotificationCustom";
import { QUERY_EVENT } from "../../../../graphql/query/event";
import BackgroundNoPrize from "./VideoPrize/image/background_hazure.png";
import GiftBackground from "./VideoPrize/image/item.png";
import Prize from "./VideoPrize/image/prize.png";
import * as qs from "query-string";
import { Spin, Button } from "antd";
import { use100vh } from "react-div-100vh";
import { v4 as uuidv4 } from "uuid";
import Cookies from "universal-cookie";
import {
  MUTATION_INCREASE_WIN,
  MUTATION_SPUN,
} from "../../../../graphql/mutation/spun";
import Forward from "./VideoPrize/image/forward.png";
import Gift from "../../../../assets/images/box1.png";
import BtnRedirectToForm from "./BtnRedirectToForm";
import PrizeAlbion from "../../../../components/PrizeAlbion2/PrizeAlbionSpin";
import BackgroundBannerSmall from "../../img/hamamacho/banner-footer.png";
import NoPrize from "../../img/hamamacho/lose.png";
import PRODUCTSINFO from "../../constants/constant";
import RoleBackground from "../../img/hamamacho/role-background.png";

function LuckyIpad({ dataOrderPrizes }) {
  const NewID = uuidv4();
  const cookies = new Cookies();
  let customerID = "";
  async function clickCookies() {
    let newDate = await cookies.get("roleDay");
    let newCustomerId = await cookies.get("userID");
    if (newCustomerId) {
      if (newDate == new Date().getDay()) {
        customerID = newCustomerId;
      } else {
        customerID = NewID;
        cookies.set("userID", NewID);
        cookies.set("roleDay", new Date().getDay());
      }
    } else {
      customerID = NewID;
      cookies.set("userID", NewID);
      cookies.set("roleDay", new Date().getDay());
    }
  }
  // const myFunction = () => {
  //   let element;
  //   window.innerWidth > 1050
  //     ? (element = document.getElementById("ViewVideo"))
  //     : (element = document.getElementById("ViewVideoMobile"));
  //   element.scrollIntoView();
  // };

  const height = use100vh();
  const location = useLocation();
  const eventId = qs.parse(location.search).eventId;
  const [showPrize, setShowPrize] = React.useState(true);
  const [play, setPlay] = useState(false);
  const [dataSpinByEvent, setDataSpinByEvent] = useState<any>();
  // const [idspin, setIdspin] = useState('')
  const [visibleButton, setVisibleButton] = useState(false);
  const [idSpin, setIdSpin] = useState();
  const [winPrize, setWinPrize] = useState(false);
  // const [onClickButton, setOnClickButton] = useState(false)
  const videoRef = React.useRef<HTMLVideoElement>(null);

  const [videoDimension, setVideoDimension] = useState({
    width: 0,
    height: 0,
  });
  const [loadingVideo, setLoadingVideo] = useState(true);
  const [customerLost, setCustomerLost] = useState("");
  const [style, setStyle] = React.useState({});
  const [showVideo, setShowVideo] = useState("");
  const [onStartVideo, setOnStartVideo] = useState(false);
  const [spinCircle, { loading }] = useMutation(MUTATION_SPIN, {
    fetchPolicy: "no-cache",
  });

  const [checkSpun] = useMutation(MUTATION_SPUN, {
    fetchPolicy: "no-cache",
  });
  const [increaseTotalWin] = useMutation(MUTATION_INCREASE_WIN, {
    fetchPolicy: "no-cache",
  });

  const { data, loading: loader } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventId,
    },
  });

  const reloadButton = () => {
    checkSpun({
      variables: {
        eventId,
        userId: customerID,
      },
    }).then(({ data }) => {
      // console.log(data, 'checkSpun')
      data?.checkSpun && setPlay(false);
      setLoadingVideo(false);
    });
    spinCircle({
      variables: {
        eventId,
        userId: customerID,
      },
    })
      .then(({ data }) => {
        setCustomerLost(data?.spin?.event?.customerLost);
        setDataSpinByEvent(data.spin);
        setIdSpin(data?.spin?.SpinPrizeID)
        if (
          data.spin.rank !== 0 ||
          (data.spin.rank === 0 && data.spin.event.customerLost === "Win")
        ) {
          setWinPrize(true);
          increaseTotalWin({
            variables: {
              eventId,
            },
          });
        }
        if (data.spin.rank === 0) {
          setWinPrize(true);
        }

        // setIdspin(data.spin.id)
      })
      // .then(() => window.location.reload())
      .catch(({ graphQLErrors }) =>
        // Notification({
        //   type: 'error',
        //   message: ' エラー',
        //   description: graphQLErrors[0]?.message
        // })
        {
          const mess = graphQLErrors.map((item) => item.message);
          console.log("graphQLErrors", mess);
          if (mess.includes("このイベントはまだ開始されていません")) {
            Notification({
              type: "error",
              message: " エラー",
              description: "イベントはまだ開始されていません。",
            });
            setShowPrize(true);
            setPlay(false);
          } else if (mess.includes("このイベントは既に終了しました")) {
            Notification({
              type: "error",
              message: " エラー",
              description: "このイベントは既に終了しました",
            });
            setShowPrize(true);
            setPlay(false);
          }else if (mess[0].includes("抽選しました")) {
            Notification({
              type: "error",
              message: " エラー",
              description: "本日は抽選済みです！１日１回チャレンジできます。",
            });
            setShowPrize(true);
            setPlay(false);
          } else {
            Notification({
              type: "error",
              message: " エラー",
              description: graphQLErrors[0]?.message,
            });
          }
        }
      );
    // .catch(({ graphQLErrors }) => {
    //   const mess = graphQLErrors.map((item) => item.message)
    //   if (mess.includes('このイベントはまだ開始されていません')) {
    //     Notification({
    //       type: 'error',
    //       message: ' エラー',
    //       description: 'イベントはまだ開催されていません。'
    //     })
    //     setShowPrize(true)
    //     setPlay(false)
    //   } else if (mess.includes('このイベントは既に終了しました')) {
    //     Notification({
    //       type: 'error',
    //       message: ' エラー',
    //       description: 'このイベントは既に終了しました'
    //     })
    //     setShowPrize(true)
    //     setPlay(false)
    //   } else {
    //     Notification({
    //       type: 'error',
    //       message: ' エラー',
    //       description: graphQLErrors[0]?.message
    //     })
    //   }
    // })
    // console.log(data, 'CheckSpin')
    setVisibleButton(false);
  };

  React.useEffect(() => {
    if (videoDimension?.width && videoDimension?.height) {
      if (videoDimension.width > videoDimension.height) {
        setStyle({ width: "100%", height: "auto", position: "absolute" });
      } else setStyle({ width: "100%", height: "auto", position: "absolute" });
    }
  }, [videoDimension]);

  React.useEffect(() => {
    if (dataSpinByEvent) {
      let video;
      if (
        data?.event.video !== null &&
        data?.event.video !== undefined &&
        dataSpinByEvent?.rank !== 0
      ) {
        video = data?.event.video?.url;
      } else if (
        data?.event.video !== null &&
        data?.event.video !== undefined &&
        dataSpinByEvent?.rank === 0
      ) {
        video =
          data?.event.noPrizeVideo === null ||
          data?.event.noPrizeVideo?.url === ""
            ? data?.event.video?.url
            : data?.event.noPrizeVideo.url;
      } else {
        video = "https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4";
      }
      setShowVideo(video);
    }
  }, [dataSpinByEvent]);

  // React.useEffect(() => {
  //     let virtualElement = document.querySelector('myVideo')
  //     virtualElement.setAttribute('src', showVideo)
  //     if(!isIOS){
  //       virtualElement.addEventListener('loadeddata', () => {
  //         if(virtualElement.offsetWidth && virtualElement.offsetHeight){
  //           setVideoDimension({
  //             width: virtualElement.offsetWidth,
  //             height: virtualElement.offsetHeight
  //           })
  //           setLoadingVideo(false)
  //           virtualElement.remove()
  //         }
  //       })
  //     } else {
  //       setVideoDimension({
  //         width: virtualElement.offsetWidth,
  //         height: virtualElement.offsetHeight
  //       })
  //       setLoadingVideo(false)
  //       virtualElement.remove()
  //     }
  //     document.getElementById('root').appendChild(virtualElement)
  // }, [showVideo])

  React.useEffect(() => {
    if (showVideo.length > 0 && dataSpinByEvent && videoRef.current) {
      videoRef.current.play();
      videoRef.current.muted = false;
    }
  }, [showVideo, dataSpinByEvent, videoRef]);

  const BannerPrize = () => (
    <div
      style={{
        background: "url(" + Prize + ")",
        backgroundRepeat: "no-repeat",
        width: "100%",
        height: "50%",
        objectFit: "cover",
        backgroundPosition: "center",
        backgroundSize: "contain",
      }}
    ></div>
  );

  return showPrize === true ? (
    <WrapperLucky id="ViewVideoMobile">
      <div className="prize-title">
        「抽選スタート」ボタンを押して抽選に参加！
      </div>
      <Div100vh
        id="ViewVideo"
        showPrize={showPrize}
        nowin={!showPrize && dataSpinByEvent?.rank === 0}
      >
        <BlockCenter height={height} style={{ width: "100%", height: "100%" }}>
          <BlockVideo
            height={height}
            style={{ width: "100%", height: "100%" }}
            nowin={!showPrize && dataSpinByEvent?.rank === 0}
          >
            <>
              <WrapperVideo style={{ width: "100%" }}>
                <>
                  {play === false ? (
                    <div className="mask">
                      <div className="mask" style={{    position: 'absolute',zIndex: 1}}>
                      </div>
                      <img
                        src={RoleBackground}
                        alt=""
                        style={{
                          objectFit: "unset",
                          width: "100%",
                          zIndex: 0,
                          // height: '350px'
                        }}
                      />
                      <ButtonRight
                        disabled={play}
                        onClick={async () => {
                          await clickCookies();
                          reloadButton();
                          setShowPrize(true);
                          setPlay(true);
                          // myFunction();
                        }}
                      >
                        抽選 スタート
                      </ButtonRight>
                    </div>
                  ) : (
                    <>
                      {loading || loadingVideo ? (
                        <div
                          style={{
                            width: "100%",
                            height: "100%",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <img
                            src={RoleBackground}
                            alt=""
                            style={{
                              objectFit: "cover",
                              width: "100%",
                              zIndex: 0,
                              // height: '350px'
                            }}
                          />
                          <Spin
                            style={{
                              zIndex: 2,
                              position: "absolute",
                              top: "50%",
                              left: "50%",
                            }}
                          />
                        </div>
                      ) : (
                        <div
                          style={{
                            width: "100%",
                            height: "100%",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <img
                            src={RoleBackground}
                            alt=""
                            style={{
                              objectFit: "cover",
                              width: "100%",
                              zIndex: 0,
                              // height: '350px'
                            }}
                          />
                          <VideoXXX
                            id="myVideo"
                            style={{
                              width: "100%",
                              height: "auto",
                              position: "absolute",
                            }}
                            onPlay={() =>
                              setTimeout(() => {
                                setOnStartVideo(true);
                              }, 1000)
                            }
                            onLoadedData={() => {
                              setVideoDimension({
                                width: videoRef.current.offsetWidth,
                                height: videoRef.current.offsetHeight,
                              });
                              setLoadingVideo(false);
                            }}
                            ref={videoRef}
                            src={showVideo}
                            autoPlay={true}
                            playsInline={true}
                            muted={true}
                            preload="yes"
                            controls={false}
                            onEnded={() => {
                              setShowPrize(false);
                              setTimeout(() => {
                                setPlay(false);
                                setVisibleButton(true);
                              }, 0);
                            }}
                          />
                        </div>
                      )}
                    </>
                  )}
                </>
              </WrapperVideo>
            </>
          </BlockVideo>
        </BlockCenter>
      </Div100vh>
    </WrapperLucky>
  ) : (
    <>
      {dataSpinByEvent?.rank !== 0 || customerLost === "Win" ? (
        <PrizeContainer>
          <div className="prize-title">
            その場であたる！浜祭2021プレゼントキャンペーン{" "}
          </div>
          <BlockImg>
            <div className="block-title">ご当選おめでとうございます！ </div>
            <PrizeAlbion
              key={1}
              namePrize={
                dataSpinByEvent?.name ? dataSpinByEvent?.name : "賞品名"
              }
              numberPrize={"item.quantity"}
              imagePrize={
                dataSpinByEvent?.imageUrl ? dataSpinByEvent?.imageUrl : Gift
              }
              // productInfo={dataOrderPrizes?.findIndex(item => item?.id === dataSpinByEvent?.id)}
            />
            <BtnRedirectToForm
              idSpin={idSpin}
              eventId={eventId}
              setShowPrize={setShowPrize}
              setPlay={setPlay}
              Forward={Forward}
            />
          {Number(dataSpinByEvent?.event?.timeOut) > 0 && 
          <p className='timeout-text'>当選時の応募情報の入力は時間制限がございます。<br/>
          {dataSpinByEvent?.event?.timeOut}分を越えての送信は当選無効となりますのでお早めにご入力・送信ください。</p>}
          </BlockImg>
        </PrizeContainer>
      ) : (
        <PrizeContainer>
          <div className="prize-title">
            その場であたる！浜祭2021プレゼントキャンペーン{" "}
          </div>
          <BlockImg>
            <img
              src={NoPrize}
              alt=""
              style={{
                padding: "30px 0",
                objectFit: "unset",
                width: "90%",
              }}
            />
            {/* <ButtonLost
            >
              プレゼント一覧をみる！記事にリンク！
            </ButtonLost> */}
            <img
              src={BackgroundBannerSmall}
              alt=""
              style={{
                padding: "30px",
                objectFit: "unset",
                width: "100%",
                cursor: "pointer",
                // height: '350px'
              }}
              onClick={() =>
                window.open("https://www.joqr.co.jp/hama_matsuri/", "_blank")
              }
            />
          </BlockImg>
        </PrizeContainer>
      )}
    </>
  );
}

export default LuckyIpad;
