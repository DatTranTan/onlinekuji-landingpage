import styled from "styled-components";
import { isDesktop } from "react-device-detect";
import Background from "./img/hamamacho/landing-background.png";
import BackgroundBanner from "./img/hamamacho/banner-background.png";

const WrapperAlbion = styled.div`
  background: url("${Background}");
  background-repeat: no-repeat;
  width: 100%;
  object-fit: cover;
  background-position: center;
  background-size: cover;
  color: #333;
`;
const ContainerAlbion = styled.div`
  background-color: transparent;
`;
const ContainerPanner = styled.div`
  width: 100%;
  border: 1;
  background-color: #fff;
  .image-footer{
    padding: 30px 220px;
    width: 100%;

    @media (max-width: 1100px) {
      padding: 30px 120px;
  }
    @media (max-width: 500px) {
      padding: 21px;
  }
  }
`;
const ContainerPrize = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  justify-content: center;
  background-color: transparent;
  border-top: none;
  padding: 5%;
`;
const ContainerIntroduction = styled.div`
  background-color: transparent;
  padding-bottom: 8rem;
  @media (max-width: 500px) {
    padding-bottom: 0rem;
    margin-bottom: 30px;
  }
`;
const ContainerLottery = styled.div``;
const ContainerPannerBot = styled.div`
  background-color: #ea5514;
  height: 80vh;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: 3.5rem;
  padding-right: 3.5rem;
  @media (max-width: 1500px) and (min-height: 800px) {
    height: 60vh;
  }
  @media (max-width: 1400px) and (min-height: 950px) {
    height: 57vh;
  }
  @media (max-width: 1100px) {
    height: 32vh;
  }
  @media (max-width: 1100px) and (max-height: 800px) {
    height: ${isDesktop ? "75vh" : "50vh"};
  }
  @media (max-width: 1100px) and (max-height: 420px) {
    height: ${isDesktop ? "75vh" : "80vh"};
  }
  @media (max-width: 500px) {
    /* padding-left: 2rem;
    padding-right: 2rem; */
    padding: 2rem;
    height: 72vh;
    flex-flow: column;
    justify-content: center;
  }

  @media (max-width: 500px) and (max-height: 760px) {
    /* padding-left: 2rem;
    padding-right: 2rem; */
    padding: 2rem;
    height: 82vh;
    flex-flow: column;
    justify-content: center;
  }
`;
const ImgHref = styled.div`
  .top-container {
    background: url("${BackgroundBanner}");
    background-repeat: no-repeat;
    width: 100%;
    height: auto;
    object-fit: cover;
    background-position: center;
    background-size: cover;
    text-align: center;
    padding-bottom: 23px;
  }
  .top-container-item-left {
    width: 55%;
  }
  .top-container-item-right {
    width: 45%;
    display: flex;
    color: white;
    font-size: 3rem;
    font-weight: 600;
    div {
      margin: auto;
      text-align: center;
    }
  }
  img {
    object-fit: unset;
    width: 35%;
    height: 35%;
    @media (max-width: 800px) {
      width: ${isDesktop?'35%':'80%'};
    }
  }
`;

const PannerLeft = styled.div`
  /* padding: 3rem; */
  width: 48%;
  height: 80%;
  background-color: #ffcc5a;
  border-radius: 64px;
  display: block;
  padding: 1rem;
  @media (max-width: 1080px) {
    border-radius: 30px;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 15rem;
    margin-bottom: 1rem;
  }
  @media (max-width: 500px) and (max-height: 760px) {
    width: 100%;
    height: 14rem;
    margin-bottom: 1rem;
  }
  @media (max-width: 350px) {
    height: 12rem;
  }
`;
const ImgLeft = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 64px;
  object-fit: unset;
  @media (max-width: 1080px) {
    border-radius: 30px;
    object-fit: unset;
  }
`;

const PannerRight = styled.div`
  /* padding: 3rem; */
  width: 48%;
  height: 80%;
  background-color: #ffcc5a;
  border-radius: 64px;
  display: block;
  padding: 1rem;
  @media (max-width: 1080px) {
    border-radius: 30px;
  }
  @media (max-width: 500px) {
    width: 100%;
    height: 15rem;
    margin-top: 1rem;
  }
  @media (max-width: 500px) and (max-height: 760px) {
    width: 100%;
    height: 14rem;
    margin-top: 1rem;
  }
  @media (max-width: 350px) {
    height: 12rem;
  }
`;
const ImgRight = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 64px;
  object-fit: unset;
  @media (max-width: 1080px) {
    border-radius: 10px;
    object-fit: unset;
  }
`;
const Footer = styled.div`
  height: 15rem;
  width: 100%;
  @media (max-width: 1080px) {
    height: 12rem;
  }
  @media (max-width: 500px) {
    height: 8rem;
  }
`;
const Content1 = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-flow: column;
  background: white;
  padding: 2rem 10px;
`;
const LineTop = styled.div`
  background: #74daf7;
  max-height: 70px;
  height: 10vw;
  width: 100%;
  @media (min-width: 600px) {
    height: 6vw;
  }
`;
const ContainerTitleSub = styled.div`
  font-size: 2rem;
  font-weight: 900;
  z-index: 10;
  margin-bottom: 10px;
  @media (max-width: 1100px) {
    font-size: 1.5rem;
    margin-bottom: 5px;
  }
  @media (max-width: 500px) {
    font-size: 1.2rem;
    margin-bottom: 5px;
  }
`;

const ContainerTitleSubSpin = styled.div`
  font-size: 1.5rem;
  font-weight: 900;
  z-index: 10;
  margin-top: 60px;
  text-align: center;
  @media (max-width: 1100px) {
    font-size: 1.5rem;
  }
`;
const ContainerTitle = styled.div`
  color: #ea5514;
  font-size: 2.5rem;
  font-weight: 900;
  z-index: 10;
  text-align: center;
  word-break: break-all;
  @media (max-width: 1100px) {
    font-size: 2rem;
  }
  @media (max-width: 500px) {
    font-size: 1.5rem;
  }
`;
const LineImg = styled.img`
  width: 400px;
  margin-top: -22px;
  @media (max-width: 1500px) {
    width: 320px;
  }
`;
const ImgBackTop = styled.img`
  width: 9rem;
  height: auto;
  @media (max-width: 800px) {
    width: 8rem;
  }
  @media (max-width: 500px) {
    width: 5rem;
  }
`;
export {
  WrapperAlbion,
  ContainerAlbion,
  ContainerPanner,
  ContainerPrize,
  ContainerIntroduction,
  ContainerLottery,
  ContainerPannerBot,
  PannerLeft,
  PannerRight,
  Footer,
  Content1,
  LineTop,
  ContainerTitle,
  LineImg,
  ImgBackTop,
  ImgLeft,
  ImgRight,
  ImgHref,
  ContainerTitleSub,
  ContainerTitleSubSpin,
};
