/* eslint-disable */
import React, { useState, useEffect } from 'react'
import {
    WrapperPrize,
    ContainerPrizeB,
    ContainerPanner,
    ContainerPrize,
    ContainerIntroduction,
    ContainerLottery,
    ContainerPannerBot,
    PannerLeft,
    PannerRight,
    Footer,
    Content1,
    LineTop,
    ContainerTitle,
    LineImg,
    ImgBackTop,
    ImgLeft,
    ImgRight,
    ImgHref,
    ContainerTitleSub,
    ContainerTitleSubSpin, Div100vh, WrapperLucky, WrapperCard, ButtonToForm
} from './Prize.style'
import {
    Form,
    ButtonGroup,
    Container,
    Wrapper,
    ContainerForm,
    BlockForm,
    BlockTitle
} from '../Lottery/LuckyIpad/Form/FormPrize.style'
import {
    FormFields,
    FormLabel,
    Error
} from '../../../components/FormFields/FormFields'
import PrizeAlbion from '../../../components/PrizeAlbion/PrizeAlbion'
import PrizeBanner from './PrizeBanner/PrizeBanner'
import BtnRedirectToForm from '../Lottery/LuckyIpad/BtnRedirectToForm'
import Lottery from '../Lottery/Lottery'
import ImageNull from '../../../assets/images/box2.png'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { Scrollbars } from 'react-custom-scrollbars'
import Input from '../../../components/Input/Input'
import Button, { KIND } from '../../../components/Button/Button'
import Select from '../../../components/Select/Select'
import { Popconfirm, Modal, Checkbox, Radio, BackTop, Card, Row, Col } from 'antd'
import queryString from 'query-string'
import { Notification } from '../../../components/Notification/NotificationCustom'
import { OverideSave, OverideCancel } from '../../LandingAlbion/Lottery/LuckyIpad/Form/Overrides'
import { MUTATION_CREATE_CUSTOMER } from '../../../graphql/mutation/create-customer'
import { RegisterCustomerForm } from '../../LandingAlbion/Lottery/LuckyIpad/Form/Interface'
import { MUTATION_SPIN } from '../../../graphql/mutation/spin'
import { toNumeric } from 'japanese-string-utils'
import { useHistory, useLocation } from 'react-router-dom'
import { SUCCESSFORM } from '../../../settings/constants'
import { from } from 'apollo-link'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { QUERY_QUESTIONNAIRE } from '../../../graphql/query/questionnaire'
import { QUERY_EVENT } from '../../../graphql/query/event'
import Questionnaire from '../Lottery/LuckyIpad/Form/Questionnaire'
import { MUTATION_CONFIRM_PRIZE } from '../../../graphql/mutation/confirm-prize'
const { confirm } = Modal
var genders = [
    { label: '男性', id: 0 },
    { label: '女性', id: 1 },
    { label: '回答しない', id: 2 }
]
interface EventFormInput {
    name: string
    contact_number: string
    email: string
    reemail: string
    address: string
    thirdRequired: string
    thirdNonRequired: string
}

type Props = any
const PrizeForm: React.FC<Props> = (props) => {
    const location = useLocation()
    const history = useHistory()
    const eventID = queryString.parse(location.search).spin
    const event = queryString.parse(location.search).event
    const userRef = React.useRef<any>(null)
    const selectGenderRef = React.useRef<HTMLInputElement | HTMLDivElement>(null)
    const [visible, setVisible] = useState(false)
    const [modal, contextHolder] = Modal.useModal()
    const [multiAnswer, setMultiAnswer] = useState({})
    const [justOneAnswer, setJustOneAnswer] = useState({ index: '', value: '' })
    const [textRequired, setTextRequired] = useState({})
    const [textNonRequired, setTextNonRequired] = useState({})
    const [isConfirm, setIsConfirm] = useState(true)
    const [formData, setFormData]: any = useState({});
    const { data: dataEvent } = useQuery(QUERY_EVENT, {
        variables: {
            id: event
        }
    })
    const [confirmPrize] = useMutation(MUTATION_CONFIRM_PRIZE)
    const questionnaire = dataEvent?.event.questionnaire?.questionList
    const listQuestion = questionnaire?.map((item) => ({
        id: item?.id,
        listQuestion: item?.question
    }))
    const [listAnswer, setListAnswer] = useState([])
    // const [answer, setAnswer] = useState()
    const [questionList, setQuestionList] = useState([])
    useEffect(() => {
        if (dataEvent?.event?.questionnaire) {
            setQuestionList(dataEvent?.event.questionnaire.questionList)
        }
    }, [dataEvent])

    const { register, handleSubmit, setValue, getValues, errors, setError, reset, control } =
        useForm<RegisterCustomerForm>({
            defaultValues: {
                name: '',
                gender: '',
                contact_number: '',
                email: '',
                reemail: '',
                address: '',
                postalCode: ''
            }
        })



    const [text, setText] = React.useState('')
    const [type, setType] = React.useState(null)
    const [onDisable, setOnDisable] = React.useState(false)
    const [AddCustomer] = useMutation(MUTATION_CREATE_CUSTOMER, {
        fetchPolicy: 'no-cache'
    })
    const handleTypeChange = (value) => {
        setValue('gender', value[0].id)
        setType(value)
    }
    const onChange = (e, index) => {
        setMultiAnswer({ index, value: e })
    }

    const onChangeJustOne = (e, index) => {
        setJustOneAnswer({ index, value: e.target.value })
    }

    const textNonRequiredHandler = (index, e) => {
        setTextNonRequired({ index, value: e.target.value })
    }
    const textRequiredHandler = (index, e) => {
        setTextRequired({ index, value: e.target.value })
    }

    const handleFormSubmit = (data) => {
        const questionnaire = []
        questionList.map((item, index) => {
            questionnaire.push({
                ...item,
                answer: data[`content${index}`]
            })
        })
        if (data.email !== data.reemail) {
            setError('email', 'notmatch', 'abcxyz')
            setError('reemail', 'notmatch', 'abcxyz')
        }
        data.email === data.reemail && (AddCustomer({
            variables: {
                createCustomerInput: {
                    name: data.name,
                    age: parseFloat(toNumeric(data?.age)),
                    // age: 11,
                    // gender: data.gender,
                    gender: type[0]?.label,
                    // gender: 'ff',
                    tel: data.contact_number,
                    email: data.email,
                    address: data.address,
                    postalCode: data.postalCode,
                    prizeId: eventID,
                    questionnaire: JSON.stringify(questionnaire)
                }
            }
        })
            .then(() => {
                Notification({
                    type: 'success',
                    message: 'ご応募頂き有難うございました。'
                })
                // props.setVisible(false)
                reset()
                history.replace(SUCCESSFORM)
                // console.log(history.go(-1), 'Check History')
            })
            .catch((err) => {
                Notification({ type: 'error', message: err.toString() })
            }))
        // confirm({
        //   title: '情報を送信します。よろしいですか？',
        //   icon: <ExclamationCircleOutlined />,
        //   // content: 'Some descriptions',
        //   onOk: () => {
        //     const answer = questionList.map((item, index) => { })

        //   }
        // })
    }
    const onSubmit = (data: any) => {
        setIsConfirm(false)
        setFormData(data)
    }

    const handleBackForm = (data: any) => {
        setIsConfirm(true)
    }

    React.useEffect(() => {
        register({ name: 'name', required: true, maxLength: 50 })
        register({ name: 'tel', required: true, maxLength: 20 })
        register({ name: 'email' }, { required: true, maxLength: 100 })
        register({ name: 'reemail' }, { required: true, maxLength: 100 })
        register({ name: 'address', required: true })
        register({ name: 'age', required: true })
        register({ name: 'gender' }, { required: true })
        // register({ name: 'CheckNumber' }, { required: true })
        register({ name: 'postalCode', required: true })
    }, [register])

    // console.log(props.idspin, 'idspin')

    React.useEffect(() => {
        if (userRef.current) userRef.current.focus()
    }, [userRef])
    let newArrQuestion = []
    listQuestion?.forEach((item1) => {
        const code = listAnswer?.find((item2) => item2?.index === item1.id)
        if (code) {
            const array = Array.isArray(code?.value)
                ? code?.value.join(',')
                : code.value
            newArrQuestion.push(item1.listQuestion, array)
        }
    })
    // console.log(questionList.map((item, index) => { }))
    questionList.map((item, index) => {
        console.log(item?.question)
    })


    return (
        <WrapperPrize>
            <ContainerPrizeB>
                <ContainerLottery>
                    <WrapperLucky>
                        <ContainerTitle>その場で当たる！浜祭2021プレゼントキャンペーン！</ContainerTitle>
                        <Div100vh>
                            <WrapperCard>
                                <Form onSubmit={handleSubmit(onSubmit)} id='myForm'>
                                    <>
                                        <Wrapper style={{ display: !isConfirm ? 'none' : 'block' }}>
                                            <BlockTitle>入力画面</BlockTitle>
                                            <p style={{ color: 'rgb(102, 109, 146)' }}>
                                                必要な情報を記入して下さい。
                                                <span style={{ color: 'red' }}>(*)は必須の項目です</span>
                                            </p>
                                            <FormFields>
                                                <FormLabel>
                                                    氏名&nbsp;<span style={{ color: 'red' }}>*</span>
                                                </FormLabel>
                                                <Input
                                                    name='name'
                                                    clearOnEscape
                                                    inputRef={(e: any) => {
                                                        register(e, {
                                                            required: true,
                                                            maxLength: 50,
                                                            minLength: 4
                                                        })
                                                        userRef.current = e
                                                    }}
                                                />
                                                {errors.name && errors.name.type === 'required' && (
                                                    <Error>これは必須の項目です。</Error>
                                                )}
                                                {errors.name && errors.name.type === 'minLength' && (
                                                    <Error>氏名は4文字以上設定してください。</Error>
                                                )}
                                                {errors.name && errors.name.type === 'maxLength' && (
                                                    <Error>氏名は50文字以下設定してください。</Error>
                                                )}
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel>年齢</FormLabel>
                                                <Input
                                                    placeholder='年齢を入力してください(半角/全角)'
                                                    inputRef={register({
                                                        maxLength: 2,
                                                        minLength: 0,
                                                        pattern:
                                                            /^[^a-z\^@\^!\^#\^$\^%\^^\^&\^*\^+\^=\^.\^<\^>\^:\^,\^"\^~\^`\^_\^-\^ぁ-ゔ\^一-龠\^ァ-ヴー\^＠\^！\^＃\^＄\^％\^＾\^＆\^＊\^（\^）\^＿\^-]+$/
                                                    })}
                                                    name='age'
                                                />
                                                {errors.age && errors.age.type === 'pattern' && (
                                                    <Error>年齢には100以下入力してください。</Error>
                                                )}
                                                {errors.age && errors.age.type === 'minLength' && (
                                                    <Error>年齢は0歳以上入力してください。</Error>
                                                )}
                                                {errors.age && errors.age.type === 'maxLength' && (
                                                    <Error>年齢には100以下入力してください。</Error>
                                                )}
                                            </FormFields>

                                            <FormFields>
                                                <FormLabel>
                                                    性別&nbsp;<span style={{ color: 'red' }}>*</span>
                                                </FormLabel>
                                                <Select
                                                    options={genders}
                                                    name='gender'
                                                    labelKey='label'
                                                    valueKey='id'
                                                    value={type}
                                                    clearable={false}
                                                    placeholder='性別を選択する'
                                                    onChange={({ value }) => {
                                                        handleTypeChange(value)
                                                    }}
                                                />
                                                {errors.gender && errors.gender.type === 'required' && (
                                                    <Error>これは必須の項目です。</Error>
                                                )}
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel>
                                                    住所&nbsp;<span style={{ color: 'red' }}>*</span>
                                                </FormLabel>
                                                <Input
                                                    inputRef={register({ maxLength: 50, required: true })}
                                                    name='address'
                                                />
                                                {errors.address && errors.address.type === 'required' && (
                                                    <Error>これは必須の項目です。</Error>
                                                )}
                                                {errors.address && errors.address.type === 'maxLength' && (
                                                    <Error>住所は50文字以下設定してください。</Error>
                                                )}
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel>
                                                    メールアドレス&nbsp;<span style={{ color: 'red' }}>*</span>
                                                </FormLabel>
                                                <Input
                                                    inputRef={register({
                                                        required: true,
                                                        maxLength: 100,
                                                        pattern:
                                                            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                                    })}
                                                    name='email'
                                                />
                                                {errors.email && errors.email.type === 'required' && (
                                                    <Error>これは必須の項目です。</Error>
                                                )}
                                                {errors.email && errors.email.type === 'pattern' && (
                                                    <Error>メールアドレスの形式は正しくありません。</Error>
                                                )}
                                                {errors.email && errors.email.type === 'maxLength' && (
                                                    <Error>メールアドレス100文字以下設定してください。</Error>
                                                )}
                                                {errors.email && errors.email.type === 'notmatch' && (
                                                    <Error>確認用のメールアドレスは一致していません</Error>
                                                )}
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel>
                                                    メールアドレスの確認&nbsp;
                                                    <span style={{ color: 'red' }}>*</span>
                                                </FormLabel>
                                                <Input
                                                    inputRef={register({
                                                        required: true,
                                                        maxLength: 100,
                                                        pattern:
                                                            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                                    })}
                                                    name='reemail'
                                                />
                                                {errors.reemail && errors.reemail.type === 'required' && (
                                                    <Error>これは必須の項目です。</Error>
                                                )}
                                                {errors.reemail && errors.reemail.type === 'pattern' && (
                                                    <Error>メールアドレスの形式は正しくありません。</Error>
                                                )}
                                                {errors.reemail && errors.reemail.type === 'maxLength' && (
                                                    <Error>メールアドレス100文字以下設定してください。</Error>
                                                )}
                                                {errors.reemail && errors.reemail.type === 'notmatch' && (
                                                    <Error>確認用のメールアドレスは一致していません</Error>
                                                )}
                                            </FormFields>

                                            <FormFields>
                                                <FormLabel>
                                                    電話番号&nbsp;<span style={{ color: 'red' }}>*</span>
                                                </FormLabel>
                                                <Input
                                                    name='contact_number'
                                                    inputRef={register({ maxLength: 20, required: true })}
                                                />
                                                {errors.contact_number &&
                                                    errors.contact_number.type === 'required' && (
                                                        <Error>これは必須の項目です。</Error>
                                                    )}
                                                {errors.contact_number &&
                                                    errors.contact_number.type === 'maxLength' && (
                                                        <Error>電話番号は20文字以下設定してください。</Error>
                                                    )}
                                            </FormFields>

                                            <FormFields>
                                                <FormLabel>
                                                    郵便番号&nbsp;<span style={{ color: 'red' }}>*</span>
                                                </FormLabel>
                                                <Input
                                                    inputRef={register({ maxLength: 50, required: true })}
                                                    name='postalCode'
                                                />
                                                {errors.postalCode && errors.postalCode.type === 'required' && (
                                                    <Error>これは必須の項目です。</Error>
                                                )}
                                                {errors.postalCode &&
                                                    errors.postalCode.type === 'maxLength' && (
                                                        <Error> 郵便番号は50文字以下設定してください。</Error>
                                                    )}
                                            </FormFields>
                                            <Questionnaire
                                                formData={formData}
                                                isConfirm={isConfirm}
                                                errors={errors}
                                                register={register}
                                                setValue={setValue}
                                                Controller={Controller}
                                                control={control}
                                            />
                                            <ButtonGroup>
                                                <Button disable={true} type='submit' overrides={OverideSave}>
                                                    次へ
                                                </Button>
                                            </ButtonGroup>
                                        </Wrapper>
                                        <Wrapper style={{ display: isConfirm ? 'none' : 'block' }}>
                                            <BlockTitle>確認画面</BlockTitle>
                                            <FormFields>
                                                <FormLabel><Row>
                                                    <Col span={10}>氏名:</Col>
                                                    <Col >{formData?.name}</Col>
                                                </Row></FormLabel>
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel><Row>
                                                    <Col span={10}>年齢:</Col>
                                                    <Col >{formData?.age}</Col>
                                                </Row></FormLabel>
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel><Row>
                                                    <Col span={10}>性別:</Col>
                                                    <Col >{formData?.gender == '0' ? genders[0].label :
                                                        formData?.gender == '1' ? genders[1].label : genders[2].label}</Col>
                                                </Row></FormLabel>
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel><Row>
                                                    <Col span={10}>住所:</Col>
                                                    <Col >{formData?.address}</Col>
                                                </Row></FormLabel>
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel><Row>
                                                    <Col span={10}>メールアドレス:</Col>
                                                    <Col >{formData?.email}</Col>
                                                </Row></FormLabel>
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel><Row>
                                                    <Col span={10}>メールアドレスの確認:</Col>
                                                    <Col >{formData?.reemail}</Col>
                                                </Row></FormLabel>
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel><Row>
                                                    <Col span={10}>電話番号:</Col>
                                                    <Col >{formData?.contact_number}</Col>
                                                </Row></FormLabel>
                                            </FormFields>
                                            <FormFields>
                                                <FormLabel><Row>
                                                    <Col span={10}>郵便番号:</Col>
                                                    <Col >{formData?.postalCode}</Col>
                                                </Row></FormLabel>
                                            </FormFields>
                                            <Questionnaire
                                                errors={errors}
                                                register={register}
                                                setValue={setValue}
                                                Controller={Controller}
                                                control={control}
                                                formData={formData}
                                                isConfirm={isConfirm}
                                            />
                                            <ButtonGroup>
                                                <Button type='button' onClick={() => handleBackForm(formData)} overrides={OverideSave}>
                                                    戻る
                                                </Button>
                                                <Button type='button' onClick={() => handleFormSubmit(formData)} overrides={OverideSave}>
                                                    応募する
                                                </Button>
                                            </ButtonGroup>
                                        </Wrapper>
                                    </>
                                </Form>
                            </WrapperCard>
                        </Div100vh>
                    </WrapperLucky>
                </ContainerLottery>

            </ContainerPrizeB>
        </WrapperPrize>
    )

}
export default PrizeForm
