import React from 'react'
import {
    WrapperLoss,
    ContainerLoss,
    ContainerPanner,
    ContainerPrize,
    ContainerIntroduction,
    ContainerLottery,
    ContainerPannerBot,
    PannerLeft,
    PannerRight,
    Footer,
    Content1,
    LineTop,
    ContainerTitle,
    LineImg,
    ImgBackTop,
    ImgLeft,
    ImgRight,
    ImgHref,
    ContainerTitleSub,
    ContainerTitleSubSpin,
    WrapperLucky,
    TextHeader,
    BlockCenter,
    TextBottom,
    BlockButton,
    ButtonLeft,
    ButtonRight,
    BlockVideo,
    WrapperVideo,
    WrapperPrize,
    BlockImg,
    Blocktext,
    VideoXXX,
    Div100vh,
    ImgPrize,
    ImgGift,
    ImgBackgroundNoprize,
    ButtonForm,
    Ablank,
    DivText,
    ImgArrow,
    SvgStyle,
    ButtonToForm
} from './PrizeBanner.style'
// import LuckyIpad from './LuckyIpad/LuckyIpad'

function PrizeBanner() {
    return (
        <ContainerLottery>
            <WrapperLucky id='ViewVideoMobile'>
                <Div100vh
                    
                >
                    {/* <BlockCenter
                        height={height}
                        style={{ width: '100%', height: '100%' }}
                    >
                        <BlockVideo
                            height={height}
                            style={{ width: '100%', height: '100%' }}
                            nowin={!showPrize && dataSpinByEvent?.rank === 0}
                        >
                            <WrapperVideo style={{ width: '100%' }}>
                                {!showPrize === true ? (
                                    <WrapperPrize style={{ width: '100%', height: '100%' }}>
                                        {dataSpinByEvent?.rank !== 0 ? (
                                            <>
                                                <div className="spin-title">ご当選おめでとうございます！ </div>
                                                <BlockImg>
                                                    <PrizeAlbion
                                                        key={1}
                                                        namePrize={dataSpinByEvent?.name ? dataSpinByEvent?.name : '賞品名'}
                                                        numberPrize={'item.quantity'}
                                                        imagePrize={dataSpinByEvent?.imageUrl ? dataSpinByEvent?.imageUrl : Gift}
                                                    />
                                                    <BtnRedirectToForm
                                                        idSpin={idSpin}
                                                        eventId={eventId}
                                                        setShowPrize={setShowPrize}
                                                        setPlay={setPlay}
                                                        Forward={Forward}
                                                    />
                                                    {visibleButton === true && (
                                                        <BtnRedirectToForm
                                                            idSpin={idSpin}
                                                            eventId={eventId}
                                                            setShowPrize={setShowPrize}
                                                            setPlay={setPlay}
                                                            Forward={Forward}
                                                        />
                                                    )}
                                                </BlockImg>
                                            </>
                                        ) : (
                                            <>
                                                <BlockImg>

                                                    {customerLost === 'Win' && <BannerPrize />}

                                                    <div
                                                        style={{
                                                            background: dataSpinByEvent?.imageUrl
                                                                ? 'url(' + dataSpinByEvent?.imageUrl + ')'
                                                                : customerLost === 'Win'
                                                                    ? 'url(' + Gift + ')'
                                                                    : 'url(' + BackgroundNoPrize + ')',
                                                            backgroundRepeat: 'no-repeat',
                                                            width: '100%',
                                                            height: '100%',
                                                            objectFit:
                                                                customerLost === 'Win' ? 'cover' : 'unset',
                                                            backgroundPosition: 'center',
                                                            backgroundSize:
                                                                customerLost === 'Win' ? 'contain' : 'cover',
                                                            borderRadius: '19px'
                                                        }}
                                                    ></div>
                                                    {customerLost === 'Win' && (
                                                        <BtnRedirectToForm
                                                            idSpin={idSpin}
                                                            eventId={eventId}
                                                            setShowPrize={setShowPrize}
                                                            setPlay={setPlay}
                                                            Forward={Forward}
                                                        />
                                                    )}
                                                </BlockImg>
                                            </>

                                            // <ImgBackgroundNoprize src={dataSpinByEvent?.imageUrl ? dataSpinByEvent?.imageUrl : BackgroundNoPrize} alt='' />
                                        )}
                                    </WrapperPrize>
                                ) : (
                                    <>
                                        {play === false ? (
                                            <ButtonRight
                                                disabled={play}
                                                onClick={async () => {
                                                    await clickCookies()
                                                    reloadButton()
                                                    setShowPrize(true)
                                                    setPlay(true)
                                                    // play == true && window.location.reload()
                                                    myFunction()
                                                }}
                                            >
                                                抽選スタート
                                            </ButtonRight>
                                        ) : (
                                            <>
                                                {loading || loadingVideo ? (
                                                    <div
                                                        style={{
                                                            width: '100%',
                                                            height: '100%',
                                                            display: 'flex',
                                                            justifyContent: 'center',
                                                            alignItems: 'center'
                                                        }}
                                                    >
                                                        <Spin />
                                                    </div>
                                                ) : (
                                                    <VideoXXX
                                                        id='myVideo'
                                                        style={style}
                                                        onLoadedData={() => {
                                                            setVideoDimension({
                                                                width: videoRef.current.offsetWidth,
                                                                height: videoRef.current.offsetHeight
                                                            })
                                                            setLoadingVideo(false)
                                                        }}
                                                        ref={videoRef}
                                                        src={showVideo}
                                                        autoPlay={true}
                                                        playsInline={true}
                                                        muted={true}
                                                        preload='yes'
                                                        controls={false}
                                                        onEnded={() => {
                                                            setShowPrize(false)
                                                            // setTimeout(() => {
                                                            //   setPlay(false)
                                                            // }, 3000)
                                                            setTimeout(() => {
                                                                // dataSpinByEvent?.rank !== 0 &&
                                                                // setvisibleForm(true)
                                                                setPlay(false)
                                                                setVisibleButton(true)
                                                            }, 0)
                                                        }}
                                                    />
                                                )}
                                            </>
                                        )}
                                    </>
                                )}
                            </WrapperVideo>
                        </BlockVideo>
                    </BlockCenter> */}
                </Div100vh>
            </WrapperLucky>
        </ContainerLottery>
    )
}

export default PrizeBanner
