/* eslint-disable */
import React, { useState } from 'react'
import {
  WrapperAlbion,
  ContainerAlbion,
  ContainerPanner,
  ContainerPrize,
  ContainerIntroduction,
  ContainerLottery,
  ContainerPannerBot,
  PannerLeft,
  PannerRight,
  Footer,
  Content1,
  LineTop,
  ContainerTitle,
  LineImg,
  ImgBackTop,
  ImgLeft,
  ImgRight,
  ImgHref,
  ContainerTitleSub,
  ContainerTitleSubSpin
} from './LandingAlbion.style'
import PrizeAlbion from '../../components/PrizeAlbion2/PrizeAlbion'
import { QUERY_EVENT } from '../../graphql/query/event'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { useLocation } from 'react-router-dom'
import queryString from 'query-string'
import Line1 from './img/line1.png'
import IntroductionCampaign from './Introduction/IntroductionCampaign'
import Lottery from './Lottery/Lottery'
import Line4 from './img/line4.png'
import { BackTop } from 'antd'
import { DivBackTop } from '../LotteryPage/Spinning/LuckyOpenIpad.style'
import BackToTop from './img/backtobutton.png'
import { useEffect } from 'react'
import { MUTATION_VISIT } from '../../graphql/mutation/total-visit'
import BackgroundBanner from "./img/hamamacho/banner-background.png";
import BackgroundBannerSmall from "./img/hamamacho/banner1.png";
import FOOTER from "./img/hamamacho/banner-footer.png";
import {Row, Col} from 'antd';
import PRODUCTSINFO, {TITLEINFO} from './constants/constant' ;

function LandingAlbion() {
  // const slideImages = [
  //   './img/background_hazure.png',
  //   './img/background_atari.png'
  // ]
  const location = useLocation()
  const eventID = queryString.parse(location.search).eventId
  const { data } = useQuery(QUERY_EVENT, {
    variables: {
      id: eventID
    }
  })

  let dataPrizes = eventID && data?.event.prizes
  let losePrizes = dataPrizes && dataPrizes?.filter((item) => item.rank === 0)
  let winPrizes = dataPrizes && dataPrizes?.filter((item) => item.rank !== 0)
  const dataOrderPrizes = dataPrizes && [...winPrizes, ...losePrizes]
  const [increaseTotalVisit] = useMutation(MUTATION_VISIT, {
    fetchPolicy: 'no-cache'
  })

  useEffect(() => {
    increaseTotalVisit({
      variables: {
        eventId: eventID
      }
    })
    document.title = 'Hamamatsu'
    const favicon = document.querySelector("link");
    favicon.href = "https://www.google.com/favicon.ico";
  }, [])

  return (
    <WrapperAlbion>
      <ContainerAlbion>
      <ContainerPanner>
          <ImgHref
            // href={data?.event?.bannerUrl}
            // target={data?.event?.bannerUrl === '' ? '' : '_blank'}
            // disabled={data?.event.bannerUrl === '' && true}
          >
            <div className='top-container'>   
            <img
              src={BackgroundBannerSmall}
              alt=''
            />
            </div>
          </ImgHref>
        </ContainerPanner>
        <Content1>
          <ContainerTitleSub>週替わりでプレゼントがかわる！</ContainerTitleSub>
          <ContainerTitle>{TITLEINFO.week2.title}</ContainerTitle>
        </Content1>
        <ContainerPrize className='role-container'>
        <Row gutter={24} justify="space-around" style={{width: '100%'}}>
          {/* {dataOrderPrizes
            ?.filter((item) =>
              data?.event.customerLost === 'Win' ? item : item?.rank !== 0
            )
            ?.map((item, index) => (
                <Col  
                xs={24}  
                sm={24}
                lg={8}
                xl={8}
                >
              <PrizeAlbion
                key={index}
                {...item}
                namePrize={item.name}
                numberPrize={item.quantity}
                imagePrize={item.imageUrl}
                productInfo={PRODUCTSINFO[index<=2?index:2]}
              />
                </Col>
            ))} */}
          {PRODUCTSINFO.map((item, index) => (
                <Col  
                xs={24}  
                sm={24}
                lg={8}
                xl={8}
                >
              <PrizeAlbion
                key={index}
                productInfo={PRODUCTSINFO[index]}
              />
                </Col>
            ))}
          </Row>
        </ContainerPrize>

        <ContainerLottery>
          <Lottery dataOrderPrizes={dataOrderPrizes}/>
        </ContainerLottery>

        <ContainerIntroduction>
        {/* <Introduction data={data}/>         */}
            <IntroductionCampaign data={data}/>
        </ContainerIntroduction>

        <ContainerPanner style={{textAlign:'center'}}>
          {/* <ImgHref
            href={data?.event?.bannerUrl === '' ? '#' : data?.event?.bannerUrl}
            target={data?.event?.bannerUrl === '' ? '' : '_blank'}
            disabled={data?.event.bannerUrl === '' && true}
          >
            <img
              src={data?.event.banner}
              alt=''
              style={{
                objectFit: 'unset',
                width: '100%',
                height: 'auto',
                maxHeight: '1200px'
              }}
            />
          </ImgHref> */}
          <img
              src={FOOTER}
              alt=''
              className='image-footer'
              style={{cursor:'pointer'}}
              onClick={() => window.open('https://www.joqr.co.jp/hama_matsuri/','_blank')}
            />
        </ContainerPanner>

        <DivBackTop>
          <BackTop duration={0}>
          <img
              src={BackToTop}
              alt='BackTop'
              className='image-backtop'
            />
            {/* <ImgBackTop src={BackToTop} alt='BackTop' /> */}
          </BackTop>
        </DivBackTop>
      </ContainerAlbion>
    </WrapperAlbion>
  )
}

export default LandingAlbion
