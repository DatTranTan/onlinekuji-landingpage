import PRODUCT from "../img/hamamacho/products.png";
import PRODUCT1 from "../img/hamamacho/product1.png";
import PRODUCT2 from "../img/hamamacho/product2.png";
import PRODUCT3 from "../img/hamamacho/product3.png";
import PRODUCTB1 from "../img/hamamacho/week2/product1.png";
import PRODUCTB2 from "../img/hamamacho/week2/product2.png";
import PRODUCTB3 from "../img/hamamacho/week2/product3.png";

const PRODUCTSINFO = {
  seasonOne: [
    {
      id: 1,
      name: "11月3日イベント（「『ヴァイナル ・ミュージック～歌謡曲2.0 ～』浜祭スペシャルライブ）参加券",
      description: "",
      image: PRODUCT1,
      quantity: 5,
    },
    {
      id: 2,
      name: "アナウンサー直筆サイン入り生写真　　　　　　　　　　　　　　　　　　　　　　　　　　　　",
      description: "",
      image: PRODUCT2,
      quantity: 10,
    },
    {
      id: 3,
      name: "キューイチローグッズ　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　",
      description: "",
      image: PRODUCT3,
      quantity: 10,
    },
  ],
  seasonTwo: [
    {
      id: 1,
      name: "11月3日イベント（「松竹芸能プレゼンツおかだ寄席」）参加券",
      description: "",
      image: PRODUCTB1,
      quantity: 5,
    },
    {
      id: 2,
      name: "アナウンサー直筆サイン入り「文化放送カレンダー2022」　　",
      description: "※サインするアナウンサーの指定は出来ません",
      image: PRODUCTB2,
      quantity: 5,
    },
    {
      id: 3,
      name: "文化放送専用FMバッジ（ラジオ）　　　　　　　　　　　　　　　　　",
      description: "",
      image: PRODUCTB3,
      quantity: 10,
    },
  ],
  seasonThree: [
    {
      id: 1,
      name: "【浜祭体験型グッズ】\n元応援団・坂口愛美があなたを全力でで応援します",
      description:
        "商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。",
      image: PRODUCT,
    },
    {
      id: 2,
      name: "【浜祭体験型グッズ】\n元応援団・坂口愛美があなたを全力でで応援します",
      description:
        "商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。",
      image: PRODUCT,
    },
    {
      id: 3,
      name: "【浜祭体験型グッズ】\n元応援団・坂口愛美があなたを全力でで応援します",
      description:
        "商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。",
      image: PRODUCT,
    },
  ],
  seasonFour: [
    {
      id: 1,
      name: "【浜祭体験型グッズ】\n元応援団・坂口愛美があなたを全力でで応援します",
      description:
        "商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。",
      image: PRODUCT,
    },
    {
      id: 2,
      name: "【浜祭体験型グッズ】\n元応援団・坂口愛美があなたを全力でで応援します",
      description:
        "商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。",
      image: PRODUCT,
    },
    {
      id: 3,
      name: "【浜祭体験型グッズ】\n元応援団・坂口愛美があなたを全力でで応援します",
      description:
        "商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。商品節女分が入ります。",
      image: PRODUCT,
    },
  ],
};
export const TITLEINFO = {
  week1: {
    title: "第 1 弾　10月1日～10月7日のプレゼントはこちら！",
    notification: "第一弾",
    products: [
      "A賞：11月3日イベント（「『ヴァイナル ・ミュージック～歌謡曲2.0 ～』浜祭スペシャルライブ）参加券×5名様",
      "B賞：アナウンサー直筆サイン入り生写真×10名様",
      "C賞：キューイチローグッズ×10名様",
    ],
  },
  week2: {
    title: "第 2 弾　10月8日～10月14日のプレゼントはこちら！",
    notification: "第二弾",
    products: [
      "A賞：11月3日イベント（「松竹芸能プレゼンツおかだ寄席」）参加券×5名様",
      "B賞：アナウンサー直筆サイン入り「文化放送カレンダー2022」×5名様　　　※サインするアナウンサーの指定は出来ません",
      "C賞：文化放送専用FMバッジ（ラジオ）×10名様",
    ],
  },
  week3: {
    title: "第 3 弾　10月15日～10月21日のプレゼントはこちら！",
    notification: "第三弾",
    products: [
      "A賞：11月3日イベント（「松竹芸能プレゼンツおかだ寄席」）参加券×5名様",
      "B賞：アナウンサー直筆サイン入り「文化放送カレンダー2022」×5名様　　　※サインするアナウンサーの指定は出来ません",
      "C賞：文化放送専用FMバッジ（ラジオ）×10名様",
    ],
  },
  week4: {
    title: "第 4 弾　10月22日～10月28日のプレゼントはこちら！",
    notification: "第四弾",
    products: [
      "A賞：11月3日イベント（「松竹芸能プレゼンツおかだ寄席」）参加券×5名様",
      "B賞：アナウンサー直筆サイン入り「文化放送カレンダー2022」×5名様　　　※サインするアナウンサーの指定は出来ません",
      "C賞：文化放送専用FMバッジ（ラジオ）×10名様",
    ],
  },
};

export default PRODUCTSINFO.seasonTwo;
