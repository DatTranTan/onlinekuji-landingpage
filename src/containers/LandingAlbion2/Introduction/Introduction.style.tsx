import styled from 'styled-components'

const WrapperIntroduction = styled.div`
    margin: auto;
    color: #333;
`
const ImgTopLeft = styled.img`
  width: 215px;
  height: 220px;
  padding-right: 20px;
  padding-bottom: 20px;
  /* width: 150px;
  height: 150px; */
  /* @media (max-width: 800px) {
    padding-right: 20px;
    padding-bottom: 20px;
  } */
  @media (max-width: 500px) {
    width: 100px;
    height: 101px;
    padding-right: 8px;
    padding-bottom: 4px;
  }
`
const BlockTitle = styled.div`
  color: #EA5514;
  font-size: 2.5rem;
  font-weight: 900;
  text-align: center;
  margin: 30px auto;
  margin-top: 0px;
  @media (max-width: 1300px) {
        font-size: 2rem;
      }
      @media (max-width: 1110px) {
        font-size: 1.5rem;
      } 
      @media (max-width: 500px) {
        font-size: 1.5rem;
      } 
`
const ContainerTitle = styled.div`
  font-size: 1.5rem;
  font-weight: 900;
  margin: 10px auto;
  padding-bottom: 10px;
  border-bottom: 3px solid #D3D3D3;
      @media (max-width: 500px) {
        font-size: 1.2rem;
      } 
`
const ContainerContent = styled.div`
  font-size: 1rem;
  margin-bottom: 2rem;
  @media (max-width: 500px) {
        font-size: 0.9rem;
      } 
`
const LineHight = styled.img`
  width: 32rem;
  @media (max-width: 500px) {
    width: 18rem;
  }
`
const Container = styled.div`
  width: 80%;
  background-color: white;
  margin: auto;
  border-radius: 20px;
  padding:60px;
  margin-top: 120px;
  @media (max-width: 1100px) {
    margin-top: 60px;
    width: 90%;
  }
  @media (max-width: 500px) {
    width: 100%;
    border-radius: 0px;
    margin-top: 40px;
    margin-bottom: 0px;
    padding: 20px;
  }
`
const BlockIntroduction = styled.div`
  width: 100%;
  padding-top: 4rem;
  padding-bottom: 4vw;
  display: flex;
  justify-content: space-around;
  padding-left: 12vw;
  padding-right: 12vw;
  @media (max-width: 800px) {
    flex-flow: column;
    align-items: center;
  }
  @media (max-width: 500px) {
    padding-left: 5vw;
    padding-right: 5vw;
  }
`
const BlockIntroductionMid = styled.div`
  width: 100%;
  padding-top: 4rem;
  padding-bottom: 4vw;
  display: flex;
  justify-content: space-around;
  flex-flow: row-reverse;
  padding-left: 12vw;
  padding-right: 12vw;
  @media (max-width: 800px) {
    flex-flow: column;
    /* justify-content: center; */
    align-items: center;
  }
  @media (max-width: 500px) {
    padding-left: 5vw;
    padding-right: 5vw;
  }
`

const ContainerLeft = styled.div`
  width: 17vw;
  height: 17vw;
  background-color: #74daf7;
  border-radius: 23px;
  padding: 1.5rem;
  @media (max-width: 800px) {
    width: 12rem;
    height: 12rem;
  }
`
const ImgGift = styled.img``
const LineGift = styled.img`
  height: 17vw;
  @media (max-width: 800px) {
    display: none;
  }
`
const LineRow = styled.img`
  @media (min-width: 800px) {
    display: none;
  }
  @media (max-width: 800px) {
    width: 15rem;
    margin-top: 1rem;
    margin-bottom: 1rem;
  }
`
const ContainerRight = styled.div`
  @media (max-width: 800px) {
    width: 100%;
  }
`
const ContainerStep = styled.div`
  font-size: 2.5rem;
  font-weight: 600;
  color: #74daf7;
  margin-left: 1.5rem;
  @media (max-width: 1120px) {
    font-size: 1.5rem;
  }
  @media (max-width: 800px) {
    font-size: 2rem;
  }
`
const Content = styled.div`
  width: 38vw;
  height: 11vw;
  background-color: rgb(255 204 90 / 35%);
  padding: 1.5rem;
  @media (max-width: 1120px) and (max-height: 420px) {
    padding: 0.8rem;
  }
  span {
    opacity: 1;
    font-size: 2rem;
    color: #707070;
    @media (max-width: 1600px) {
      font-size: 1.5rem;
    }
    @media (max-width: 1400px) {
      font-size: 1.2rem;
    }
    @media (max-width: 1120px) {
      font-size: 1rem;
    }
    @media (max-width: 1120px) and (max-height: 420px) {
      font-size: 0.8rem;
    }
    @media (max-width: 800px) {
      font-size: 1.5rem;
    }
    @media (max-width: 800px) and (max-height: 400px) {
      font-size: 1.2rem;
    }
    @media (max-width: 500px) {
      font-size: 1rem;
    }
  }
  @media (max-width: 800px) {
    width: 100%;
    height: 12rem;
  }
  @media (max-width: 500px) {
    height: 10rem;
  }
`
const ContentLast = styled.div`
  width: 38vw;
  height: 13vw;
  background-color: rgb(255 204 90 / 35%);
  padding: 1.5rem;
  @media (max-width: 1120px) and (max-height: 420px) {
    padding: 0.8rem;
  }
  span {
    opacity: 1;
    font-size: 2rem;
    color: #707070;
    @media (max-width: 1600px) {
      font-size: 1.5rem;
    }
    @media (max-width: 1400px) {
      font-size: 1.2rem;
    }
    @media (max-width: 1120px) {
      font-size: 1rem;
    }

    @media (max-width: 1120px) and (max-height: 420px) {
      font-size: 0.8rem;
    }
    @media (max-width: 800px) {
      font-size: 1.5rem;
    }
    @media (max-width: 800px) and (max-height: 400px) {
      font-size: 1.2rem;
    }
    @media (max-width: 500px) {
      font-size: 0.9rem;
    }
  }
  @media (max-width: 800px) {
    width: 100%;
    height: 12rem;
  }
  @media (max-width: 500px) {
    height: 10rem;
    margin-bottom: 2rem;
  }
`
const ImgBotRight = styled.img`
  @media (max-width: 800px) {
    width: 150px;
    height: 150px;
  }
  @media (max-width: 500px) {
    width: 100px;
    height: 100px;
  }
`
const DivBottomLine = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  padding-right: 3.4rem;
  margin-top: -16.8rem;
  @media (max-width: 800px) {
    padding-right: 3.5rem;
    margin-top: -9.3rem;
  }
  @media (max-width: 500px) {
    /* padding-right: 2rem;
    margin-top: -9.3rem; */
    padding-right: 1rem;
    margin-top: -6.2rem;
  }
`

export {
  WrapperIntroduction,
  ImgTopLeft,
  BlockTitle,
  ContainerTitle,
  LineHight,
  Container,
  BlockIntroduction,
  ContainerLeft,
  ImgGift,
  LineGift,
  ContainerRight,
  ContainerStep,
  Content,
  ImgBotRight,
  DivBottomLine,
  LineRow,
  BlockIntroductionMid,
  ContentLast,
  ContainerContent
}
