import { styled } from 'baseui'

export const LayoutWrapper = styled('div', () => ({
  width: '100%',
  display: 'flex',
  alignItems: 'flex-start'
}))

export const ContentWrapper = styled('div', ({ $theme }) => ({
  width: '100%',
  height: 'auto',
  display: 'flex',
  backgroundColor: $theme.colors.backgroundF7,
  overflow: 'hidden'
}))

export const ContentInnerWrapper = styled('div', ({ $theme }) => ({
  // width: '100%',
  // height: 'auto',
  padding: '1.5rem',
  marginBottom: '64px',
  display: 'flex',
  flexDirection: 'column',
  backgroundColor: $theme.colors.backgroundF7,

  '@media only screen and (max-width: 990px)': {
    padding: '1rem'
  },

  '@media only screen and (max-width: 767px)': {
    padding: '0.5rem'
  }
}))
