import React from 'react'
import useComponentSize from '../../settings/useComponentSize'
import Sidebar from './Sidebar/Sidebar'
import Topbar from './Topbar/Topbar'
import DrawerItems from '../DrawerItems/DrawerItems'
import { DrawerProvider } from '../../context/DrawerContext'
import {
  LayoutWrapper,
  ContentWrapper,
  ContentInnerWrapper
} from './Layout.style'
import { useDeviceType } from '../../settings/useDeviceType'
import { styled } from 'baseui'
import { SelectProvider } from '../../context/SelectContext'
import { EventProvider } from '../../context/EventContext'
import { Scrollbars } from 'react-custom-scrollbars'

const SidedbarDesktop = styled('div', () => ({
  '@media only screen and (max-width: 1199px)': {
    display: 'none'
  }
}))

const AdminLayout = ({ children }: any) => {
  let [topbarRef, { height }] = useComponentSize()
  let [sidebarRef, { width }] = useComponentSize()
  const { desktop } = useDeviceType()

  return (
    <DrawerProvider>
      <SelectProvider>
        <EventProvider>
          <Topbar refs={topbarRef} />
          <LayoutWrapper
            style={{
              height: `calc(100vh - ${height}px)`,
              overflow: 'hidden'
            }}
          >
            {desktop ? (
              <>
                <SidedbarDesktop>
                  <Sidebar
                    refs={sidebarRef}
                    style={{
                      height: `calc(100vh - ${height}px)`
                    }}
                  />
                </SidedbarDesktop>
                <Scrollbars
                  autoHide
                  renderView={(props) => (
                    <ContentInnerWrapper
                    className="scroll-container-class"
                      style={{
                        ...props.style,
                        overflowX: 'hidden',
                      }}
                    />
                  )}
                  renderTrackHorizontal={(props) => (
                    <div
                      {...props}
                      style={{ display: 'none' }}
                      className='track-horizontal'
                    />
                  )}
                >
                  {children}
                </Scrollbars>
              </>
            ) : (
              <ContentWrapper
                style={{
                  width: '100%'
                }}
              >
                <h3>
                  width: {width} , height: {height}
                </h3>
                <ContentInnerWrapper>{children}</ContentInnerWrapper>
              </ContentWrapper>
            )}
          </LayoutWrapper>
          <DrawerItems />
        </EventProvider>
      </SelectProvider>
    </DrawerProvider>
  )
}

export default AdminLayout
