import { styled, withStyle } from 'baseui'
import { StyledBodyCell as BaseStyledCell } from 'baseui/table-grid'
import { StyledRoot } from 'baseui/list'
import { Col } from '../../components/FlexBox/FlexBox'
import { Modal } from 'antd'

export const Header = styled('header', () => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  flexDirection: 'row',
  flexWrap: 'wrap',
  backgroundColor: '#FFF',
  padding: '0 1rem',
  marginBottom: '1.5rem',
  minHeight: '70px',
  boxShadow: '0 0 8px rgba(0, 0 ,0, 0.1)',

  // '@media only screen and (max-width: 1024px)': {
  //   padding: '0 0.5rem'
  // },

  '@media only screen and (max-width: 990px)': {
    marginBottom: '1rem'
  },

  '@media only screen and (max-width: 767px)': {
    marginBottom: '0.5rem'
  }
}))
export const Heading = styled('a', ({ $theme }) => ({
  ...$theme.typography.fontBold18,
  color: $theme.colors.textDark,
  margin: 0
}))

export const ImageWrapper = styled('div', ({ $theme }) => ({
  width: '40px',
  height: '40px',
  overflow: 'hidden',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: '20px',
  backgroundColor: $theme.colors.backgroundF7,
  boxShadow: '0 0 5px rgba(0, 0 , 0, 0.05)'
}))

export const Icon = styled('span', () => ({
  width: '100%',
  height: 'auto'
}))

export const TableWrapper = styled('div', () => ({
  width: '100%'
}))

export const StyledCell = withStyle(BaseStyledCell, () => ({
  fontFamily: "'Lato', sans-serif",
  fontWeight: 400,
  color: '#161F6A !important',
  display: 'flex',
  alignSelf: 'center',
  justifyContent: 'flex-start',
  alignItems: 'center',
  wordWrap: 'break-word',
  padding: '0 10px',
  borderTop: '0.3px solid rgba(224, 224, 224, 0.6)',
  height: '62px',

  '@media only screen and (max-width: 1440px)': {
    height: '56px'
  },

  '@media only screen and (max-width: 1280px)': {
    height: '45px'
  },

  '@media only screen and (max-width: 1024px)': {
    height: '40px'
  },

  '@media only screen and (max-width: 767px)': {
    height: '50px'
  }
}))

export const StyledListItem = styled(StyledRoot, () => ({
  margin: 0,
  padding: '0 10px',
  borderBottom: '1px solid #ddd',
  alignContent: 'center',
  justifyContent: 'center',
  height: '10%',

  ':hover': {
    backgroundColor: 'rgb(246, 246, 246)'
  }
}))
export const StyledHead = styled('thead', () => ({
  boxShadow: 'rgba(0, 0, 0, 0.16) 0px 1px 4px'
}))
export const ToolWrapper = styled('div', () => ({
  justifyContent: 'flex-end',
  display: 'flex',
  flexDirection: 'row',
  marginBottom: '1rem',
  overflow: 'hidden'
}))

export const TitleCol = styled(Col, () => ({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',

  '@media only screen and (max-width: 767px)': {
    marginBottom: '10px',

    ':last-child': {
      marginBottom: 0
    }
  },
  height: '100%'
}))

export const ActionCol = styled(Col, () => ({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-end',
  alignItems: 'center',

  '@media only screen and (max-width: 767px)': {
    marginBottom: '10px',

    ':last-child': {
      marginBottom: 0
    }
  },

  height: '100%'
}))

export const ActionWrapper = styled('div', () => ({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-end',
  // marginRight: '1rem',
  width: '85%'
}))
export const SearchWrapper = styled('div', () => ({
  paddingRight: '1rem',
  width: '34%',
  // alignItems: 'center',

  '@media only screen and (max-width: 1024px)': {
    paddingRight: '0.5rem'
  }
}))

export const SelectWrapper = styled('div', () => ({
  width: '22%',
  paddingRight: '1rem',

  // ':last-child': {
  //   paddingRight: 0
  // },

  '@media only screen and (max-width: 1024px)': {
    paddingRight: '0.5rem'
  }
}))

export const ButtonWrapper = styled('div', ({ $theme, $isFocused }) => ({
  display: 'flex',
  justifyContent: 'flex-start',
  alignItems: 'center',
  width: '15%',
  paddingLeft: '1rem',

  '@media only screen and (max-width: 1024px)': {
    paddingLeft: '0.5rem'
  },
  '@media only screen and (min-width: 1600px)': {
    width: '10%'
  }
}))

export const CustomModal = styled(Modal, () => ({
  minWidth: '80vw',
  minHeight: '50vh',

  '@media only screen and (max-width: 1024px)': {
    minWidth: '90vw'
  }
}))

export const ModalTitle = styled('h2', () => ({
  color: 'rgb(0, 197, 141)'
}))
