import { styled } from 'baseui'

export const Wrapper = styled('div', () => ({
  background: 'white',
  // background: '#f7f7f7',
  padding: '1rem'
}))

export const Grid = styled('div', () => ({
  width: '100%',
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',

  '@media only screen and (max-width: 1024px)': {
    flexDirection: 'column'
  }
}))

export const Row = styled('div', () => ({
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  width: '50%',

  ':first-child': {
    paddingRight: '0.5rem'
  },

  ':last-child': {
    paddingLeft: '0.5rem'
  },

  '@media only screen and (max-width: 1024px)': {
    width: '100%',

    ':first-child': {
      marginBottom: '1rem',
      paddingRight: 0
    },

    ':last-child': {
      paddingLeft: 0
    }
  },

  '@media only screen and (max-width: 500px)': {
    flexDirection: 'column'
  }
}))

export const Col = styled('div', () => ({
  width: '50%',
  padding: '0 0.5rem',

  ':first-child': {
    paddingLeft: 0
  },

  ':last-child': {
    paddingRight: 0
  },

  '@media only screen and (max-width: 500px)': {
    width: '100%',
    padding: 0,
    paddingBottom: '1rem',

    ':last-child': {
      paddingBottom: 0
    }
  }
}))

export const StatHeader = styled('div', () => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'row',
  backgroundColor: '#FFF',
  boxShadow: '0 0 8px rgba(0, 0 ,0, 0.1)',
  padding: '1rem 0.5rem',
  marginBottom: '1rem',
  flexWrap: 'wrap'
}))

export const StatSelectWrapper = styled('div', () => ({
  width: '25%',
  padding: '0 0.5rem',

  '@media only screen and (max-width: 1024px)': {
    width: '50%',
    padding: '0.5rem'
  }
}))

export const StastisticWrapper = styled('div', ({ $theme, $isFocused }) => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'flex-end',
  width: '100%',
  padding: '0 1rem 1.5rem 1rem'
}))

export const Label = styled('label', ({ $theme }) => ({
  ...$theme.typography.fontBold14,
  marginBottom: '10px',
  color: $theme.colors.textDark
}))
