/* eslint-disable */
import { Select } from 'antd'
import React, { memo, useRef, useState } from 'react'
import confirm from 'antd/lib/modal/confirm'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { useMutation } from '@apollo/react-hooks'
import { MUTATION_UPDATE_CUSTOMER } from '../../graphql/mutation/update-customer'
import { Notification } from '../../components/Notification/NotificationCustom'
import { MUTATION_CHANGE_PRIZE_STATUS } from '../../graphql/mutation/change-status'

interface Item {
  key: string
  id: string
  name: string
  prizeId: string
  tel: string
  email: string
  postalCode: string
  address: string
  age: number
  prizeStatus: string
  gender: string
  customerLost: any
}

export interface EditableCellProps {
  title: React.ReactNode
  editable: boolean
  children?: React.ReactNode
  dataIndex: keyof Item
  record: Item
}

const StatusOptions = [
  { value: 'won', label: '当選者' },
  { value: 'preparing', label: '賞品準備中' },
  { value: 'sent', label: '賞品送付済み' }
]

const EditableCell: React.FC<EditableCellProps> = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  ...restProps
}) => {
  const inputRef = useRef(null)
  const [selectValue, setSelectValue] = useState(record && record.prizeStatus)
  const [updateStatus] = useMutation(MUTATION_CHANGE_PRIZE_STATUS, {
    fetchPolicy: 'no-cache'
  })
  let childNode = children

  const onSelectChange = (index: string) => {
    updateStatus({
      variables: {
        customerId: record?.id,
        prizeStatus: index
      }
    })
      .then((data) => {
        setSelectValue(data.data?.updatePrizeStatus.prizeStatus)
        Notification({
          type: 'success',
          message: '成功',
          description: '状態の更新に成功しました。'
        })
      })
      .catch(({ graphQLErrors }) => {
        Notification({
          type: 'error',
          message: ' エラー',
          description: graphQLErrors[0].message
        })
      })
  }

  if (editable) {
    childNode = (
      <Select
        ref={inputRef}
        options={StatusOptions}
        onChange={(value) => {
          confirm({
            type: 'warning',
            title: '確認',
            content: '当選者の状態を変更してもよろしいですか？',
            cancelText: 'キャンセル',
            icon: <ExclamationCircleOutlined />,
            okText: 'OK',
            onOk: () => onSelectChange(value)
          })
        }}
        placeholder='ユーザ種別の絞り込み'
        value={selectValue}
        style={{
          minWidth: '100%',
          height: '28px'
        }}
      />
    )
  }

  return <td {...restProps}>{childNode}</td>
}
export default memo(EditableCell)
