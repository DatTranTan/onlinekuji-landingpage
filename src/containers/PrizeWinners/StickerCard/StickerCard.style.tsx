import { styled } from 'baseui'

export const Card = styled('div', () => ({
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  padding: '1.5rem',
  borderRadius: '6px',
  backgroundColor: '#ffffff',
  border: '2px solid #DDD',

  '@media only screen and (max-width: 1280px)': {
    padding: '1rem'
  },

  '@media only screen and (max-width: 990px)': {
    padding: '1.5rem'
  }
}))

export const TopInfo = styled('div', () => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between'
  // minHeight: '80px',
  // alignItems: 'center'
}))

export const TitleWrapper = styled('div', () => ({
  width: 'calc(100% - 60px)',
  display: 'flex',
  flexDirection: 'column',
  height: '128px',
  // alignItems: 'center'

  '@media only screen and (max-width: 1280px)': {
    width: 'calc(100% - 40px)'
  },

  '@media only screen and (max-width: 990px)': {
    width: 'calc(100% - 100px)'
  }
}))

export const Title = styled('span', ({ $theme }) => ({
  ...$theme.typography.fontBold18,
  fontFamily: $theme.typography.primaryFontFamily,
  lineHeight: '1.5',
  color: $theme.colors.textDark,
  marginBottom: '10px',

  '@media only screen and (max-width: 1280px)': {
    ...$theme.typography.fontBold16
  },

  '@media only screen and (max-width: 990px)': {
    ...$theme.typography.fontBold21
  }
}))

export const SubTitle = styled('span', ({ $theme }) => ({
  ...$theme.typography.fontBold13,
  fontFamily: $theme.typography.primaryFontFamily,
  lineHeight: '1.2',
  color: $theme.colors.textNormal,
  marginBottom: '0px'
}))

export const IconBox = styled('div', ({ $theme }) => ({
  width: '4.5rem',
  height: '4.4rem',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  '@media only screen and (max-width: 1280px)': {
    width: '3.8rem',
    height: '3.8rem'
  },

  '@media only screen and (max-width: 990px)': {
    width: '4rem',
    height: '4rem'
  }
}))

export const Price = styled('span', ({ $theme }) => ({
  ...$theme.typography.fontBold21,
  fontFamily: $theme.typography.primaryFontFamily,
  lineHeight: '1.2',
  color: $theme.colors.textDark,
  marginBottom: '10px'
}))

export const Text = styled('span', ({ $theme }) => ({
  ...$theme.typography.fontBold14,
  fontFamily: $theme.typography.primaryFontFamily,
  lineHeight: '1.2',
  marginBottom: '50px'
}))

export const Note = styled('span', ({ $theme }) => ({
  ...$theme.typography.font14,
  fontFamily: $theme.typography.primaryFontFamily,
  color: $theme.colors.textNormal,
  lineHeight: '1.2'
}))

export const Link = styled('a', ({ $theme }) => ({
  ...$theme.typography.fontBold13,
  fontFamily: $theme.typography.primaryFontFamily,
  color: $theme.colors.purple400,
  lineHeight: '1.2',
  textDecoration: 'none'
}))
