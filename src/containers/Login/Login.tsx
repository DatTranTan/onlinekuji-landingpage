import React, { useContext } from 'react'
import { Redirect, useHistory, useLocation } from 'react-router-dom'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import { AuthContext } from '../../context/auth'
import { use100vh } from 'react-div-100vh'
import {
  FormFields,
  FormFieldsLogin,
  FormLabel,
  // FormTitle,
  Error
} from '../../components/FormFields/FormFields'
import {
  Wrapper,
  FormWrapper,
  // LogoImage,
  LogoWrapper,
  LogoText,
  PrimaryText
} from './Login.style'
import Input from '../../components/Input/InputLogin'
import Button from '../../components/Button/Button'
import { EVENT, PRIZE_WINNERS, USERS } from '../../settings/constants'

const initialValues = {
  username: '',
  password: ''
}

const getLoginValidationSchema = () => {
  return Yup.object().shape({
    username: Yup.string().required('このフィールドが必須な項目です。'),
    password: Yup.string().required('このフィールドが必須な項目です。')
  })
}

const MyInput = ({ field, form, ...props }) => {
  return <Input {...field} {...props} />
}

export default () => {
  let history = useHistory()
  let location = useLocation()

  const { authenticate, isAuthenticated } = useContext(AuthContext)
  const [isLoading, setIsLoading] = React.useState(false)

  const height = use100vh()

  if (isAuthenticated)
    return (
      <Redirect
        to={{
          pathname: localStorage.getItem('role') !== 'client' ? USERS : EVENT
        }}
      />
    )

  let { from } = (location.state as any) || {
    from: { pathname: PRIZE_WINNERS }
  }
  let login = ({ username, password }) => {
    setIsLoading(true)
    authenticate({ username, password }, (isSuccess: boolean) => {
      isSuccess &&
        history.replace(localStorage.getItem('role') !== 'client' ? from : '/')
      setIsLoading(false)
    })
  }

  return (
    <Wrapper style={{ height: height }}>
      <FormWrapper>
        <Formik
          initialValues={initialValues}
          onSubmit={login}
          render={({ errors, status, touched }) => (
            <Form>
              <FormFields>
                <LogoWrapper>
                  {/* <LogoImage src={Logoimage} alt='admin' /> */}
                  <LogoText>
                    抽選管理システム <PrimaryText>管理者ページ</PrimaryText>
                  </LogoText>
                </LogoWrapper>
                {/* <FormTitle>Log in to admin</FormTitle> */}
              </FormFields>

              <FormFieldsLogin>
                <FormLabel>ユーザーID</FormLabel>
                <Field
                  type='text'
                  name='username'
                  component={MyInput}
                  placeholder='ユーザーIDを入力してください。'
                />
                {errors.username && touched.username && (
                  <Error>{errors.username}</Error>
                )}
              </FormFieldsLogin>
              <FormFieldsLogin>
                <FormLabel>パスワード</FormLabel>
                <Field
                  type='password'
                  name='password'
                  component={MyInput}
                  placeholder='パスワードを入力してください。'
                />
                {errors.password && touched.password && (
                  <Error>{errors.password}</Error>
                )}
              </FormFieldsLogin>
              <Button
                type='submit'
                //disabled={isLoading}
                isLoading={isLoading}
                overrides={{
                  BaseButton: {
                    style: ({ $theme }) => ({
                      width: '100%',
                      marginLeft: 'auto',
                      marginTop: '1rem',
                      borderTopLeftRadius: '3px',
                      borderTopRightRadius: '3px',
                      borderBottomLeftRadius: '3px',
                      borderBottomRightRadius: '3px'
                    })
                  }
                }}
              >
                ログインする
              </Button>
            </Form>
          )}
          validationSchema={getLoginValidationSchema}
        />
      </FormWrapper>
    </Wrapper>
  )
}
