import { useCreateContext } from './create-context'

const initialState = {
  SelectedPrize: {},
  isReset: false,
  prizeIndex: null
}
type State = typeof initialState
type Action = any
function reducer(state: State, action: Action) {
  switch (action.type) {
    case 'EDIT_PRIZE':
      return {
        ...state,
        SelectedPrize: action.prize,
        isReset: false
      }
    case 'RESET_PRIZE_FIELDS':
      return {
        ...state,
        isReset: action.isReset,
        SelectedPrize: {}
      }
    case 'DELETE_PRIZE':
      return {
        ...state,
        prizeIndex: action.prizeIndex
      }
    default:
      return state
  }
}
const [useEventState, useEventDispatch, EventProvider] = useCreateContext(
  initialState,
  reducer
)

export { useEventState, useEventDispatch, EventProvider }
