import { useCreateContext } from './create-context'

const initialState = {
  isOpen: false
}
type State = typeof initialState
type Action = any
function reducer(state: State, action: Action) {
  switch (action.type) {
    case 'OPEN_SELECT':
      return {
        ...state,
        isOpen: true
      }
    case 'CLOSE_SELECT':
      return {
        ...state,
        isOpen: false
      }
    default:
      return state
  }
}
const [useSelectState, useSelectDispatch, SelectProvider] = useCreateContext(
  initialState,
  reducer
)

export { useSelectState, useSelectDispatch, SelectProvider }
