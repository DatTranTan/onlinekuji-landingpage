import AWS from 'aws-sdk'
import {
  PutObjectRequest,
  HeadObjectRequest,
  DeleteObjectRequest,
  ListObjectsRequest,
  DeleteObjectsRequest
} from 'aws-sdk/clients/s3'

var albumBucketName = process.env.REACT_APP_BUCKET_NAME
// Initialize the Amazon Cognito credentials provider
AWS.config.region = 'ap-northeast-1' // Region
// AWS.config.credentials = new AWS.CognitoIdentityCredentials({
//   IdentityPoolId: process.env.REACT_APP_POOL_ID
// })

AWS.config.accessKeyId = process.env.REACT_APP_ACCESS_KEY
AWS.config.secretAccessKey = process.env.REACT_APP_SECRET_KEY

var s3 = new AWS.S3({
  apiVersion: process.env.REACT_APP_API_VER,
  // region: 'ap-northeast-1',
  params: { Bucket: albumBucketName }
})

export function createAlbum(albumName) {
  albumName = albumName.trim()

  if (!albumName) {
    return alert('Album names must contain at least one non-space character.')
  }
  if (albumName.indexOf('/') !== -1) {
    return alert('Album names cannot contain slashes.')
  }
  var albumKey = encodeURIComponent(albumName)
  s3.headObject({ Key: albumKey } as HeadObjectRequest, function (err, data) {
    if (!err) {
      return alert('Album already exists.')
    }
    if (err.code !== 'NotFound') {
      return alert('There was an error creating your album: ' + err.message)
    }
    s3.putObject({ Key: albumKey } as PutObjectRequest, function (err, data) {
      if (err) {
        return alert('There was an error creating your album: ' + err.message)
      }
      alert('Successfully created album.')
    })
  })
}

export default function addPhoto(
  albumName: string,
  folderName: string,
  files: any,
  callback: any
) {
  if (!files.length) {
    return alert('Please choose a file to upload first.')
  }
  var file = files[0]
  var fileName = file.name
  var albumPhotosKey = encodeURIComponent(albumName) + '/'

  var photoKey = folderName + albumPhotosKey + fileName

  var upload = new AWS.S3.ManagedUpload({
    params: {
      Bucket: albumBucketName,
      Key: photoKey,
      Body: file,
      ACL: 'public-read'
    }
  })
  upload.on('httpUploadProgress', (progress) => {
    if (progress.total >= 1022700) {
      callback.onLoad(
        (progress.loaded / 1048576).toFixed(2),
        (progress.total / 1048576).toFixed(2),
        'MB'
      )
    } else {
      callback.onLoad(
        Math.round(progress.loaded / 1024),
        Math.round(progress.total / 1024),
        'KB'
      )
    }
  })
  var promise = upload.promise()
  promise.then(
    function (data) {
      // alert("Successfully uploaded photo.");
      const urlImage = encodeURI(
        'https://' +
          albumBucketName +
          '.s3-ap-northeast-1.amazonaws.com/' +
          folderName +
          albumPhotosKey +
          fileName
      )
      callback.onSuccess(urlImage)
    },
    function (err) {
      alert('There was an error uploading your photo: ' + err.message)
      callback.onError('')
    }
  )
}

export function deletePhoto(photoKey) {
  s3.deleteObject(
    { Key: photoKey } as DeleteObjectRequest,
    function (err, data) {
      if (err) {
        return alert('There was an error deleting your photo: ' + err.message)
      }
      alert('Successfully deleted photo.')
    }
  )
}

export function deleteAlbum(albumName) {
  var albumKey = encodeURIComponent(albumName) + '/'
  s3.listObjects(
    { Prefix: albumKey } as ListObjectsRequest,
    function (err, data) {
      if (err) {
        return alert('There was an error deleting your album: ' + err.message)
      }
      var objects = data.Contents.map(function (object) {
        return { Key: object.Key }
      })
      s3.deleteObjects(
        {
          Delete: { Objects: objects, Quiet: true }
        } as DeleteObjectsRequest,
        function (err, data) {
          if (err) {
            return alert(
              'There was an error deleting your album: ' + err.message
            )
          }
          alert('Successfully deleted album.')
        }
      )
    }
  )
}
