import React from "react";
import PropTypes from "prop-types";
import {  WrapperPrizeSpin, 
} from "./PrizeAlbion.style";
import ImageNull from "../../assets/images/box2.png";
import { Card } from "antd";
import { Row, Col } from "antd";

const { Meta } = Card;

function PrizeAlbion(props: any) {
  // const {productInfo} = props
  // const {id, image, description, name} = PRODUCTSINFO[2productInfo<=2?productInfo:2]

  return (
    <>
      {/* <Card
    hoverable
    style={{  }}
    cover={
        <img
          // id='imageid'
          src={props.imagePrize ? props.imagePrize : ImageNull}
          alt='imagePrize'
          style={{
            width: '100%',
            height: '250px',
            objectFit: 'contain'
          }}
        />
    }
  >
    <Meta 
    title={
    <h2 className="header-text" style={{color: 'black', fontWeight: 900, textAlign: 'left'}}>
      {props.namePrize}
    </h2>} 
    style={{color: 'black', textAlign: 'left'}}
    description="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt place"
    />
  </Card> */}
      <Row gutter={24} justify="space-around" style={{ width: "100%", marginLeft:0,marginRight:0 }}>
        <Col xs={21} sm={24} lg={8} xl={9}>
          <WrapperPrizeSpin>
            <Card
              // hoverable
              style={{ border: "none"}}
              cover={
                <img
                  // id='imageid'
                  className="card-image-spin"
                  src={props.imagePrize ? props.imagePrize : ImageNull}
                  alt="imagePrize"
                  // style={{
                  //   width: '100%',
                  //   objectFit: 'contain'
                  // }}
                />
              }
            >
              <Meta
                title={
                  <h2
                    className="header-text"
                    style={{
                      color: "black",
                      fontWeight: 900,
                      textAlign: "left",
                    }}
                  >
                    {props.namePrize}
                  </h2>
                }
                style={{ color: "black", textAlign: "left" }}
                // description={description}
              />
            </Card>
          </WrapperPrizeSpin>
        </Col>
      </Row>
    </>
  );
}

PrizeAlbion.propTypes = {
  namePrize: PropTypes.string,
  numberPrize: PropTypes.string,
  imagePrize: PropTypes.string,
  productInfo: PropTypes.any,
};

export default PrizeAlbion;
