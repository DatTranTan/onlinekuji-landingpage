import styled from 'styled-components'
import { Popover } from "antd";

export const VideoURL = styled.a`
  @media (max-width: 1024px) {
    width: 100%;
    overflow : hidden;
    textOverflow: ellipsis;
    whiteSpace: nowrap;
    display: block;
  }
` 

export const CustomPopover = styled(Popover)`
  .title{
    color: red;
  }
  .ant-popover-inner-content{
    max-height: 500px;
    overflow-y: scroll;
    background-color: red;
  }
`
export const PopoverContainer = styled.div`
div{
  color: red;
}
  .title{
    color: red;
  }
`
