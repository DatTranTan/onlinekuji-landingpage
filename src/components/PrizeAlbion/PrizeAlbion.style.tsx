import styled from 'styled-components'
import {
  isDesktop
} from "react-device-detect";

const WrapperPrize = styled.div`
  max-width: 28rem;
  width: 28%;
  height: 40rem;
  border: 1px solid #707070;
  border-radius: 37px;
  background-color: #fff;
  display: flex;
  justify-content: space-around;
  align-items: center;
  flex-flow: column;
  margin-top: 5%;
  margin-left: 3%;
  margin-right: 3%;
  /* margin-bottom: 5%; */
  box-shadow: 9px 10px rgb(121 103 103 / 23%);
  @media (max-width: 1650px) {
    margin-left: 2rem;
    margin-right: 2rem;
    margin-top: 4.5rem;
    height: 34rem;
    /* margin-bottom: 4.5rem; */
  }

  @media (max-width: 1300px) {
    margin-top: 3rem;
    width: 26%;
    height: 27rem;
    /* margin-bottom: 3rem; */
  }
  @media (max-width: 1110px) {
    height: 24rem;
    }
    @media (max-width: 950px) {
      height: ${isDesktop?'20rem':'22rem'}
    }
  @media (max-width: 850px) {
    width: 40%;
    height: 27rem;
  }
  @media (max-width: 500px) {
    width: 45%;
    margin-left: 0;
    margin-right: 0;
    height: 14rem;
    border-radius: 20px;
    :first-child {
      margin-top: 3rem;
    }
    :nth-child(2) {
      margin-top: 3rem;
    }
    margin-top: 1.2rem;
  }
`
const Contents = styled.div`
  width: 100%;
  /* height: 100%; */
  display: flex;
  justify-content: center;
  align-items: center;
  flex-flow: column;
  font-family: Hiragino Sans;
`
const ContentTop = styled.div`
  text-align: center;
  font-size: 2rem;
  font-weight: 900;
  color: #707070;
  max-width: 80%;
  overflow: hidden;
  text-overflow: ellipsis;
  word-break: break-word;
  display: -webkit-box;

  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  @media (max-width: 1500px) {
    font-size: 1.9rem;
    }
    @media (max-width: 1300px) {
      font-size: 1.5rem;
    }
    @media (max-width: 1110px) {
      font-size: 1.2rem;
    }
    @media (max-width: 950px) {
      font-size: ${isDesktop?'1.1rem':'1.4rem'}
    }
    @media (max-width: 850px) {
      font-size: 1.4rem;
    }
    @media (max-width: 500px) {
      margin-top: 15px;
      font-size: 0.8rem;
    }
`
const ContentBot = styled.div`
  font-size: 1.5rem;
  font-weight: 700;
  color: #ffcc5a;
  @media (max-width: 1500px) {
    font-size: 1.5rem;
    }
    @media (max-width: 1300px) {
      font-size: 1.5rem;
    }
    @media (max-width: 1110px) {
      font-size: 1.2rem;
    }
    @media (max-width: 950px) {
      font-size: ${isDesktop?'1.1rem':'1.4rem'}
    }
    @media (max-width: 850px) {
      font-size: 1.4rem;
    }
    @media (max-width: 500px) {
      font-size: 0.8rem;
    }
`
const BlockImage = styled.div`
  width: 80%;
  height: 65%;
    @media (max-width: 1500px) {
      height: 60%;
    }
    @media (max-width: 1300px) {
      height: 61%;
    }
    @media (max-width: 1110px) {
      height: 57%;
    }
    @media (max-width: 950px) {
      height: ${isDesktop?'58%':'50%'}
    }
    @media (max-width: 850px) {
      height: 57%;
    }
    @media (max-width: 500px) {
      height: 66% !important;
      margin-top: 0;
    }

  img {
    @media (max-width: 3120px) {
      margin-top: -1.5rem;
    }
    @media (max-width: 1650px) {
      margin-top: -1rem;
    }
    @media (max-width: 850px) {
      margin-top: -1rem;
    }
    @media (max-width: 500px) {
      height: 90% !important;
      margin-top: 2px;
    }
  }
`

export { WrapperPrize, Contents, ContentTop, ContentBot, BlockImage }
