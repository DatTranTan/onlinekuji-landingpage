import * as React from 'react'
import { Select } from 'baseui/select'
import { CarretDownIcon } from '../AllSvgIcon'
import { PLACEMENT } from 'baseui/popover'

export const getContainerFontStyle = ({ $theme }) => {
  return $theme.typography.fontBold14
}
export default ({ ...props }) => {
  return (
    <Select
      noResultsMsg='データがありません。'
      overrides={{
        SelectArrow: () => {
          return <CarretDownIcon />
        },
        // Root: {
        //   style: ({ $theme, $isFocused, $width }) => ({
        //     width: '100%',
        //     border: $isFocused
        //       ? `2px solid ${$theme.colors.primary}`
        //       : '2px solid #EEEEEE'
        //   })
        // },
        // InputContainer: {
        //   style: ({ $theme, $isFocused }) => ({
        //     borderTopColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
        //     borderBottomColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
        //     borderLeftColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
        //     borderRightColor: $isFocused ? '#F6F6F6' : '#EEEEEE'
        //   })
        // },
        // ControlContainer: {
        //   style: ({ $theme, $isFocused }) => ({
        //     borderTopColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
        //     borderBottomColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
        //     borderLeftColor: $isFocused ? '#F6F6F6' : '#EEEEEE',
        //     borderRightColor: $isFocused ? '#F6F6F6' : '#EEEEEE'
        //   })
        // },
        Dropdown: {
          style: ({ $theme }) => ({
            maxHeight: '300px'
          })
        },
        DropdownContainer: {
          style: ({ $theme }) => ({
            minWidth: '300px',
            maxWidth: '500px'
          })
        },
        Placeholder: {
          style: ({ $theme }) => {
            return {
              ...$theme.typography.fontBold14
            }
          }
        },
        DropdownListItem: {
          style: ({ $theme }) => {
            return {
              ...$theme.typography.fontBold14,
              color: $theme.colors.textNormal,
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              texOverflow: 'ellipsis'
            }
          }
        },
        OptionContent: {
          style: ({ $theme, $selected }) => {
            return {
              ...$theme.typography.fontBold14,
              color: $selected
                ? $theme.colors.textDark
                : $theme.colors.textNormal
            }
          }
        },
        SingleValue: {
          style: ({ $theme }) => {
            return {
              ...$theme.typography.fontBold14,
              color: $theme.colors.textDark
            }
          }
        },
        Popover: {
          props: {
            placement:
              window.innerWidth > 1024
                ? PLACEMENT.bottom
                : PLACEMENT.bottomLeft,
            overrides: {
              Body: {
                style: { zIndex: props.zIndex ? props.zIndex : 2 }
              }
              // Inner: {
              //   style: { width: '300px' }
              // }
            }
          }
        },
        ClearIcon: {
          props: {
          title:"データ削除",
          }
        }
      }}
      {...props}
    />
  )
}
