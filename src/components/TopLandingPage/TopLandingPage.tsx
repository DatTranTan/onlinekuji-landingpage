import React from 'react'
import {
  WrapperBlock,
  BlockTitle,
  BlockElement,
  TopElement,
  Content,
  WrapperImg
} from './TopLandingPage.style'

// const Array = {
//   value: '1',
//   title: '一等賞',
//   element: '商品A',
//   content:
//     '当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者当選者'
// }

type TopLandingPageProps = {
  name: string
  rank: number
  quantity: number
  imageUrl?: string
}

const TopLandingPage: React.FC<TopLandingPageProps> = ({
  name,
  rank,
  quantity,
  imageUrl,
  ...props
}) => {
  return (
    <WrapperBlock {...props}>
      <BlockTitle>
        <h1
          style={{
            fontWeight: 600,
            color: '#e52f37',
            textShadow: '3px 4px 1 rgba(0, 0, 0, 0.15)',
            marginBottom: 0
          }}
        >
          {rank}
        </h1>
      </BlockTitle>
      <BlockElement>
        <TopElement>{name}</TopElement>
        <Content>
          {quantity}
          <span> 名様</span>
        </Content>
        <WrapperImg>
          <img
            src={imageUrl}
            alt=''
            width='100%'
            height='100%'
            style={{ objectFit: 'contain' }}
          />
        </WrapperImg>
      </BlockElement>
    </WrapperBlock>
  )
}

export default TopLandingPage
