import React from 'react'
import { Input as BaseInput, SIZE } from 'baseui/input'

const getInputFontStyle = ({ $theme }) => {
  return {
    color: $theme.colors.textDark,
    ...$theme.typography.fontBold14,
    ":-ms-input-placeholder": { /* Internet Explorer 10-11 */
      color: 'gray',
     }
  }
}

const Input = ({ ...props }) => {
  return (
    <BaseInput
      // onKeyDown={(e) => {
      //   if (e.key === 'Enter') {
      //     e.preventDefault()
      //   }
      // }}
      overrides={{
        Input: {
          style: ({ $theme }) => {
            return {
              ...getInputFontStyle({ $theme })
            }
          }
        }
      }}
      {...props}
    />
  )
}

export { SIZE }
export default Input
