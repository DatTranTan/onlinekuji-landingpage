/* eslint-disable */
import React from 'react'
import { Input as BaseInput, SIZE } from 'baseui/input'
import { EyeInvisibleOutlined, EyeOutlined } from '@ant-design/icons'
import { Tooltip } from 'antd'
import { Button, KIND } from 'baseui/button'
const InputPassword = ({ ...props }) => {
  const [showPassword, setShowPassword] = React.useState(false)
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword)
  }
  return (
    <Tooltip placement='topLeft' title={showPassword && props.value}>
      <div>
        <BaseInput
          type={showPassword ? 'text' : 'password'}
          disabled
          endEnhancer={
            <Button
              onClick={handleClickShowPassword}
              size='compact'
              kind={KIND.secondary}
              shape={'round'}
              overrides={{
                BaseButton: {
                  style: () => ({
                    backgroundColor: 'rgb(0, 0, 0, 0)',
                    maxHeight: '30px',
                    maxWidth: '30px'
                  })
                }
              }}
            >
              {showPassword ? (
                <EyeOutlined style={{ color: 'black' }} />
              ) : (
                <EyeInvisibleOutlined style={{ color: 'black' }} />
              )}
            </Button>
          }
          overrides={{
            Input: {
              style: () => ({
                cursor: 'text',
                paddingTop: 0,
                paddingBottom: 0,
                overflow: 'hidden',
                textOverflow: 'ellipsis'
              })
            },
            EndEnhancer: {
              style: ({ $theme, $isFocused }) => ({
                backgroundColor: 'rgb(0, 0, 0, 0)',
                maxWidth: '20px'
              })
            },
            InputContainer: {
              style: () => ({
                backgroundColor: 'rgb(0, 0, 0, 0)',
                borderRightStyle: 0,
                borderLeftWidth: 0,
                borderTopStyle: 0,
                borderBottomStyle: 0,
                borderLeftStyle: 0,
                borderRightWidth: 0,
                borderTopWidth: 0,
                borderBottomWidth: 0
              })
            },
            MaskToggleButton: ({}) => null
          }}
          {...props}
        />
      </div>
    </Tooltip>
  )
}

export { SIZE }
export default InputPassword
