/* eslint-disable */
import React, { useEffect, useState, useCallback, memo } from 'react'
import { useDropzone } from 'react-dropzone'
import { CloseIcon, UploadIcon } from '../AllSvgIcon'
import { CloseOutlined, LoadingOutlined } from '@ant-design/icons'
import {
  FileItemWrapper,
  PrizeContainer,
  Text,
  ImageName,
  TextHighlighted,
  ButtonContainer,
  CloseButton,
  LabelUpload,
  FileName,
  FileSize
} from './Uploader.style'
import { useEventState, useEventDispatch } from '../../context/EventContext'
import { useStyletron } from 'baseui'
import { theme } from '../../theme/index'
import { TestShowImage } from './BannerUploader.style'
type Props = {
  onChange: (files: any) => void
  disabled: boolean
  onDelete: () => void
  imageURL: string
  progress: any
}
const Uploader: React.FC<Props> = ({
  onChange,
  disabled,
  onDelete,
  imageURL,
  progress: { loaded, total, unit }
}) => {
  const [css] = useStyletron()
  const [files, setFiles] = useState([])
  let isReset = useEventState('isReset')
  const dispatch = useEventDispatch()
  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/*',
    multiple: false,
    disabled: disabled,
    onDrop: useCallback(
      (acceptedFiles) => {
        setFiles(
          acceptedFiles.map((file) =>
            Object.assign(file, {
              preview: URL.createObjectURL(file)
            })
          )
        )
        onChange(acceptedFiles)
      },
      [onChange]
    )
  })
  const deletePhotoShow = () => {
    setFiles([])
    onDelete()
  }
  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview))
    },
    [files]
  )
  useEffect(() => {
    // Make sure to revoke the data uris to avoid memory leaks
    if (!!imageURL)
      setFiles([
        { name: decodeURI(imageURL)?.split('/')[5], preview: imageURL }
      ])
  }, [imageURL])

  useEffect(() => {
    // Make sure to revoke the data uris to avoid memory leaks
    if (isReset) {
      setFiles([])
      dispatch({
        type: 'RESET_PRIZE_FIELDS',
        isReset: false
      })
    }
  }, [isReset])

  const progressPercent = total && `${loaded}/${total} ${unit}`
  const totalPercent = total && `${total} ${unit}`
  return (
    <section
      style={{
        display: 'flex',
        flexDirection: 'column',
        padding: 0
      }}
      className='uploader'
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          padding: 0
        }}
      >
        <LabelUpload
          className={css({
            backgroundColor: 'rgb(221,221,221)',
            fontFamily: "'Lato', sans-serif",
            ...theme.typography.fontBold14,
            color: theme.colors.textDark,
            minWidth: '100px !important'
          })}
        >
          画像
        </LabelUpload>
        <PrizeContainer {...getRootProps()}>
          <input {...getInputProps()} />
          <UploadIcon />
          {/* <Text>
          <TextHighlighted>
            ここをクリック、あるいは画像をドラッグ＆ドロップしてください。{' '}
          </TextHighlighted>
        </Text> */}
        </PrizeContainer>
      </div>
      {files.length > 0 && (
        <FileItemWrapper title={files[0].name}>
          {/* <ImageName href={files[0]?.preview} target={'_blank'}>
            <FileName>{files[0].name}</FileName> &nbsp;(
            <FileSize>
              {loaded === total ? totalPercent : progressPercent}
            </FileSize>
            )
          </ImageName> */}
          <ImageName target={'_blank'}>
            <TestShowImage src={files[0].preview}></TestShowImage>
            <FileName>{files[0].name}</FileName>&nbsp;
            <FileSize>
              ({loaded === total ? totalPercent : progressPercent})
            </FileSize>
          </ImageName>
          <ButtonContainer>
            <CloseButton onClick={deletePhotoShow} disabled={disabled}>
              {!disabled ? (
                <CloseOutlined width={7} height={7} />
              ) : (
                <LoadingOutlined width={8} height={8} />
              )}{' '}
            </CloseButton>
          </ButtonContainer>
        </FileItemWrapper>
      )}
    </section>
  )
}

export default memo(Uploader)
