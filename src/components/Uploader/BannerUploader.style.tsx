import styled from 'styled-components'
import { Image } from 'antd'

export const TestShowImage = styled(Image)`
  width: 40px;
  height: 40px;
  object-fit: cover;
  margin: 10px;
`
