import gql from 'graphql-tag'

export const MUTATION_CREATE_QUESTIONNAIRE = gql`
  mutation($createQuestionnaireInput: CreateQuestionnaireInput!) {
    createQuestionnaire(createQuestionnaireInput: $createQuestionnaireInput) {
      id
      question
      questionList{
        id
        question
        type
        content
      }
      createdAt
      updatedAt
    }
  }
`
