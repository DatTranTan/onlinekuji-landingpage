import gql from 'graphql-tag'

export const MUTATION_DELETE_USER = gql`
  mutation($ids: [String!]!) {
    deletesUser(ids: $ids)
  }
`
