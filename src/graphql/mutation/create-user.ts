import gql from 'graphql-tag'

export const MUTATION_CREATE_USER = gql`
  mutation($createUserInput: CreateUserInput!) {
    createUser(createUserInput: $createUserInput) {
      id
      pwd
      nameKanji
      companyName
      userType {
        id
        role
        name
      }
      agency {
        id
        pwd
        nameKanji
      },
      maxClient
    }
  }
`
