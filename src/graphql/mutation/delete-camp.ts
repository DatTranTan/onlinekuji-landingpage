import gql from 'graphql-tag'

export const DELETE_CAMPAIGNS = gql`
  mutation($ids: [String!]!) {
    deletecampaigns(ids: $ids)
  }
`