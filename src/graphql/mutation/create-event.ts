import gql from 'graphql-tag'

export const MUTATION_CREATE_EVENT = gql`
  mutation ($createEventInput: CreateEventInput!) {
    createEvent(createEventInput: $createEventInput) {
      id
      name
      startTime
      endTime
      memo
      timeOut
      videoBackground
      video {
        id
        name
        url
      }
      noPrizeVideo {
        id
        name
        url
      }
      banner
      bannerUrl
      footerBanner
      footerBannerUrl
      prizes {
        id
        name
        imageUrl
        rate
        quantity
      }
      owner {
        id
        nameKanji
      }
      questionnaire {
        id
        question
        type
        content
      }
      customerLost
      template {
          id
          name
        }
      campaign {
        id 
        name
        content
      }
    }
  }
`
