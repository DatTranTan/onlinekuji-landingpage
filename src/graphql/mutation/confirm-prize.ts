import gql from 'graphql-tag'

export const MUTATION_CONFIRM_PRIZE = gql`
  mutation($eventId: String!, $prizeId: String!) {
    minusPrizeQuantity(eventId: $eventId, prizeId: $prizeId)
  }
`
