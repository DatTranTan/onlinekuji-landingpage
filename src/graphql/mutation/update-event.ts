import gql from 'graphql-tag'

export const MUTATION_UPDATE_EVENT = gql`
  mutation ($updateEventInput: UpdateEventInput!) {
    updateEvent(updateEventInput: $updateEventInput) {
      id
      name
      startTime
      endTime
      memo
      timeOut
      video {
        id
        name
        url
      }
      noPrizeVideo {
        id
        name
        url
      }
      banner
      footerBanner
      bannerUrl
      footerBannerUrl
      prizes {
        id
        name
        imageUrl
        rate
        quantity
      }
      owner {
        id
        nameKanji
      }
      questionnaire {
        id
        question
        type
        content
      }
      customerLost
      template {
          id
          name
        }
      campaign {
        id 
        name
        content
      }
    }
  }
`
