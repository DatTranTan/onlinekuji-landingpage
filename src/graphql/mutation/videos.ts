import gql from 'graphql-tag'

export const MUTATION_CREATE_VIDEOS = gql`
  mutation($createVideoInput: CreateVideoInput!) {
    createVideo(createVideoInput: $createVideoInput) {
      id
      name
      url
    }
  }
`
