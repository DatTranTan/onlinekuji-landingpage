import gql from 'graphql-tag'

export const MUTATION_UPDATE_USER = gql`
  mutation($updateUserInput: UpdateUserInput!) {
    updateUser(updateUserInput: $updateUserInput) {
      id
      pwd
      nameKanji
      companyName
      agency {
        id
        nameKanji
      }
      userType {
        id
        role
        name
      }
      tel
      email
      address
      maxClient
    }
  }
`
