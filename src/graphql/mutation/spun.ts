import gql from 'graphql-tag'

export const MUTATION_SPUN = gql`
  mutation ($userId: String!, $eventId: String!) {
    checkSpun(userId: $userId, eventId: $eventId)
  }
`

export const MUTATION_INCREASE_WIN = gql`
  mutation ($eventId: String!) {
    increaseTotalWin(eventId: $eventId)
  }
`
