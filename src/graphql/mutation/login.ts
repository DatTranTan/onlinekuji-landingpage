import gql from 'graphql-tag'

export const MUTATION_LOGIN = gql`
  mutation($input: LoginInput!) {
    login(loginInput: $input) {
      id
      pwd
      nameKanji
      companyName
      userType {
        id
        role
        name
      }
      tel
      email
      address
      accessToken
      expiresTime
      refreshToken
    }
  }
`
