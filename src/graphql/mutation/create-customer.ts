import gql from 'graphql-tag'

export const MUTATION_CREATE_CUSTOMER = gql`
  mutation ($createCustomerInput: CreateCustomerInput!) {
    createCustomer(createCustomerInput: $createCustomerInput) {
      name
      age
      gender
      tel
      email
      address
      postalCode
      createdAt
      updatedAt
      prize {
        id
        name
        rank
        rate
        quantity
      }
      questionnaire
      event {
        customerLost
      }
      prizeStatus
    }
  }
`
