import gql from 'graphql-tag'

export const MUTATION_CREATE_CAMP = gql`
mutation($createCampaignInput: CreateCampaignInput!) {
    createCampaign(createCampaignInput: $createCampaignInput) {
        id
        name
        content
    }
}
`