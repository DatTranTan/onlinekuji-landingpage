import gql from 'graphql-tag'

export const MUTATION_UPDATE_QUESTIONNAIRE = gql`
  mutation($updateQuestionnaireInput: UpdateQuestionnaireInput!) {
    updateQuestionnaire(updateQuestionnaireInput: $updateQuestionnaireInput) {
      id
      question
      questionList{
        id
        question
        type
        content
      }
      createdAt
      updatedAt
    }
  }
`
