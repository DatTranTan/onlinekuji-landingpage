import gql from 'graphql-tag'

export const DELETE_VIDEO = gql`
  mutation($ids: [String!]!) {
    deleteVideos(ids: $ids)
  }
`
