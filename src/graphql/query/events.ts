import gql from 'graphql-tag'

export const QUERY_EVENTS = gql`
  query (
    $id: String
    $offset: Float
    $limit: Float
    $isExpired: Boolean
    $orderBy: EventOrderByInput
  ) {
    events(
      id: $id
      offset: $offset
      limit: $limit
      isExpired: $isExpired
      orderBy: $orderBy
    ) {
      count
      events {
        id
        name
        startTime
        endTime
        memo
        timeOut
        videoBackground
        video {
          id
          name
          url
        }
        noPrizeVideo {
          id
          name
          url
        }
        banner
        footerBanner
        bannerUrl
        footerBannerUrl
        prizes {
          id
          name
          rank
          rate
          quantity
          numberOfWinner
          imageUrl
          createdAt
          updatedAt
        }
        owner {
          id
          nameKanji
        }
        numberOfLotteryPeople
        createdAt
        updatedAt
        questionnaire {
          id
          question
          content
          type
        }
        customerLost
        template {
          id
          name
        }
        campaign {
          id
          name
          content
        }
      }
    }
  }
`
