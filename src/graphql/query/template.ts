import gql from 'graphql-tag'

export const QUERY_TEMPLATES = gql`
  query ($name: String, $offset: Float = 0, $limit: Float = 0) {
    Templates(name: $name,offset: $offset, limit: $limit) {
     count
     templates {
       id
       name
     }
    }
  }
`

