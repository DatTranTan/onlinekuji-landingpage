import gql from 'graphql-tag'

export const QUERY_CUSTOMERS = gql`
  query (
    $searchText: String
    $offset: Float
    $limit: Float
    $prizeStatus: PrizeStatus
    $eventId: String
    $ownerId: String
  ) {
    customers(
      searchText: $searchText
      offset: $offset
      limit: $limit
      prizeStatus: $prizeStatus
      eventId: $eventId
      ownerId: $ownerId
    ) {
      count
      customers {
        id
        name
        tel
        email
        address
        postalCode
        age
        gender
        createdAt
        updatedAt
        prize {
          id
          name
          rank
        }
        prizeStatus
        event {
          id
          name
          customerLost
        }
        questionnaire
      }
    }
  }
`

export const QUERY_EVENTS = gql`
  query ($id: String, $offset: Float, $limit: Float, $ownerId: String) {
    events(id: $id, offset: $offset, limit: $limit, ownerId: $ownerId) {
      count
      events {
        id
        name
        owner {
          id
          nameKanji
        }
      }
    }
  }
`

export const QUERY_AGENCYS = gql`
  query ($searchText: String, $skip: Float, $take: Float, $userType: String) {
    agencys: users(
      searchText: $searchText
      skip: $skip
      take: $take
      userType: $userType
    ) {
      count
      users {
        id
        nameKanji
      }
    }
  }
`
