import gql from 'graphql-tag'

export const QUERY_VIDEO = gql`
  query($name: String, $offset: Float, $limit: Float) {
    Videos(name: $name, offset: $offset, limit: $limit) {
      count
      videos {
        id
        name
        url
        createdAt
        updatedAt
      }
    }
  }
`
