import gql from 'graphql-tag'

export const QUERY_EVENT = gql`
  query ($id: String!) {
    event(id: $id) {
      id
      name
      startTime
      endTime
      memo
      footerBanner
      bannerUrl
      footerBannerUrl
      numberOfLotteryPeople
      timeOut
      videoBackground
      prizes {
        id
        name
        rank
        rate
        quantity
        numberOfWinner
        imageUrl
        message
        createdAt
        updatedAt
      }
      video {
        id
        name
        url
      }
      noPrizeVideo {
        id
        name
        url
      }
      questionnaire {
        id
        question
        content
        type
        questionList {
          id
          question
          content
          type
        }
      }
      customerLost
      banner
      template {
          id
          name
        }
      campaign {
        id
        name
        content
      }
    }
  }
`

export const QUERY_EVENT_VIDEO = gql`
  query ($id: String!) {
    event(id: $id) {
      id
      name
      startTime
      endTime
      prizes {
        id
        name
        rank
        rate
        quantity
        numberOfWinner
        imageUrl
        message
      }
      video {
        id
        name
        url
      }
      banner
    }
  }
`

// export const QUERY_EVENT_VIDEO = gql`
//   query($id: String!) {
//     event(id: $id) {
//       id
//       name
//       startTime
//       endTime
//       prizes {
//         id
//         name
//         rank
//         rate
//         quantity
//         numberOfWinner
//         imageUrl
//         message

//       }
//       video {
//         id
//         name
//         url
//         createdAt
//         updatedAt
//       }
//     }
//   }
// `
