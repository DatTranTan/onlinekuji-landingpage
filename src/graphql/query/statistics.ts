import gql from 'graphql-tag'

export const QUERY_STATISTICS = gql`
  query ($eventId: String, $ownerId: String, $timeFrom: Float, $timeTo: Float) {
    statistics(
      eventId: $eventId
      ownerId: $ownerId
      timeFrom: $timeFrom
      timeTo: $timeTo
    ) {
      numberOfLotteryPeople
      totalVisit
      totalForm
      totalPeopleWin
    }
  }
`
